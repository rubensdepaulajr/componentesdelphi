{*******************************************************************************
* Unit Name : uModCad                                       allSoft - Sistemas *
* Author    : Rubens Jr                                     Date  : 29.09.2002 *
* Purpose   : Cadastro Modelo                                                  *
*******************************************************************************}
unit uModCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ImgList, ComCtrls, StdCtrls, Buttons, ExtCtrls, DBCtrls,
  ToolWin, Db, ADODb, Menus, Variants;

type
  TString4  = string[4];

  TfModCad = class(TForm)
    dsMod: TDataSource;
    setMod: TADODataSet;
    StatusBar: TStatusBar;
    pnlClient: TPanel;
    CoolBar1: TCoolBar;
    ToolBar: TToolBar;
    btnNew: TToolButton;
    btnEdit: TToolButton;
    ToolButton3: TToolButton;
    btnSave: TToolButton;
    btnCancel: TToolButton;
    ToolButton6: TToolButton;
    btnDel: TToolButton;
    ToolButton8: TToolButton;
    btnAjuda: TToolButton;
    ToolButton10: TToolButton;
    btnClose: TToolButton;
    btnPrint: TToolButton;
    ToolButton1: TToolButton;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnAjudaClick(Sender: TObject);
  private
    { Private declarations }
    procedure pShowHint(Sender: TObject);

  protected
    { Protected declarations }
    // Recebe o componente que ganhar� foco no momento da Inclus�o/Edi��o
    oFocusControl: TWinControl;

    procedure pLockControls(bBrowse: Boolean); virtual;
  public
    { Public declarations }
    procedure pInit_Form(sTabelaUsada: string);
  end;

var
  fModCad: TfModCad;

implementation

uses udmGeral;

{$R *.DFM}

procedure TfModCad.pShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
    begin
      StatusBar.SimplePanel := True;
      StatusBar.SimpleText := Application.Hint;
    end
  else
    StatusBar.SimplePanel := False;
  //   StatusBar.Panels[0].Text:= Application.Hint;
end;

procedure TfModCad.FormCreate(Sender: TObject);
begin
  // Verifica se o usu�rio tem Controle Total no From
  //dmGeral.bControleTotal := dmGeral.fGetAcesso(-1);

  // Determina que a Aplica��o que no momento que for disparado o OnHint da
  // aplica��o o mesmo usar� a fun��o pShowHint e exibir na barra de Status o
  // Hint do componente.
  Application.OnHint := pShowHint;

  if dsMod.Dataset = nil then
    begin
      ShowMessage('Erro no Modelo: Voc� esqueceu de "setar" o DataSet do DataSource');
      PostMessage(Self.Handle, WM_Close, 0, 0);
      Exit;
    end;

  if pnlClient.ControlCount = 0 then
    begin
      ShowMessage('Erro na configura��o do Form: Voc� esqueceu de adicionar os controles de edi��o no Form Tab!');
      PostMessage(Self.Handle, WM_Close, 0, 0);
      Exit;
    end;
  setMod.Open;
  pLockControls(True);
end;

procedure TfModCad.pInit_Form(sTabelaUsada: string);
begin
  ShowModal;
  Free;
end;

procedure TfModCad.pLockControls(bBrowse: Boolean);
var i: Integer;
begin                                                    
  for i := 0 to ComponentCount -1 do
    if (Components[i] is TDBEdit) and not(TDBEdit(Components[i]).ReadOnly) then
        TDBEdit(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBMemo) and not(TDBMemo(Components[i]).ReadOnly) then
      TDBMemo(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBLookupComboBox) and not (TDBLookupComboBox(Components[i]).ReadOnly) then
      TDBLookupComboBox(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBComboBox) and not (TDBComboBox(Components[i]).ReadOnly) then
      TDBComboBox(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBRadioGroup) and not (TDBRadioGroup(Components[i]).ReadOnly)then
      TDBRadioGroup(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBCheckBox) and not (TDBCheckBox(Components[i]).ReadOnly) then
      TDBCheckBox(Components[i]).Enabled := not  bBrowse;


  // Mudando a cor dos componentes que tem o preenchimento requerido
  for i := 0 to ComponentCount -1 do
    begin
      if (Components[i] is TDBEdit) and (Components[i].Tag = 0) then
        if bBrowse then
          TDBEdit(Components[i]).Color := clWindow
        else
          TDBEdit(Components[i]).Color := clInfoBk;
      if (Components[i] is TDBMemo) and (Components[i].Tag = 0) then
        if bBrowse then
          TDBMemo(Components[i]).Color := clWindow
        else
          TDBMemo(Components[i]).Color := clInfoBk;
      if (Components[i] is TDBLookupComboBox) and (Components[i].Tag = 0) then
        if bBrowse then
          TDBLookupComboBox(Components[i]).Color := clWindow
        else
          TDBLookupComboBox(Components[i]).Color := clInfoBk;
    end;

  if bBrowse  then
    begin
      setMod.Cancel;
      btnNew.Enabled    := True;
      btnEdit.Enabled   := not setMod.Eof;
      btnDel.Enabled    := btnEdit.Enabled;
      btnSave.Enabled   := False;
      btnCancel.Enabled := False;
      btnAjuda.Enabled  := True;
    end
  else
    begin
      btnNew.Enabled    := False;
      btnEdit.Enabled   := False;
      btnDel.Enabled    := False;
      btnSave.Enabled   := True;
      btnCancel.Enabled := True;
      btnAjuda.Enabled  := False;
  //    if Assigned(oFocusControl) then
  //      oFocusControl.SetFocus;
    end;
end;

// We want to destroy forms which are closed
procedure TfModCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfModCad.btnCancelClick(Sender: TObject);
begin
  pLockControls(True);
  setMod.Cancel;
end;

procedure TfModCad.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var mResult : Integer;
begin
  if (setMod <> nil) and
     (setMod.Modified) then
    begin
      mResult := MessageDlg('Salvar as altera��es', mtConfirmation,
                            [mbYes, mbNo, mbCancel], 0);
      case mResult of
        mrYes    : pLockControls(True);
        mrNo     : pLockControls(True);
        mrCancel : CanClose := False;
      end;
    end;
end;

procedure TfModCad.FormDestroy(Sender: TObject);
begin
  //Esta dando problema aqui no momento de fechar
  setMod.Close;
  // Sen�o liberar, d� um erro de mem�ria quando o Form � fechado
  Application.OnHint := nil;
end;

procedure TfModCad.btnDelClick(Sender: TObject);
begin
  //if not dmGeral.bControleTotal then
  //if not dmGeral.fGetAcesso(1) then
  //  Exit;

  if MessageDlg('Apagar o registro selecionado?', mtConfirmation, [MBYes, MBNo], 0) = mrYes then
    try
      setMod.Delete;
    except;
      dmGeral.pMensagemOK('Este registro n�o pode ser exclu�do!  Existem outros que dependem dele.','i');
    end;
end;

procedure TfModCad.btnEditClick(Sender: TObject);
begin
  //if not dmGeral.bControleTotal then
  //if not dmGeral.fGetAcesso(0) then
  //  Exit;

  pLockControls(False);
  setMod.Edit;
end;

procedure TfModCad.btnNewClick(Sender: TObject);
begin
  //if not dmGeral.bControleTotal then
  //if not dmGeral.fGetAcesso(2) then
  //  Exit;
  pLockControls(False);
  setMod.Insert;
end;

procedure TfModCad.btnSaveClick(Sender: TObject);
var sSavePlace: string;

    function fVerificaPreenchimento: Boolean;
    var i: Integer;
    begin
      Result := True;
      for i := 0 to ComponentCount -1 do
        if (Components[i] is TDBEdit) and (Trim(TDBEdit(Components[i]).Text) = '')and (TDBEdit(Components[i]).tag = 0) then
          begin
            Result := False;
            Break;
          end
        else
        if (Components[i] is TDBMemo) and (Trim(TDBMemo(Components[i]).Text) = '')and (TDBMemo(Components[i]).tag = 0) then
          begin
            Result := False;
            Break;
          end
        else
        if (Components[i] is TDBLookupComboBox) and (TDBLookupComboBox(Components[i]).KeyValue = Null)and (TDBLookupComboBox(Components[i]).tag = 0) then
          begin
            Result := False;
            Break;
          end{
        if (Components[i] is TDBDatePicker) and (TDBDatePicker(Components[i]).Text = '  /  /    ')and (TDBDatePicker(Components[i]).tag = 0) then
          begin
            Result := False;
            Break;
          end};
    end;

begin
  if not fVerificaPreenchimento then
    begin
      Application.MessageBox('Os campos em destaque s�o requeridos!','Aten��o!!!', mb_OK + mb_IconExclamation);
      Abort;
    end
  else
    begin
      pLockControls(True);
      try
        setMod.Post;
        sSavePlace := setMod.Fields[0].Text;
        setMod.Requery;
        setMod.Locate(setMod.Fields[0].FieldName,sSavePlace,[]);
      except
        pLockControls(False);
        dmGeral.pMensagemOK('Erro ao gravar o registro. Verifique se n�o existe um registro com o mesmo c�digo.','x');
      end;

    end;
end;

procedure TfModCad.SpeedButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfModCad.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfModCad.btnAjudaClick(Sender: TObject);
begin
  Application.HelpCommand(HELP_CONTENTS, 0);
end;

end.
