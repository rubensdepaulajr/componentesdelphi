{*******************************************************************************
* Unit Name : uModMtaGrp                                    allSoft - Sistemas *
* Author    : Rubens Jr                                     Date  : 12.04.2003 *
* Purpose   : Cadastro Modelo para sele��o                                     *
*******************************************************************************}
unit uModMtaGrp;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Buttons, StdCtrls, DBCtrls, Grids, DBGrids, ComCtrls, Db, ADODB, ToolWin,
  ExtCtrls;

type
  TfModMtaGrp = class(TForm)
    PgCtrl: TPageControl;
    Tsh1: TTabSheet;
    Dbg1: TDBGrid;
    Tsh2: TTabSheet;
    DBText1 : TDbText;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    St1: TADODataSet;
    St2: TADODataSet;
    Dt1: TDataSource;
    Dt2: TDataSource;
    St0: TADODataSet;
    Dt0: TDataSource;
    Dbg2: TDBGrid;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    BackBtn: TToolButton;
    TbCtrl: TTabControl;
    Dbg3: TDBGrid;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    Dbg4: TDBGrid;
    procedure SpeedButton1Click(Sender: TObject);
    procedure SpeedButton2Click(Sender: TObject);
    procedure PgCtrlChange(Sender: TObject);
    procedure PgCtrlChanging(Sender: TObject;
      var AllowChange: Boolean);
    procedure BackBtnClick(Sender: TObject);
    procedure Dt0DataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fModMtaGrp: TfModMtaGrp;

implementation

{$R *.DFM}
uses udmGeral;
procedure TfModMtaGrp.SpeedButton1Click(Sender: TObject);
begin
  st1.Close;
  St1.Open;
  St2.Close;
  St2.Open;
end;

procedure TfModMtaGrp.SpeedButton2Click(Sender: TObject);
begin
  St1.Close;
  St1.Open;
  St2.Close;
  St2.Open;
end;

procedure TfModMtaGrp.PgCtrlChange(Sender: TObject);
begin
  St1.Close;
  St1.Open;
  St2.Close;
  St2.Open;
end;

procedure TfModMtaGrp.PgCtrlChanging(Sender: TObject; var AllowChange: Boolean);
begin
  if St0.RecordCount = 0 then
    AllowChange := false;

end;

procedure TfModMtaGrp.BackBtnClick(Sender: TObject);
begin
  Close;
end;

procedure TfModMtaGrp.Dt0DataChange(Sender: TObject; Field: TField);
begin
  St2.Close;
  St2.Open;
end;

procedure TfModMtaGrp.FormCreate(Sender: TObject);
begin
  St0.Open;  //St2 abrir� no evento onChange de st0
  St1.Open;
  PgCtrl.ActivePage := Tsh1;//Evita que esque�amos de deixar a p�gina 1 ativa
end;

end.
