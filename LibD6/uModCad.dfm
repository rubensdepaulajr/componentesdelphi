�
 TFMODCAD 0�
  TPF0TfModCadfModCadLeft� Top� BorderIconsbiSystemMenu BorderStylebsDialogCaptionfModCadClientHeight�ClientWidth�Color	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoScreenCenterOnClose	FormCloseOnCloseQueryFormCloseQueryOnCreate
FormCreate	OnDestroyFormDestroyPixelsPerInch`
TextHeight 
TStatusBar	StatusBarLeft Top�Width�HeightPanelsWidth�  Width2  SimplePanel  TPanel	pnlClientLeft Top:Width�Height_AlignalClient
BevelInnerbvRaised
BevelOuter	bvLoweredTabOrder  TCoolBarCoolBar1Left Top Width�Height:AutoSize	BandsControlToolBar
ImageIndex�	MinHeight6Width�   TToolBarToolBarLeft	Top WidthzHeight6AutoSize	ButtonHeight4ButtonWidth7CaptionToolBarDisabledImagesdmGeral.Image32DisableFlat		HotImagesdmGeral.Image32HotImagesdmGeral.Image32GrayedShowCaptions	TabOrder  TToolButtonbtnNewLeft Top HintInserir um Novo RegistroCaption&Novo
ImageIndex ParentShowHintShowHint	OnClickbtnNewClick  TToolButtonbtnEditLeft7Top HintAlterar o RegistroCaptionAlt&erar
ImageIndexParentShowHintShowHint	OnClickbtnEditClick  TToolButtonToolButton3LeftnTop WidthCaptionToolButton3
ImageIndexStyletbsSeparator  TToolButtonbtnSaveLeftvTop HintSalvar o RegistroCaption&Salvar
ImageIndexParentShowHintShowHint	OnClickbtnSaveClick  TToolButton	btnCancelLeft� Top Hint.   Cancelar as Alterações efetuadas no RegistroCaption	&Cancelar
ImageIndexParentShowHintShowHint	OnClickbtnCancelClick  TToolButtonToolButton6Left� Top WidthCaptionToolButton6
ImageIndexStyletbsSeparator  TToolButtonbtnDelLeft� Top HintExlcluir o RegistroCaptionE&xcluir
ImageIndexParentShowHintShowHint	OnClickbtnDelClick  TToolButtonToolButton8Left#Top WidthCaptionToolButton8
ImageIndexStyletbsSeparator  TToolButtonbtnPrintLeft+Top HintImprimirCaption	&Imprimir
ImageIndexParentShowHintShowHint	Visible  TToolButtonToolButton1LeftbTop WidthCaptionToolButton1
ImageIndexStyletbsSeparatorVisible  TToolButtonbtnAjudaLeftjTop HintAjudaCaption&Ajuda
ImageIndexParentShowHintShowHint	VisibleOnClickbtnAjudaClick  TToolButtonToolButton10Left�Top WidthCaptionToolButton10
ImageIndexStyletbsSeparatorVisible  TToolButtonbtnCloseLeft�Top HintFechar a JanelaCaption&Fechar
ImageIndexParentShowHintShowHint	OnClickbtnCloseClick    TDataSourcedsModDataSetsetModLeft�Top  TADODataSetsetMod
Parameters LeftTop   