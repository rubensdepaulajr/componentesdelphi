unit udmGeral;

interface

uses
  SysUtils, Classes, ImgList, Controls, DB, ADODB, Messages,
  Windows, Graphics, Dialogs, Forms;

type
  TdmGeral = class(TDataModule)
    ADOConnection: TADOConnection;
    Image32Grayed: TImageList;
    Image32Hot: TImageList;
    Image32Disable: TImageList;
    cmdGeral: TADOCommand;
    setGeral: TADODataSet;
    procedure DataModuleCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    function fMensagemYesNo(Texto: string): Boolean;
    function fGetValue(sSQL: string): Variant;
    
    procedure pMensagemOK(Texto, Icone: string);
    procedure pExecSQL(sSQL: string);
  end;

var
  dmGeral: TdmGeral;

implementation

{$R *.dfm}

procedure TdmGeral.pMensagemOK(Texto, Icone: string);
begin
  if Icone = 'i' then
    Application.Messagebox(PChar(Texto), PChar('Mensagem'), mb_OK + mb_IconInformation);
  if Icone = '!' then
    Application.Messagebox(PChar(Texto), PChar('Mensagem'), mb_OK + mb_IconExclamation);
  if Icone = '?' then
    Application.Messagebox(PChar(Texto), PChar('Mensagem'), mb_OK + mb_IconQuestion);
  if Icone = 'x' then
    Application.Messagebox(PChar(Texto), PChar('Mensagem'), mb_OK + mb_IconError);
end;

function TdmGeral.fMensagemYesNo(Texto: string): Boolean;
begin
  if Application.Messagebox(PChar(Texto), PChar('Mensagem'),
                            mb_YesNo + mb_IconQuestion) = IDYes then
    Result := True
  else
    Result := False;
end;


procedure TdmGeral.DataModuleCreate(Sender: TObject);
begin
  ShortDateFormat := 'dd/mm/yyyy';
end;

procedure TdmGeral.pExecSQL(sSQL: string);
begin
  with cmdGeral do
    begin
      CommandText := sSQL;
      Execute;
    end;
end;

function TdmGeral.fGetValue(sSQL: string): Variant;
var vTemp: Variant;
    setTmp: TADODataSet;      
begin
  setTmp := TADODataSet.Create(Self);
  with setTmp do
    begin
      Connection := ADOConnection;
      CommandText := sSQL;
      Open;
      vTemp := Fields[0].Value;
      Close;
    end;
    Result := vTemp;
end;


end.
