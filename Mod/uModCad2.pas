{*******************************************************************************
* Unit Name : uModCad2                                    Mecatron Informática *
* Author    : Rubens Jr                                     Date  : 29.09.2002 *
* Purpose   : Cadastro Modelo                                                  *
*******************************************************************************}
unit uModCad2;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms,
  Dialogs, uModCad, DB, ADODB, ComCtrls, ToolWin, ExtCtrls, Grids, DBGrids,
  DBCtrls, StdCtrls, Buttons, Mask, Spin, ToolEdit, CurrEdit;

type
  TfModCad2 = class(TfModCad)
    pnlEdit: TPanel;
    pnlBrowse: TPanel;
    grd: TDBGrid;
    procedure btnNewClick(Sender: TObject);
    procedure grdTitleClick(Column: TColumn);
  private
    procedure pLockControls(bBrowse: Boolean); override;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fModCad2: TfModCad2;

implementation

{$R *.dfm}

procedure TfModCad2.pLockControls(bBrowse: Boolean);
var i: Integer;
begin                                             {
  for i := 0 to ComponentCount -1 do
    if (Components[i] is TDBEdit) and not(TDBEdit(Components[i]).ReadOnly) then
        TDBEdit(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBMemo) and not(TDBMemo(Components[i]).ReadOnly) then
      TDBMemo(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBLookupComboBox) and not (TDBLookupComboBox(Components[i]).ReadOnly) then
      TDBLookupComboBox(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBComboBox) and not (TDBComboBox(Components[i]).ReadOnly) then
      TDBComboBox(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBRadioGroup) and not (TDBRadioGroup(Components[i]).ReadOnly)then
      TDBRadioGroup(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBCheckBox) and not (TDBCheckBox(Components[i]).ReadOnly) then
      TDBCheckBox(Components[i]).Enabled := not  bBrowse;      }


  // Mudando a cor dos componentes que tem o preenchimento requerido
  for i := 0 to ComponentCount -1 do
    begin
      if (Components[i] is TEdit) and (Components[i].Tag = 1) then
        if bBrowse then
          TEdit(Components[i]).Color := clWindow
        else
          TEdit(Components[i]).Color := clInfoBk;
      if (Components[i] is TMemo) and (Components[i].Tag = 1) then
        if bBrowse then
          TMemo(Components[i]).Color := clWindow
        else
          TMemo(Components[i]).Color := clInfoBk;
      if (Components[i] is TDBLookupComboBox) and (Components[i].Tag = 1) then
        if bBrowse then
          TDBLookupComboBox(Components[i]).Color := clWindow
        else
          TDBLookupComboBox(Components[i]).Color := clInfoBk;
      if (Components[i] is TMaskEdit) and (Components[i].Tag = 1) then
        if bBrowse then
          TMaskEdit(Components[i]).Color := clWindow
        else
          TMaskEdit(Components[i]).Color := clInfoBk;
      if (Components[i] is TCurrencyEdit) and (Components[i].Tag = 1) then
        if bBrowse then
          TCurrencyEdit(Components[i]).Color := clWindow
        else
          TCurrencyEdit(Components[i]).Color := clInfoBk;
      if (Components[i] is TSpinEdit) and (Components[i].Tag = 1) then
        if bBrowse then
          TSpinEdit(Components[i]).Color := clWindow
        else
          TSpinEdit(Components[i]).Color := clInfoBk;
    end;

  if bBrowse  then
    begin
      pnlEdit.Visible   := False;
      pnlSearch.Visible := False;
      grd.Enabled       := True;
      btnSearch.Enabled := True;
      btnNew.Enabled    := True;
      btnEdit.Enabled   := not setMod.Eof;
      btnDel.Enabled    := btnEdit.Enabled;
      btnSave.Enabled   := False;
      btnCancel.Enabled := False;
      btnHelp.Enabled   := True;
    end
  else
    begin
      pnlEdit.Visible   := True;
      grd.Enabled       := False;
      btnNew.Enabled    := False;
      btnEdit.Enabled   := False;
      btnSearch.Enabled := False;
      btnDel.Enabled    := False;
      btnSave.Enabled   := True;
      btnCancel.Enabled := True;
      btnHelp.Enabled   := False;
      if Assigned(oFocusControl) then
        oFocusControl.SetFocus;
    end;
end;

procedure TfModCad2.btnNewClick(Sender: TObject);
var i: Integer;
begin
  inherited;
  //pnlEdit.Visible := True;
end;

procedure TfModCad2.grdTitleClick(Column: TColumn);
begin
  inherited;
  if Column.FieldName <> setMod.IndexFieldNames then
    setMod.IndexFieldNames := Column.FieldName
  else
    setMod.IndexFieldNames := Column.FieldName + ' DESC';
end;

end.
