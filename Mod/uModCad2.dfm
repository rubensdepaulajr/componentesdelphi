inherited fModCad2: TfModCad2
  Left = 224
  Top = 133
  Caption = 'fModCad2'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited pnlClient: TPanel
    object pnlEdit: TPanel
      Left = 2
      Top = 2
      Width = 647
      Height = 65
      Align = alTop
      BevelInner = bvRaised
      BevelOuter = bvLowered
      TabOrder = 0
      Visible = False
    end
    object pnlBrowse: TPanel
      Left = 2
      Top = 67
      Width = 647
      Height = 219
      Align = alClient
      BevelInner = bvRaised
      BevelOuter = bvLowered
      BorderWidth = 1
      TabOrder = 1
      object grd: TDBGrid
        Left = 3
        Top = 3
        Width = 641
        Height = 213
        Align = alClient
        DataSource = dsMod
        Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
        ReadOnly = True
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnTitleClick = grdTitleClick
      end
    end
  end
  inherited dsMod: TDataSource
    Left = 587
    Top = 9
  end
  inherited setMod: TADODataSet
    Left = 617
    Top = 9
  end
end
