unit uModMasterDetail;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uModCad2, DB, ADODB, StdCtrls, Buttons, ComCtrls, ToolWin,
  Grids, DBGrids, ExtCtrls;

type
  TfModMasterDetail = class(TfModCad2)
    pnlBottom: TPanel;
    grdDetail: TDBGrid;
    pnlDetail: TPanel;
    Splitter1: TSplitter;
    dsDetail: TDataSource;
    setDetail: TADODataSet;
    procedure grdEnter(Sender: TObject);
    procedure grdDetailEnter(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure bntCancelClick(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    { Private declarations }
  protected
    bMaster: Boolean; // Indica o grid que o usu�rio selecionou [True - Master|False - Detail]
  public
    { Public declarations }

  end;

var
  fModMasterDetail: TfModMasterDetail;

implementation

{$R *.dfm}

procedure TfModMasterDetail.grdEnter(Sender: TObject);
begin
  inherited;
  grd.Color := clCream;
  grdDetail.Color := clWindow;
  pnlBrowse.Color := clMoneyGreen;
  pnlBottom.Color := clBtnFace;
end;

procedure TfModMasterDetail.grdDetailEnter(Sender: TObject);
begin
  inherited;
  grd.Color := clWindow;
  grdDetail.Color := clCream;
  pnlBrowse.Color := clBtnFace;
  pnlBottom.Color := clMoneyGreen;
end;

procedure TfModMasterDetail.btnNewClick(Sender: TObject);
begin
  if bMaster then
    inherited;
end;

procedure TfModMasterDetail.btnEditClick(Sender: TObject);
begin
  if bMaster then
    inherited;
end;

procedure TfModMasterDetail.btnSaveClick(Sender: TObject);
begin
  if bMaster then
    inherited;
end;

procedure TfModMasterDetail.bntCancelClick(Sender: TObject);
begin
  if bMaster then
    inherited;
end;

procedure TfModMasterDetail.btnDelClick(Sender: TObject);
begin
  if bMaster then
    inherited;
end;

procedure TfModMasterDetail.FormCreate(Sender: TObject);
begin
  bMaster := True;
  inherited;
  setDetail.Open;
end;

procedure TfModMasterDetail.FormDestroy(Sender: TObject);
begin
  inherited;
  setDetail.Close;
end;

end.
