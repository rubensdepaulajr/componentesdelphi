inherited fModMasterDetail: TfModMasterDetail
  Left = 337
  Top = 73
  Height = 550
  Caption = 'fModMasterDetail'
  PixelsPerInch = 96
  TextHeight = 13
  inherited StatusBar: TStatusBar
    Top = 497
  end
  inherited pnlClient: TPanel
    Height = 439
    object Splitter1: TSplitter [0]
      Left = 2
      Top = 273
      Width = 585
      Height = 3
      Cursor = crVSplit
      Align = alBottom
    end
    inherited pnlBrowse: TPanel
      Height = 206
      BorderWidth = 2
      Color = clMoneyGreen
      inherited grd: TDBGrid
        Left = 4
        Top = 4
        Width = 577
        Height = 198
        Color = clCream
        OnEnter = grdEnter
      end
    end
    object pnlBottom: TPanel
      Left = 2
      Top = 276
      Width = 585
      Height = 161
      Align = alBottom
      BevelInner = bvRaised
      BevelOuter = bvLowered
      BorderWidth = 2
      TabOrder = 2
      object grdDetail: TDBGrid
        Left = 4
        Top = 69
        Width = 577
        Height = 88
        Align = alClient
        DataSource = dsDetail
        TabOrder = 0
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -11
        TitleFont.Name = 'MS Sans Serif'
        TitleFont.Style = []
        OnEnter = grdDetailEnter
      end
      object pnlDetail: TPanel
        Left = 4
        Top = 4
        Width = 577
        Height = 65
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        TabOrder = 1
        Visible = False
      end
    end
  end
  inherited dsMod: TDataSource
    Top = 4
  end
  inherited setMod: TADODataSet
    Top = 4
  end
  object dsDetail: TDataSource
    DataSet = setDetail
    Left = 587
    Top = 33
  end
  object setDetail: TADODataSet
    Parameters = <>
    Left = 617
    Top = 33
  end
end
