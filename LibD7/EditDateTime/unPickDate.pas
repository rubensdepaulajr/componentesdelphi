unit unPickDate;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, Grids, Calendar, ComCtrls, Spin, ExtCtrls;

type
  TfoPickDate = class(TForm)
    EDay: TSpinEdit;
    EMonth: TComboBox;
    EYear: TSpinEdit;
    Calendar: TCalendar;
    Bevel1: TBevel;
    SpeedButton1: TSpeedButton;
    Bevel2: TBevel;
    Bevel3: TBevel;
    btnOK: TSpeedButton;
    SpeedButton3: TSpeedButton;
    SpeedButton4: TSpeedButton;
    procedure CalendarChange(Sender: TObject);
    procedure CalendarDblClick(Sender: TObject);
    procedure EDayChange(Sender: TObject);
    procedure EMonthChange(Sender: TObject);
    procedure EYearChange(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure SpeedButton4Click(Sender: TObject);
    procedure SpeedButton3Click(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
    DatePick : TDateTime;
  public
    { Public declarations }
    dDate : TDateTime;
  end;

var
  foPickDate: TfoPickDate;


implementation

{$R *.DFM}
uses EditDateTime;

procedure TfoPickDate.CalendarChange(Sender: TObject);
begin
 EMonth.Text:=EMonth.Items[Calendar.Month-1];
 EDay.Value:=Calendar.Day;
 EYear.Value:=Calendar.Year;
 With Calendar do
   datePick :=EncodeDate(Year,Month,Day);
end;

procedure TfoPickDate.CalendarDblClick(Sender: TObject);
Begin
 btnOK.Click;
end;

procedure TfoPickDate.EDayChange(Sender: TObject);
begin
 Calendar.Day:=EDay.Value;
end;

procedure TfoPickDate.EMonthChange(Sender: TObject);
begin
 Calendar.Month:=EMonth.ItemIndex+1;
end;

procedure TfoPickDate.EYearChange(Sender: TObject);
begin
  Calendar.Year:=EYear.Value;
end;


procedure TfoPickDate.FormShow(Sender: TObject);
Var
 y,m,d : Word;
begin
 DatePick := dDate;

 DecodeDate(DatePick,y,m,d);
 With Calendar do
  Begin
   Day:=D;
   Month:=M;
   Year:=Y;
  End;
 EDay.Value:=d;
 EMonth.Text:=EMonth.Items[m-1];
 EYear.Value:=y;

 Calendar.SetFocus;
end;



procedure TfoPickDate.SpeedButton1Click(Sender: TObject);
Var
 y,m,d : Word;
begin
 DatePick:=Now;

 DecodeDate(DatePick,y,m,d);
 With Calendar do
  Begin
   Day:=D;
   Month:=M;
   Year:=Y;
  End;
 EDay.Value:=d;
 EMonth.Text:=EMonth.Items[m-1];
 EYear.Value:=y;

end;

procedure TfoPickDate.FormCreate(Sender: TObject);
begin
  { define initial date }
 // if TDateEditCal( Self ).Text <> '' then
    // m_CurrentDateSelected := StrToDate( TDateEditCal( ctlParent ).Text )
  //else
    // m_CurrentDateSelected := Date;
end;

procedure TfoPickDate.SpeedButton4Click(Sender: TObject);
begin
 Tag :=  0;
  Close;
end;

procedure TfoPickDate.SpeedButton3Click(Sender: TObject);
begin
  Tag:= -1;
   Close;
end;

procedure TfoPickDate.btnOKClick(Sender: TObject);
Var
 m,y,d : Word;
begin
 With Calendar do
  Begin
   m:=Month;
   d:=Day;
   Y:=Year;
  End;
 dDate:=EnCodeDate(y,m,d);
 Tag := 1;
 Close;
end;

end.
