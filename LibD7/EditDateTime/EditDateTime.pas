unit EditDateTime;

(*********************************************


A date edit field with drop down calendar.

PROPERTIES:

Date - TDateTime that contains the date value of the control.

ValidDateColor - The color that "valid dates" will be rendered.

METHODS:

procedure AddValidDate - Adds a datetime value to a list of "valid dates" maintained by the
control.  These dates will be drawn in the color specified by ValidDateColor.

procedure ClearValidDates - Clears all "valid dates" from the list.

function DateInList - Checks if the specified date is in the list of "valid dates".

EVENTS:

OnDateChange - Triggered whenever the Date property is updated.
*********************************************)

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, StdCtrls, unPickDate, Buttons, IniFiles, ExtCtrls;
type

  PTDateTime = ^TDateTime;
  TDateButton = class( TBitBtn )
  private
  protected
  public
     procedure Click; override;
  published
  end;

  TEditDateTime = class( TLabeledEdit )
  private
     FButton: TDateButton;
     FDate: TDateTime;
     FOnDateChange: TNotifyEvent;
     FValColor: TColor;
     FResultQuoted : boolean;
     lstDates: TList;
     sSep: string[1];
     sDateFmt: string[20];
     function fDt(sTp : String;dDateTime : TDateTime): String;

  protected
     nSep1, nSep2: integer;
     procedure WMSize( var Message: TWMSize ); message WM_SIZE;
     function GetDate: TDateTime;
     procedure SetDate( dtArg: TDateTime );
     procedure KeyPress( var Key: char ); override;
     procedure Change;Override;
     procedure DoExit; override;
     procedure DoEnter; override;

  public
     constructor Create( AOwner: TComponent ); override;
     destructor Destroy; override;
     procedure CreateParams( var Params: TCreateParams ); override;
     property Date: TDateTime read GetDate write SetDate;
     function StrCanonicDateHR : String;
     function StrCanonicDatePM : String;
     function StrCanonicDateAM : String;
     function StrCanonicDateDT   : String;
     function DateInList( dt: TDateTime ): boolean;
     procedure AddValidDate( dt: TDateTime );
     procedure ClearValidDates;
  published
     property OnDateChange: TNotifyEvent read FOnDateChange write FOnDateChange;
     property ValidDateColor: TColor read FValColor write FValColor default clMaroon;
     property ResultQuoted : Boolean read FResultQuoted write FResultQuoted default True;
  end;

procedure Register;

var
  frmCalendar: TfoPickDate;

implementation

{$R EDITDATETIME.Res }

{--- TDateButton ---}
procedure TDateButton.Click;
var
  editParent: TEditDateTime;
begin
  editParent := TEditDateTime(Parent);
  frmCalendar := TfoPickDate.Create( editParent );
  //frmCalendar.Left := editParent.Left;
  //frmCalendar.Top  := editParent.Top + editParent.Height;
  if editParent.Text = '' then
    frmCalendar.dDate := Now
  else
    frmCalendar.dDate := editParent.Date;
  frmCalendar.ShowModal;
  if frmCalendar.Tag = 1 then
    begin
      editParent.Date := frmCalendar.dDate;
      editParent.color := clInfoBk;
    end
  else
  if frmCalendar.Tag = 0 then
    begin
      editParent.Text := '';
      editParent.Date := 0;
    end;
  frmCalendar.Free;
  inherited Click;
end;

{--- TEditDateTime ---}

constructor TEditDateTime.Create( AOwner: TComponent );
begin
  inherited Create( AOwner );
  sSep := '/';
  sDateFmt := 'dd/mm/yyyy';
  FDate := 0.0;
  FButton := TDateButton.Create( self );
  FButton.Visible := TRUE;
  FButton.Parent := self;
  FButton.Glyph.Handle :=   LoadBitmap ( hInstance, 'BTNDATETIME' );
  ControlStyle := ControlStyle - [csSetCaption];
  lstDates := TList.Create;
  FValColor := clBlue;
  FResultQuoted := True;
end;

procedure TEditDateTime.CreateParams( var Params: TCreateParams );
begin
  inherited CreateParams( Params );
  Params.Style := Params.Style or WS_CLIPCHILDREN;
end;

destructor TEditDateTime.Destroy;
begin
  FButton := nil;
  ClearValidDates;
  lstDates.Free;
  inherited Destroy;
end;

procedure TEditDateTime.WMSize( var Message: TWMSize );
begin
  FButton.Height := Height -2 ;
  FButton.Width := FButton.Height;
  FButton.Left := Width - FButton.Width -1;
  FButton.Refresh;
//  if FDate = 0.0 then
//     Date := Now;
end;

function TEditDateTime.GetDate: TDateTime;
begin
  GetDate := FDate;
end;

procedure TEditDateTime.SetDate( dtArg: TDateTime );

begin
  if FDate <> dtArg then
     begin
        FDate := dtArg;
        Modified := TRUE;
        if FDate = 0 then
           Text := ''
        else
           Text := FormatDateTime( sDateFmt, FDate );
        if Assigned( FOnDateChange ) then
           FOnDateChange( self );
     end;
end;

procedure TEditDateTime.DoEnter;
begin

end;

procedure TEditDateTime.DoExit;
begin
  inherited DoExit;
  try
    Date := StrToDate(Self.Text );
  except
     if Self.Text <> '' then  //Para deixar a data em branco
       //Date := Now;
       Self.Text := '';
     //SetFocus;
  end;
end;

(*********************************************
Is the supplied data in the date list?
*********************************************)
function TEditDateTime.DateInList( dt: TDateTime ): boolean;
var
  pDate: PTDateTime;
  i: integer;
begin
  Result := FALSE;
  for i := 0 to lstDates.Count - 1 do
     begin
        pDate := lstDates[i];
        if pDate^ = dt then
           begin
              Result := TRUE;
              Break;
           end;
     end;
end;

(*********************************************
Maintain list of valid dates.
*********************************************)
procedure TEditDateTime.AddValidDate( dt: TDateTime );
var
  pDate: PTDateTime;
begin
  New( pDate );
  pDate^ := dt;
  lstDates.Add( PDate );
end;

procedure TEditDateTime.ClearValidDates;
var
  pDate: PTDateTime;
begin
  while lstDates.Count > 0 do
     begin
        pDate := lstDates[0];
        Dispose( pDate );
        lstDates.Delete( 0 );
     end;
end;




procedure TEditDateTime.Change;
Var I : Integer;
    sText,sTemp : String;
    Separator : boolean;
begin
  sTemp := '';
  Separator := false;
  sText := Self.Text;
  For i := 1 To Length(sText) do
    begin
      sTemp := sTemp + sText[i];
      if ((i = 2)and(sText[i+1] <> '/'))or((i = 5)and(sText[i+1] <> '/')) then
        begin
          sTemp := sTemp +  '/';
          Separator := true;
        end;
    end;
  Self.Text := sTemp;
  if Separator then
    Self.SelStart := Length(Self.text);
end;










procedure TEditDateTime.KeyPress( var Key: char );
begin
  if (Not (Key in ['0'..'9',#8]))or((Length(Self.Text)>=10)and (Not(Self.Focused))) then
     Key := #0
  else
    inherited KeyPress( Key );
end;



function TEditDateTime.fDt(sTp : String;dDateTime : TDateTime): String;
Var iDia,iMes,iAno,iHor,iMin,iSeg,iMis : Word;
    sHor : String[8];
    sAno : String[4];
    sDat : String[17];
function AddZeros (sTexto : String;iTamanho : Integer;iIniFim : Char) : String;
begin
  While Length(sTexto) < iTamanho do
    If iIniFim = 'I' then
      sTexto := '0' + sTexto
    else
      sTexto := sTexto + '0';
  Result := sTexto;
end;
begin
  DecodeDate(dDateTime,iAno,iMes,iDia);
  DecodeTime(dDateTime,iHor,iMin,iSeg,iMis);
  sHor := AddZeros(IntToStr(iHor),2,'I') +':'+ AddZeros(IntToStr(iMin),2,'I') +':'+ AddZeros(IntToStr(iSeg),2,'I');
  sDat := AddZeros(InttoStr(iMes),2,'I')+ AddZeros(IntToStr(iDia),2,'I');
  sAno := InttoStr(iAno);
  if Length(sAno) < 4 then
    sAno := '20' + sAno;

  if sTp = 'DT00' then //Somente Data
    Result := sAno + sDat
  else
  if sTp = 'DTAM' then //Data e hora antes de meio dia
    Result := sAno + sDat + ' 00:00:00'
  else
  if sTp = 'DTPM' then //Data e hora depois de meio dia
    Result := sAno + sDat + ' 23:59:00'
  else
  if sTp = 'HR00' then //Somente Hora
    Result := sHor;
  if FResultQuoted then
    Result := QuotedStr(Result);
end;


 function TEditDateTime.StrCanonicDateDT   : String;
 Begin
    if Trim(Self.Text) = '' then
      Result := ''
    else
      Result := fDt('DT00',StrToDateTime(Self.Text));
 end;

 function TEditDateTime.StrCanonicDateAM : String;
 Begin
     if Trim(Self.Text) = '' then
      Result := ''
    else
      Result := fDt('DTAM',StrToDateTime(Self.Text));
 end;

 function TEditDateTime.StrCanonicDatePM : String;
 Begin
      if Trim(Self.Text) = '' then
      Result := ''
    else
      Result := fDt('DTPM',StrToDateTime(Self.Text));
 end;
  function TEditDateTime.StrCanonicDateHR : String;
 Begin
      if Trim(Self.Text) = '' then
      Result := ''
    else
      Result := fDt('HR00',StrToDateTime(Self.Text));
 end;




procedure Register;
begin
  RegisterComponents('Extras', [TEditDateTime]);
end;

end.
