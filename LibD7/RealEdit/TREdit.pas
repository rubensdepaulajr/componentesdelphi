unit TREdit;
interface
uses
	StdCtrls, Classes, SysUtils, Controls	, Forms, Graphics, Menus, Messages;
type
	TRealFormat = (fNumber ,fMoney, fFixed);

	TRealEdit = class(TCustomMemo)
	private
    { Private declarations }
	 ds,sinal : Boolean;
    procedure SetValue(valor: Double);
    function  GetValue: Double;
	 function  TrimAll(s : String): String;
	 procedure SetFormat(Value: TRealFormat);
	 procedure SetFSize(Value: String);
	 procedure OnFim(Sender: TObject);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
	protected
	 ftipo: TRealFormat;
    Fsize: String;
    procedure KeyPress(var Key: Char); override;
    { Public declarations }
	public
		constructor Create( AOwner: TComponent ); override;
		property Value: Double read GetValue write SetValue;
	published
    property Align;
    property Alignment;
    property BorderStyle;
    property Color;
    property Ctl3D;
    property DragCursor;
    property DragMode;
    property Enabled;
    property Font;
    property HideSelection;
    property MaxLength;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ReadOnly;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property WantTabs;
	 property FormatReal: TRealFormat read FTipo write SetFormat;
	 property FormatSize: String read FSize write SetFSize;

    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDrag;

  end;

procedure Register;

implementation

{$R TREdit32.res}


constructor TRealEdit.Create( AOwner: TComponent );
begin
  inherited Create( AOwner );
  Height		:= 21;
  Width		:= 121;
  WordWrap		:= False;
  Alignment	:= taRightJustify;
  Value		:= 0.0;
  FSize		:= '10.2';
  FormatReal	:= fNumber;
  OnExit		:= OnFim;
  ds := False;
  sinal := False;
end;

procedure TRealEdit.OnFim(Sender: TObject);
begin
	SetValue(GetValue);
end;

procedure TRealEdit.SetFormat(Value: TRealFormat);
begin
  if Value <> FTipo then
  begin
    FTipo := Value;
    Invalidate;
  end;
end;

procedure TRealEdit.SetFSize(Value: String);
begin
  if Value <> FSize then
  begin
    FSize := Value;
  end;
end;

procedure TRealEdit.SetValue(valor: Double);
var	ff : String;
begin
	if FTipo = fNumber then ff := '%'+ FSize + 'n';
	if FTipo = fMoney  then ff := '%'+ FSize + 'm';
	if FTipo = fFixed  then ff := '%'+ FSize + 'f';
	Text := TrimAll(format(ff,[valor])); //##
end;

function  TRealEdit.GetValue: Double;
var aux : String;
begin
	aux := Text;
	if aux = '-' then
        aux := '0.00';
	result := StrToFloatDef(TrimAll(aux),0);
end;

function TRealEdit.TrimAll(s : String): String;
var i : Integer;
begin
  i := length(s);
  while Pos(' ', S) > 0 do
    Delete (s, Pos(' ', S), 1);
  while Pos(ThousandSeparator, S) > 0 do
    Delete (s, Pos(ThousandSeparator, S), 1);
  i := Pos(CurrencyString, S);
  if i > 0 then
    Delete(S, I, Length(CurrencyString));
  result := Trim(s); // ##
end;

procedure TRealEdit.CMEnter(var Message: TCMEnter);
begin
  SelectAll;
  inherited;
end;

procedure TRealEdit.KeyPress(var Key: Char);
var
   aux : String;
begin
	if key = ThousandSeparator then
   	key := DecimalSeparator;

	if Not (Key in ['0'..'9',DecimalSeparator,'-',chr(8)]) Then
   begin
		Key := #0;
		inherited KeyPress(Key);
       exit;
   end;

   aux := DecimalSeparator;
	if key = DecimalSeparator then
		if Pos(aux,Text) <> 0 then Key := #0;

	if key = '-' then
		if Length(Text) > 0 then
       	if Self.SelLength = Length(Text) then
					key := '-'
			else	Key := #0;

	inherited KeyPress(Key);
end;

procedure Register;
begin
  RegisterComponents('Extras', [TRealEdit]);
end;

 end.



