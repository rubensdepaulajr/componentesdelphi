unit McValDlg;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, Buttons, Grids, McValida, ExtCtrls;

type
  TFormValidDlg = class(TForm)
    lstErrors: TListBox;
    BtnProssegue: TBitBtn;
    BtnCancelar: TBitBtn;
    BtnOk: TBitBtn;
    Panel1: TPanel;
    Bevel1: TBevel;
    Panel2: TPanel;
    Bevel2: TBevel;
    btnImpValid: TSpeedButton;
    btnLogValid: TSpeedButton;
    Label1: TLabel;
    Label2: TLabel;
    Bevel3: TBevel;
    Bevel4: TBevel;
    lblNumErros: TLabel;
    lblNumAlertas: TLabel;
    procedure lstErrorsDrawItem(Control:TWinControl;Index:Integer;
      Rect:TRect;State:TOwnerDrawState);
    procedure FormCreate(Sender:TObject);
    procedure FormDestroy(Sender:TObject);
    procedure FormShow(Sender:TObject);
    procedure lstErrorsMeasureItem(Control:TWinControl;Index:Integer;
      var Height:Integer);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FErrorBmp:TBitmap;  // Usado para etNormal, etValidation, e etOther
    FWarningBmp:TBitmap;
    FCriticalBmp:TBitmap;
    FSystemBmp:TBitmap;
    FNumError:Integer;
    FNumWarning:Integer;
    //FListErrors:TStrings;
  public
    Function  NumberOfErrors:Integer;
    Procedure ClearMessages;
    Procedure AddError(const Msg:string;ValidationType:TValidationType);
    Procedure SetButtons(Proceed:Boolean);
  end;

var
  FormValidDlg: TFormValidDlg;

implementation

{$R *.DFM}
{$R MCVALRES}

{------------------------------------------------------------------------------}
procedure TFormValidDlg.FormCreate(Sender: TObject);
begin
  FErrorBmp := TBitmap.Create;
  FErrorBmp.Handle    := LoadBitmap(HInstance,   'ET_ERROR');
  FWarningBmp := TBitmap.Create;
  FWarningBmp.Handle  := LoadBitmap(HInstance, 'ET_WARNING');
  FCriticalBmp := TBitmap.Create;
  FCriticalBmp.Handle := LoadBitmap(HInstance,'ET_CRITICAL');
  FSystemBmp := TBitmap.Create;
  FSystemBmp.Handle   := LoadBitmap(HInstance,  'ET_SYSTEM');

  //FListErrors := TStringList.Create;

	FNumError   := 0;
  FNumWarning := 0;
end;
{------------------------------------------------------------------------------}
procedure TFormValidDlg.FormDestroy(Sender: TObject);
begin
  FErrorBmp.Free;
  FWarningBmp.Free;
  FCriticalBmp.Free;
  FSystemBmp.Free;
	// Lista de erros
  //FListErrors.Free;
end;
{------------------------------------------------------------------------------}
procedure TFormValidDlg.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	//Action := caFree;
end;
{------------------------------------------------------------------------------}
procedure TFormValidDlg.ClearMessages;
begin
	IF self.LstErrors <> nil Then
	  self.LstErrors.Clear;
	{IF FListErrors <> nil Then
	  FListErrors.Clear;}
	FNumError   := 0;
  FNumWarning := 0;
end;
{------------------------------------------------------------------------------}
function TFormValidDlg.NumberOfErrors:Integer;
begin
	{IF FListErrors <> nil Then
	  Result := FListErrors.Count
  Else
  	Result := 0;}
	IF self.lstErrors <> nil Then
	  Result := self.LstErrors.Items.Count
  Else
  	Result := 0;
end;
{------------------------------------------------------------------------------}
procedure TFormValidDlg.AddError(const Msg:String;ValidationType:TValidationType);
begin
	IF self.lstErrors <> nil Then
  	Begin
      self.lstErrors.Items.AddObject(Msg,TObject(ValidationType));
      Case ValidationType of
        etWarning: Inc(FNumWarning);
        etAbort:   Inc(FNumError);
        etSystem:  Inc(FNumError);
        etError:   Inc(FNumError);
      End;
      lblNumErros.Caption   := IntToStr(FNumError);
      lblNumAlertas.Caption := IntToStr(FNumWarning);
    End;

	{IF FListErrors <> nil Then
  	Begin
      FListErrors.AddObject(Msg,TObject(ValidationType));
      Case ValidationType of
        etWarning: Inc(FNumWarning);
        etAbort:   Inc(FNumError);
        etSystem:  Inc(FNumError);
        etError:   Inc(FNumError);
      End;
      lblNumErros.Caption   := IntToStr(FNumError);
      lblNumAlertas.Caption := IntToStr(FNumWarning);
    End;}
end;
{------------------------------------------------------------------------------}
procedure TFormValidDlg.lstErrorsDrawItem(Control:TWinControl;Index:Integer;
	Rect:TRect;State:TOwnerDrawState );
Var Bmp:TBitmap;
		R,DestRct,SrcRct:TRect;
		TransColor:TColor;
	  ItemStz:array[0..255] of Char;

  Function GetTransparentColor(B:TBitmap):TColor;
  	Begin
			Result := B.Canvas.Pixels[0,B.Height - 1];
    End;

begin
  With LstErrors.Canvas do
	  begin
  	  Bmp := TBitmap.Create;
    	try
      	FillRect( Rect );  //Limpa a �rea de dezenho do listbox
	      DestRct := Classes.Rect(0,0,18,18);
  	    SrcRct := DestRct;
      	//N�o esque�a de fixar a Largura e Altura de bitmap de destino.
	      Bmp.Width  := 18;
  	    Bmp.Height := 18;

	      IF odSelected in State then
	        Bmp.Canvas.Brush.Color := clHighlight
  	    else
    	    Bmp.Canvas.Brush.Color := LstErrors.Color;

	      case TValidationType(LstErrors.Items.Objects[Index]) of
  	      etWarning:
            begin
              TransColor := GetTransparentColor(FWarningBmp);
              Bmp.Canvas.BrushCopy(DestRct,FWarningBmp,SrcRct,TransColor);
            end;
        	etAbort:
            begin
              TransColor := GetTransparentColor(FCriticalBmp);
              Bmp.Canvas.BrushCopy(DestRct,FCriticalBmp,SrcRct,TransColor);
            end;
	        etSystem:
            begin
              TransColor := GetTransparentColor(FSystemBmp);
              Bmp.Canvas.BrushCopy(DestRct,FSystemBmp,SrcRct,TransColor);
            end;
	    	else
  		  	begin
						Inc(FNumError);
          	TransColor := GetTransparentColor(FErrorBmp);
	          Bmp.Canvas.BrushCopy(DestRct,FErrorBmp,SrcRct,TransColor);
  	      end;
      	end;
	      Draw(Rect.Left + 4,Rect.Top + 2,Bmp);
	      R := Rect;
  	    Inc(R.Left,24);
    	  Inc(R.Top,2);
      	StrPCopy(ItemStz,LstErrors.Items[Index]);
	      DrawText(Handle,ItemStz,-1,R,dt_WordBreak or dt_ExpandTabs);
  	  finally
    	  Bmp.Free;
	    end;
  	end;
end;
{------------------------------------------------------------------------------}
procedure TFormValidDlg.lstErrorsMeasureItem(Control:TWinControl;Index:Integer;
	var Height : Integer );
Var R:TRect;
	  ItemStz:array[ 0..255 ] of Char;
  	H:Integer;
begin
  R := Rect( 0, 0, lstErrors.Width - 24, 2 );
  StrPCopy( ItemStz, lstErrors.Items[ Index ] );
  H := DrawText(LstErrors.Canvas.Handle,ItemStz, -1, R,
  			dt_CalcRect or dt_WordBreak or dt_ExpandTabs );
  if H < 20 then
    H := 20;
  Height := H + 6;
end;
{------------------------------------------------------------------------------}
procedure TFormValidDlg.FormShow(Sender: TObject);
begin
	//LstErrors.Items.Assign(FListErrors);
  LstErrors.SetFocus;
end;
{------------------------------------------------------------------------------}
procedure TFormValidDlg.SetButtons(Proceed: Boolean);
begin
	btnProssegue.Visible := Proceed;
  btnCancelar.Visible  := Proceed;
  btnOk.Visible        := not Proceed;
end;
{------------------------------------------------------------------------------}
procedure TFormValidDlg.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
	IF Key = VK_ESCAPE Then
  	Begin
      Key := 0;
      ModalResult := mrCancel;
    End;
end;
{------------------------------------------------------------------------------}
end.
