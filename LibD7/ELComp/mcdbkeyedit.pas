{ *****************************************************************************
  Nome
    mcDbKeyEdit.pas
  Descri��o
    Unit do Componente TMcDbKeyEdit;
  Par�metros
    -
  Notas
    -
  Hist�rico de Modifica��es
  ----------------------------------------------------------------------------
  Nome                    Data        Coment�rio
  ---------------------  ----------  -----------------------------------------
  Julierme U. Machado    08/11/2004   Publicado a Propriedade EditMask;
                                      Declara��o da Vari�vel FExecDataChange
                                      Para Controle de M�scaras no M�todo
                                      DataChange;
 ***************************************************************************** }
 
unit mcDbKeyEdit;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, DBCtrls, Mask, Db, dbtables;

type
	{Eventos Personalizadaos}
	TAfterKeyEditDisable = procedure(Sender: TObject; DataSet: TDataSet) of Object;

	// Pr� Declara��o de Classes
	TFillChar = Class;

  {TListComponents}
  TListComponents = class(TPersistent)
  private
  	FListComponents: TStrings;
    function  GetCount: Integer;
		//Leitura o arquivo DFM
  	procedure ReadListComponents(Reader: TReader);
  	procedure WriteListComponents(Writer: TWriter);
	protected
    function  GetStrings(Index: Integer): String; virtual;
    procedure SetStrings(Index: Integer; Value: String); virtual;
  	procedure DefineProperties(Filer: TFiler); override;
  public
  	constructor Create;
    destructor Destroy; override;
    function  Add(const S: String): Integer;
    function  ListLocateComponent(Value: TComponent):Boolean;
    procedure AddStrings(Strings: TStrings);
    procedure Clear;

    property Count: Integer read GetCount;
    property Strings[Index: Integer]: String read GetStrings write SetStrings;
	end;

  TMcDbKeyEdit = class(TCustomMaskEdit)
  private
    { private declarations }
    FExecDataChange: Boolean;
    FFieldDataLink: TFieldDataLink;
    FListComponents: TListComponents;
    FCanvas: TControlCanvas;
		FFillChar: TFillChar;
    FAlignment: TAlignment;
    FFocused: Boolean;
    FFindRecordAuto:Boolean;
    FOnAfterKeyEditDisable: TAfterKeyEditDisable;
    procedure SetListComponents(Value: TListComponents);
    procedure DataChange(Sender: TObject);
    procedure EditingChange(Sender: TObject);
    function GetDataField: string;
    function GetDataSource: TDataSource;
    function GetField: TField;
    function GetReadOnly: Boolean;
    function GetTextMargins: TPoint;
    procedure ResetMaxLength;
    procedure SetFillChar(Value: TFillChar);
    procedure SetDataField(const Value: string);
    procedure SetDataSource(Value: TDataSource);
    procedure SetFocused(Value: Boolean);
    procedure SetReadOnly(Value: Boolean);
    procedure UpdateData(Sender: TObject);
    procedure WMCut(var Message: TMessage); message WM_CUT;
    procedure WMPaste(var Message: TMessage); message WM_PASTE;
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    procedure CMExit(var Message: TCMExit); message CM_EXIT;
    procedure WMPaint(var Message: TWMPaint); message WM_PAINT;
    procedure CMGetDataLink(var Message: TMessage); message CM_GETDATALINK;
    procedure CMFocusChanged(var Message: TCMFocusChanged); message CM_FOCUSCHANGED;
  protected
    { protected declarations }
    procedure Change; override;
    function EditCanModify: Boolean; override;
    procedure KeyDown(var Key: Word; Shift: TShiftState); override;
    procedure KeyPress(var Key: Char); override;
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent;Operation: TOperation); override;
    procedure Reset; override;
    procedure FindRecord; virtual;
  public
    { public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function ExecuteAction(Action: TBasicAction): Boolean; override;
    function UpdateAction(Action: TBasicAction): Boolean; override;
    function UseRightToLeftAlignment: Boolean; override;
    property Field: TField read GetField;
  published
    { Published declarations }
    property Anchors;
    property AutoSelect;
    property AutoSize;
    property BiDiMode;
    property BorderStyle;
    property CharCase;
    property Color;
    property Constraints;
    property Ctl3D;
    property DataField: string read GetDataField write SetDataField;
    property DataSource: TDataSource read GetDataSource write SetDataSource;
    property DragCursor;
    property DragKind;
    property DragMode;
    property EditMask;
    property Enabled;
    property FillChar: TFillChar  read FFillChar  write SetFillChar stored True;
    property FindRecordAuto: Boolean read FFindRecordAuto write FFindRecordAuto stored True;
    property Font;
    property ImeMode;
    property ImeName;
		property ListComponents: TListComponents read FListComponents write SetListComponents;
    property MaxLength;
    property ParentBiDiMode;
    property ParentColor;
    property ParentCtl3D;
    property ParentFont;
    property ParentShowHint;
    property PasswordChar;
    property PopupMenu;
    property ReadOnly: Boolean read GetReadOnly write SetReadOnly default False;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnAfterKeyEditDisable: TAfterKeyEditDisable read FOnAfterKeyEditDisable write FOnAfterKeyEditDisable;
    property OnChange;
    property OnClick;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;

	{ TFillChar }
  EInvalidFillChar = Class(Exception);

  TFillChar = Class(TPersistent)
 	private
  	FChar: Char;
    FFillSize: Integer;
    function  Replicate(Value: String): String;
    procedure SetChar(Value: Char);
    procedure SetFillSize(Value: Integer);
  public
    constructor Create;
    function Value(Text: String): String;
  published
    property Char: Char read FChar write SetChar;
    property FillSize: Integer read FFillSize write SetFillSize;
  end;

procedure Register;

implementation

uses Clipbrd, DBConsts;

{Pesquisa se o c�digo j� existe no cadastro do componente----------------------}
procedure TMcDbKeyEdit.FindRecord; Var TableFind: TQuery; FindText: String;
begin
  With Self, TQuery(Self.DataSource.DataSet) Do
  begin
    TableFind             := TQuery.Create(Nil);
    TableFind.Active      := False;
    TableFind.DatabaseName:= DatabaseName;
    TableFind.SessionName := SessionName;
    TableFind.Sql.Text    := Sql.Text;
    TableFind.Params.AssignValues(Params);
    TableFind.Active      := True;
    if TableFind.Locate(DataField, EditText, [loPartialKey]) Then
    begin
      FindText            := EditText;
      Cancel;
      Locate(DataField, FindText, [loPartialKey]);
    end;
    TableFind.Free;
  end;
end;

{------------------------------------------------------------------------------}
{--- TNullValidation ----------------------------------------------------------}
constructor TListComponents.Create;
begin
	FListComponents:= TStringList.Create;
end;

destructor TListComponents.Destroy;
begin
	FListComponents.Clear;
	FListComponents:= nil;
	FListComponents.Free;

	inherited Destroy;
end;

function TListComponents.GetStrings(Index: Integer): String;
begin
	Result:= FListComponents.Strings[Index];
end;

procedure TListComponents.SetStrings(Index: Integer; Value: String);
begin
	FListComponents.Strings[Index]:= Value;
end;

procedure TListComponents.DefineProperties(Filer: TFiler);
begin;
	Inherited DefineProperties(Filer);
	Filer.DefineProperty('ListComponents',ReadListComponents,WriteListComponents,FListComponents.Count > 0);
end;

function TListComponents.GetCount: Integer;
begin
	Result:= FListComponents.Count;
end;

procedure TListComponents.ReadListComponents(Reader: TReader);
begin
	Reader.ReadListBegin;
  FListComponents.Clear;
  While not Reader.EndOfList do
	  FListComponents.Add(Reader.ReadString);
  Reader.ReadListEnd;
end;

procedure TListComponents.WriteListComponents(Writer: TWriter);
Var I: Integer;
begin
	Writer.WriteListBegin;
  For I:= 0 to FListComponents.Count - 1 do
  	Writer.WriteString(FListComponents.Strings[I]);
	Writer.WriteListEnd;
end;

function TListComponents.Add(const S: String): Integer;
begin
	Result:= FListComponents.Add(S);
end;

procedure TListComponents.AddStrings(Strings: TStrings);
begin
	FListComponents.AddStrings(Strings);
end;

function TListComponents.ListLocateComponent(Value: TComponent): Boolean;
begin
	Result:= FListComponents.IndexOf(Value.Name) > -1;
end;

procedure TListComponents.Clear;
begin
	FListComponents.Clear;
end;



{--- TMcDbKeyEdit -------------------------------------------------------------}
procedure TMcDbKeyEdit.ResetMaxLength;
var F: TField;
begin
  if (MaxLength > 0) and Assigned(DataSource) and Assigned(DataSource.DataSet) then
  begin
    F:= DataSource.DataSet.FindField(DataField);
    if Assigned(F) and (F.DataType = ftString) and (F.Size = MaxLength) then
      MaxLength:= 0;
  end;
end;

constructor TMcDbKeyEdit.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  inherited ReadOnly:= True;

  ControlStyle:= ControlStyle + [csReplicatable];

  FExecDataChange := True;

	// Objeto de Preenchimento;
  FFillChar:= TFillChar.Create;

	// Objetos de Perda de Foco
  FListComponents:= TListComponents.Create;

  // Objetos internos do componente
  FFieldDataLink:= TFieldDataLink.Create;
  FFieldDataLink.Control:= Self;
  FFieldDataLink.OnDataChange   := DataChange;
  FFieldDataLink.OnEditingChange:= EditingChange;
  FFieldDataLink.OnUpdateData   := UpdateData;
  Enabled:= False;
end;

destructor TMcDbKeyEdit.Destroy;
begin
	FListComponents.Destroy;
  FFieldDataLink.Free;
  FFieldDataLink:= nil;
  FCanvas.Free;
  FFillChar.Destroy;

  inherited Destroy;
end;

procedure TMcDbKeyEdit.Loaded;
begin
  inherited Loaded;
  ResetMaxLength;
  if (csDesigning in ComponentState) then DataChange(Self);
end;

procedure TMcDbKeyEdit.Notification(AComponent: TComponent;Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (FFieldDataLink <> nil) and
    (AComponent = DataSource) then DataSource:= nil;
end;

function TMcDbKeyEdit.UseRightToLeftAlignment: Boolean;
begin
  Result:= DBUseRightToLeftAlignment(Self, Field);
end;

procedure TMcDbKeyEdit.KeyDown(var Key: Word; Shift: TShiftState);
begin
  inherited KeyDown(Key, Shift);
  if (Key = VK_DELETE) or ((Key = VK_INSERT) and (ssShift in Shift)) then
    FFieldDataLink.Edit;
end;

procedure TMcDbKeyEdit.KeyPress(var Key: Char);
begin
  inherited KeyPress(Key);
  if (Key in [#32..#255]) and (FFieldDataLink.Field <> nil)
  and (not FFieldDataLink.Field.IsValidChar(Key)) then
	  begin
  	  MessageBeep(0);
    	Key:= #0;
	  end;
  case Key of
    ^H, ^V, ^X, #32..#255:
      FFieldDataLink.Edit;
    #27:
      begin
        FFieldDataLink.Reset;
        SelectAll;
        Key:= #0;
      end;
  end;
end;

function TMcDbKeyEdit.EditCanModify: Boolean;
begin
  Result:= FFieldDataLink.Edit;
end;

procedure TMcDbKeyEdit.Reset;
begin
  FFieldDataLink.Reset;
  SelectAll;
end;

procedure TMcDbKeyEdit.SetFocused(Value: Boolean);
begin
  if FFocused <> Value Then
  begin
    FFocused:= Value;
    if (FAlignment <> taLeftJustify) and (not IsMasked) Then
      Invalidate;
    FFieldDataLink.Reset;
  end;
end;

procedure TMcDbKeyEdit.Change;
begin
  FFieldDataLink.Modified;
  inherited Change;
end;

function TMcDbKeyEdit.GetDataSource: TDataSource;
begin
  Result:= FFieldDataLink.DataSource;
end;

procedure TMcDbKeyEdit.SetDataSource(Value: TDataSource);
begin
  if not (FFieldDataLink.DataSourceFixed
  and (csLoading in ComponentState)) then
    FFieldDataLink.DataSource:= Value;
  if Value <> nil Then
  	Value.FreeNotification(Self);
end;

function TMcDbKeyEdit.GetDataField: string;
begin
  Result:= FFieldDataLink.FieldName;
end;

procedure TMcDbKeyEdit.SetDataField(const Value: string);
begin
  if not (csDesigning in ComponentState) then
    ResetMaxLength;
	FFieldDataLink.FieldName:= Value;
end;

function TMcDbKeyEdit.GetReadOnly: Boolean;
begin
  Result:= FFieldDataLink.ReadOnly;
end;

procedure TMcDbKeyEdit.SetReadOnly(Value: Boolean);
begin
  FFieldDataLink.ReadOnly:= Value;
end;

function TMcDbKeyEdit.GetField: TField;
begin
  Result:= FFieldDataLink.Field;
end;

procedure TMcDbKeyEdit.SetListComponents(Value: TListComponents);
begin
	if Value <> FListComponents Then
		FListComponents:= Value;
end;

procedure TMcDbKeyEdit.SetFillChar(Value: TFillChar);
begin
	if Value <> FFillChar Then
  begin
    if FFieldDataLink.Field <> nil Then
    begin
      if Value.FillSize > FFieldDataLink.Field.Size Then
        Raise EInvalidFillChar.Create('The informed value for FillSize is larger than the size of the field.');
      FFillChar:= Value;
    end;
  end;
end;

procedure TMcDbKeyEdit.DataChange(Sender: TObject);
begin
  if (not FExecDataChange) then
    Exit;
    
  if FFieldDataLink.Field <> nil then
  begin
    { Alinhamento do conte�do do edit baseado no campo }
    if FAlignment <> FFieldDataLink.Field.Alignment then
    begin
      EditText:= '';  {forces update}
      FAlignment:= FFieldDataLink.Field.Alignment;
    end;

    { Passagem da m�scara do campo }
    {J - Rotina Anterior
    EditMask:= FFieldDataLink.Field.EditMask;
    J}
    if (Trim(FFieldDataLink.Field.EditMask) <> EmptyStr) then
      EditMask:= FFieldDataLink.Field.EditMask;

    { Limita a entrada de dados ao tamanho do campo }
    if not (csDesigning in ComponentState) then
    begin
      if (FFieldDataLink.Field.DataType = ftString) and (MaxLength = 0) then
      MaxLength:= FFieldDataLink.Field.Size;
    end;

    { Define o estado do contole de acordo com o estado do DataSet }
    if FFieldDataLink.DataSet.State = dsInsert Then
      Enabled:= True
    else
      Enabled:= False;

    if FFocused and FFieldDataLink.CanModify then
    begin
      Text:= FFieldDataLink.Field.Text;
    end
    else
    begin
      {J - Rotina Anterior
      EditText:= FFieldDataLink.Field.DisplayText;
      J}
      try
        FExecDataChange := False;
        if (Trim(FFieldDataLink.Field.EditMask) <> EmptyStr) then
          EditText := FFieldDataLink.Field.DisplayText
        else
          EditText := FFieldDataLink.Field.AsString;
      finally
        FExecDataChange := True;
      end;

      if FFieldDataLink.Editing {and FDataLink.FModified} then
        Modified:= True;
    end;
  end
  else
  begin
    FAlignment:= taLeftJustify;
    if csDesigning in ComponentState then
      EditText:= Name
    else
      EditText:= '';
  end;
end;

procedure TMcDbKeyEdit.EditingChange(Sender: TObject);
begin
  inherited ReadOnly:= not FFieldDataLink.Editing;
end;

procedure TMcDbKeyEdit.UpdateData(Sender: TObject);
begin
  ValidateEdit;
  // Executa o preenchimento dos dados
	if FFillChar.FillSize > 0 Then
  	FFieldDataLink.Field.Text:= FFillChar.Value(Text)
  else
  	FFieldDataLink.Field.Text:= Text;
end;

procedure TMcDbKeyEdit.WMPaste(var Message: TMessage);
begin
  FFieldDataLink.Edit;
  inherited;
end;

procedure TMcDbKeyEdit.WMCut(var Message: TMessage);
begin
  FFieldDataLink.Edit;
  inherited;
end;

procedure TMcDbKeyEdit.CMEnter(var Message: TCMEnter);
begin
  SetFocused(True);
  inherited;
  if SysLocale.FarEast and FFieldDataLink.CanModify then
    inherited ReadOnly:= False;
end;

procedure TMcDbKeyEdit.CMExit(var Message: TCMExit);
begin
  try
    FFieldDataLink.UpdateRecord;
  except
    SelectAll;
    SetFocus;
    raise;
  end;
  SetFocused(False);
  CheckCursor;
  DoExit;
  if FFindRecordAuto Then
   Try FindRecord Except end;
end;

procedure TMcDbKeyEdit.WMPaint(var Message: TWMPaint);
const
  AlignStyle: array[Boolean, TAlignment] of DWORD =
   ((WS_EX_LEFT, WS_EX_RIGHT, WS_EX_LEFT),
    (WS_EX_RIGHT, WS_EX_LEFT, WS_EX_LEFT));
var
  Left: Integer;
  Margins: TPoint;
  R: TRect;
  DC: HDC;
  PS: TPaintStruct;
  S: string;
  AAlignment: TAlignment;
  ExStyle: DWORD;
begin
  AAlignment:= FAlignment;
  if UseRightToLeftAlignment then ChangeBiDiModeAlignment(AAlignment);
  if ((AAlignment = taLeftJustify) or FFocused) and
    not (csPaintCopy in ControlState) then
  begin
    if SysLocale.MiddleEast and HandleAllocated and (IsRightToLeft) then
    begin { This keeps the right aligned text, right aligned }
      ExStyle:= DWORD(GetWindowLong(Handle, GWL_EXSTYLE)) and (not WS_EX_RIGHT) and
        (not WS_EX_RTLREADING) and (not WS_EX_LEFTSCROLLBAR);
      if UseRightToLeftReading then ExStyle:= ExStyle or WS_EX_RTLREADING;
      if UseRightToLeftScrollbar then ExStyle:= ExStyle or WS_EX_LEFTSCROLLBAR;
      ExStyle:= ExStyle or
        AlignStyle[UseRightToLeftAlignment, AAlignment];
      if DWORD(GetWindowLong(Handle, GWL_EXSTYLE)) <> ExStyle then
        SetWindowLong(Handle, GWL_EXSTYLE, ExStyle);
    end;
    inherited;
    Exit;
  end;
{ Since edit controls do not handle justification unless multi-line (and
  then only poorly) we will draw right and center justify manually unless
  the edit has the focus. }
  if FCanvas = nil then
  begin
    FCanvas:= TControlCanvas.Create;
    FCanvas.Control:= Self;
  end;
  DC:= Message.DC;
  if DC = 0 then DC:= BeginPaint(Handle, PS);
  FCanvas.Handle:= DC;
  try
    FCanvas.Font:= Font;
    with FCanvas do
    begin
      R:= ClientRect;
      if not (NewStyleControls and Ctl3D) and (BorderStyle = bsSingle) then
      begin
        Brush.Color:= clWindowFrame;
        FrameRect(R);
        InflateRect(R, -1, -1);
      end;
      Brush.Color:= Color;
      if not Enabled then
        Font.Color:= clGrayText;
      if (csPaintCopy in ControlState) and (FFieldDataLink.Field <> nil) then
      begin
        S:= FFieldDataLink.Field.DisplayText;
        case CharCase of
          ecUpperCase: S:= AnsiUpperCase(S);
          ecLowerCase: S:= AnsiLowerCase(S);
        end;
      end else
        S:= EditText;
      if PasswordChar <> #0 then System.FillChar(S[1], Length(S), PasswordChar);
      Margins:= GetTextMargins;
      case AAlignment of
        taLeftJustify: Left:= Margins.X;
        taRightJustify: Left:= ClientWidth - TextWidth(S) - Margins.X - 1;
      else
        Left:= (ClientWidth - TextWidth(S)) div 2;
      end;
      if SysLocale.MiddleEast then UpdateTextFlags;
      TextRect(R, Left, Margins.Y, S);
    end;
  finally
    FCanvas.Handle:= 0;
    if Message.DC = 0 then EndPaint(Handle, PS);
  end;
end;

procedure TMcDbKeyEdit.CMGetDataLink(var Message: TMessage);
begin
  Message.Result:= Integer(FFieldDataLink);
end;

procedure TMcDbKeyEdit.CMFocusChanged(var Message: TCMFocusChanged);
begin
  inherited;

	if (Self.Handle <> Message.Sender.Handle)
  and (Self.ClassName <> Message.Sender.ClassName)
  and (Message.Sender.Enabled) Then
  begin
    if FListComponents.ListLocateComponent(Message.Sender) Then
    begin
      if (Self.FFieldDataLink.Field <> nil)
      and (not Self.FFieldDataLink.Field.IsNull) Then
      begin
        Self.Enabled:= False;
        if Assigned(FOnAfterKeyEditDisable) Then
          FOnAfterKeyEditDisable(Self,Self.FFieldDataLink.DataSet);
      end;
    end;
  end;

  Self.SelectAll;
end;

function TMcDbKeyEdit.GetTextMargins: TPoint;
var
  DC: HDC;
  SaveFont: HFont;
  I: Integer;
  SysMetrics, Metrics: TTextMetric;
begin
  if NewStyleControls then
  begin
    if BorderStyle = bsNone then I:= 0 else
      if Ctl3D then I:= 1 else I:= 2;
    Result.X:= SendMessage(Handle, EM_GETMARGINS, 0, 0) and $0000FFFF + I;
    Result.Y:= I;
  end
  else
  begin
    if BorderStyle = bsNone then I:= 0 else
    begin
      DC:= GetDC(0);
      GetTextMetrics(DC, SysMetrics);
      SaveFont:= SelectObject(DC, Font.Handle);
      GetTextMetrics(DC, Metrics);
      SelectObject(DC, SaveFont);
      ReleaseDC(0, DC);
      I:= SysMetrics.tmHeight;
      if I > Metrics.tmHeight then I:= Metrics.tmHeight;
      I:= I div 4;
    end;
    Result.X:= I;
    Result.Y:= I;
  end;
end;

function TMcDbKeyEdit.ExecuteAction(Action: TBasicAction): Boolean;
begin
  Result:= inherited ExecuteAction(Action) or (FFieldDataLink <> nil) and
    FFieldDataLink.ExecuteAction(Action);
end;

function TMcDbKeyEdit.UpdateAction(Action: TBasicAction): Boolean;
begin
  Result:= inherited UpdateAction(Action) or (FFieldDataLink <> nil) and
    FFieldDataLink.UpdateAction(Action);
end;

{------------------------------------------------------------------------------}
{ TFillChar }
{------------------------------------------------------------------------------}
constructor TFillChar.Create;
begin
	inherited Create;
	FChar:= ' ';
  FFillSize:= 0;
end;

function TFillChar.Value(Text: String): String;
begin
	if FFillSize > 0 Then
  	Result:= Replicate(Text);
end;

function TFillChar.Replicate(Value: String): String;
Var I,Size: Integer;
begin
	Result:= '';
	if Length(Value) <= FFillSize Then
  	Size:= FFillSize - Length(Value)
  else
  	Size:= Length(Value);
  For I:= 1 To Size do
  	Result:= Result + FChar;
  Result:= Trim(Result) + Value;
end;

procedure TFillChar.SetChar(Value: Char);
begin
	if Value <> FChar Then
  	FChar:= Value;
end;

procedure TFillChar.SetFillSize(Value: Integer);
begin
	if Value < 0 Then
  	Raise EInvalidFillChar.Create('The property "FillSize" only allows positive values');
	if Value <> FFillSize Then
  	FFillSize:= Value;
end;


{------------------------------------------------------------------------------}
procedure Register;
begin
  RegisterComponents('McBizComps', [TMcDbKeyEdit]);
end;
{------------------------------------------------------------------------------}
end.
