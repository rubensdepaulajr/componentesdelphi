unit McValida;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, Db, DBTables;

type
  TMcValidation = class;

	TUserValidation = Procedure(UserValidation:TMcValidation; DataSet:TDataSet;var AllowSave:Boolean) of Object;

  TValidationType = (etAbort,etError,etSystem,etWarning);

  TValidationErrors = (veErrorsOnly,veWarningsOnly,veErrorsAndWarnings);

  {Objeto TNullValidation}
  TNullValidation = class(TPersistent)
  Private
		FValidNullFields:TStrings;
  	FValidNullTypes:TStrings;
    FValidNullMessages:TStrings;
    procedure SetValidNullType(Value:TStrings);
    procedure SetValidNullMessage(Value:TStrings);
    procedure SetValidNullField(Value:TStrings);
		//Leitura o arquivo DFM
  	Procedure ReadValidNullFields(Reader:TReader);
  	Procedure WriteValidNullFields(Writer:TWriter);
  	Procedure ReadValidNullTypes(Reader:TReader);
  	Procedure WriteValidNullTypes(Writer:TWriter);
  	Procedure ReadValidNullMessages(Reader:TReader);
  	Procedure WriteValidNullMessages(Writer:TWriter);
	Protected
  	Procedure DefineProperties(Filer:TFiler); override;
  Public
  	Constructor Create;
    Destructor Destroy; override;
    Property ValidNullField:TStrings    read FValidNullFields   write SetValidNullField;
    Property ValidNullType:TStrings     read FValidNullTypes    write SetValidNullType;
    Property ValidNullMessage:TStrings  read FValidNullMessages write SetValidNullMessage;
	End;

  {Objeto TKeyValidation}
  TKeyValidation = class(TPersistent)
  Private
		FValidKeyFields:TStrings;
  	FValidKeyTypes:TStrings;
    FValidKeyMessages:TStrings;
    procedure SetValidKeyType(Value:TStrings);
    procedure SetValidKeyMessage(Value:TStrings);
    procedure SetValidKeyField(Value:TStrings);
		//Leitura o arquivo DFM
  	Procedure ReadValidKeyFields(Reader:TReader);
  	Procedure WriteValidKeyFields(Writer:TWriter);
  	Procedure ReadValidKeyTypes(Reader:TReader);
  	Procedure WriteValidKeyTypes(Writer:TWriter);
  	Procedure ReadValidKeyMessages(Reader:TReader);
  	Procedure WriteValidKeyMessages(Writer:TWriter);
	Protected
  	Procedure DefineProperties(Filer:TFiler); override;
  Public
  	Constructor Create;
    Destructor Destroy; override;
    Property ValidKeyField:TStrings    read FValidKeyFields   write SetValidKeyField;
    Property ValidKeyType:TStrings     read FValidKeyTypes    write SetValidKeyType;
    Property ValidKeyMessage:TStrings  read FValidKeyMessages write SetValidKeyMessage;
	End;

  {Objeto TMcValidation}
  TMcValidation = class(TComponent)
  private
  	FOwner:TComponent;
    FFont:TFont;
    FCaption:string;
    FDataSet:TDBDataSet;
    FNullValidation:TNullValidation;
    FKeyValidation:TKeyValidation;
    FOnUserValidation:TUserValidation;
    Function  GetValidationType(TypeValid:String):TValidationType;
    Function  GetTypeOfErrors:TValidationErrors;
    Procedure SetNullValidation(Value:TNullValidation);
    Procedure SetKeyValidation(Value:TKeyValidation);
    procedure SetFont(Value:TFont);
		Procedure SetDataSet(Value:TDBDataSet);
    Procedure ValidNullValues;
    procedure ValidKeyFields;
    procedure ValidKeyFieldsTable;
    procedure ValidKeyFieldsQuerye;
  protected
    { Protected declarations }
    procedure Notification(AComponent:TComponent;Operation:TOperation); override;
  Public
    Constructor Create(AOwner:TComponent); override;
    Destructor Destroy; override;
		{--- Procedimentos P�blicos -----------------------------------------------}
    Function  DisplayHandleValidation:Boolean; virtual;
    Function  Validate:Boolean; Virtual;
    procedure AddValidation(Msg:String;ValidationType:TValidationType); Virtual;
    procedure Clear; virtual;
  Published
    Property DataSet:TDBDataSet           read FDataSet      write SetDataSet;
    Property NullValidation:TNullValidation read FNullValidation write SetNullValidation;
    Property KeyValidation:TKeyValidation   read FKeyValidation  write SetKeyValidation;
    property Caption:string read FCaption write FCaption;
    property Font:TFont read FFont write SetFont;
    Property OnUserValidation:TUserValidation read FOnUserValidation write FOnUserValidation;
  end;

procedure Register;

implementation

uses McValDlg;

{--- TKeyValidation ----------------------------------------------------------}
constructor TKeyValidation.Create;
begin
	FValidKeyFields   := TStringList.Create;
  FValidKeyTypes    := TStringList.Create;
  FValidKeyMessages := TStringList.Create;
end;
{------------------------------------------------------------------------------}
destructor TKeyValidation.Destroy;
begin
	FValidKeyFields.Clear;
	FValidKeyTypes.Clear;
	FValidKeyMessages.Clear;

	FValidKeyFields   := nil;
	FValidKeyTypes    := nil;
	FValidKeyMessages := nil;

	FValidKeyFields.Free;
  FValidKeyTypes.Free;
  FValidKeyMessages.Free;

	inherited Destroy;
end;
{------------------------------------------------------------------------------}
Procedure TKeyValidation.DefineProperties(Filer:TFiler);
Begin;
	Inherited DefineProperties(Filer);
	Filer.DefineProperty('ValidKeyTypes',ReadValidKeyTypes,WriteValidKeyTypes,FValidKeyTypes.Count > 0);
	Filer.DefineProperty('ValidKeyFields',ReadValidKeyFields,WriteValidKeyFields,FValidKeyFields.Count > 0);
	Filer.DefineProperty('ValidKeyMessages',ReadValidKeyMessages,WriteValidKeyMessages,FValidKeyMessages.Count > 0);
End;
{------------------------------------------------------------------------------}
Procedure TKeyValidation.ReadValidKeyFields(Reader:TReader);
Begin
	Reader.ReadListBegin;
  FValidKeyFields.Clear;
  While not Reader.EndOfList do
	  FValidKeyFields.Add(Reader.ReadString);
  Reader.ReadListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TKeyValidation.WriteValidKeyFields(Writer:TWriter);
Var I:Integer;
Begin
	Writer.WriteListBegin;
  For I := 0 to FValidKeyFields.Count - 1 do
  	Writer.WriteString(FValidKeyFields.Strings[I]);
	Writer.WriteListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TKeyValidation.ReadValidKeyTypes(Reader:TReader);
Begin
	Reader.ReadListBegin;
  FValidKeyTypes.Clear;
  While not Reader.EndOfList do
	  FValidKeyTypes.Add(Reader.ReadString);
  Reader.ReadListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TKeyValidation.WriteValidKeyTypes(Writer:TWriter);
Var I:Integer;
Begin
	Writer.WriteListBegin;
  For I := 0 to FValidKeyTypes.Count - 1 do
  	Writer.WriteString(FValidKeyTypes.Strings[I]);
	Writer.WriteListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TKeyValidation.ReadValidKeyMessages(Reader:TReader);
Begin
	Reader.ReadListBegin;
  FValidKeyMessages.Clear;
  While not Reader.EndOfList do
	  FValidKeyMessages.Add(Reader.ReadString);
  Reader.ReadListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TKeyValidation.WriteValidKeyMessages(Writer:TWriter);
Var I:Integer;
Begin
	Writer.WriteListBegin;
  For I := 0 to FValidKeyMessages.Count - 1 do
  	Writer.WriteString(FValidKeyMessages.Strings[I]);
	Writer.WriteListEnd;
End;
{------------------------------------------------------------------------------}
procedure TKeyValidation.SetValidKeyField(Value:TStrings);
begin
	IF Value.Text <> FValidKeyFields.Text Then
  	FValidKeyFields.Assign(Value);
end;
{------------------------------------------------------------------------------}
procedure TKeyValidation.SetValidKeyType(Value:TStrings);
begin
	IF Value.Text <> FValidKeyTypes.Text Then
  	FValidKeyTypes.Assign(Value);
end;
{------------------------------------------------------------------------------}
procedure TKeyValidation.SetValidKeyMessage(Value:TStrings);
begin
	IF Value.Text <> FValidKeyMessages.Text Then
  	FValidKeyMessages.Assign(Value);
end;
{------------------------------------------------------------------------------}


{--- TNullValidation ----------------------------------------------------------}
constructor TNullValidation.Create;
begin
	FValidNullFields   := TStringList.Create;
  FValidNullTypes    := TStringList.Create;
  FValidNullMessages := TStringList.Create;
end;
{------------------------------------------------------------------------------}
destructor TNullValidation.Destroy;
begin
	FValidNullFields.Clear;
	FValidNullTypes.Clear;
	FValidNullMessages.Clear;

	FValidNullFields   := nil;
	FValidNullTypes    := nil;
	FValidNullMessages := nil;

	FValidNullFields.Free;
  FValidNullTypes.Free;
  FValidNullMessages.Free;

	inherited Destroy;
end;
{------------------------------------------------------------------------------}
Procedure TNullValidation.DefineProperties(Filer:TFiler);
Begin;
	Inherited DefineProperties(Filer);
	Filer.DefineProperty('ValidNullTypes',ReadValidNullTypes,WriteValidNullTypes,FValidNullTypes.Count > 0);
	Filer.DefineProperty('ValidNullFields',ReadValidNullFields,WriteValidNullFields,FValidNullFields.Count > 0);
	Filer.DefineProperty('ValidNullMessages',ReadValidNullMessages,WriteValidNullMessages,FValidNullMessages.Count > 0);
End;
{------------------------------------------------------------------------------}
Procedure TNullValidation.ReadValidNullFields(Reader:TReader);
Begin
	Reader.ReadListBegin;
  FValidNullFields.Clear;
  While not Reader.EndOfList do
	  FValidNullFields.Add(Reader.ReadString);
  Reader.ReadListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TNullValidation.WriteValidNullFields(Writer:TWriter);
Var I:Integer;
Begin
	Writer.WriteListBegin;
  For I := 0 to FValidNullFields.Count - 1 do
  	Writer.WriteString(FValidNullFields.Strings[I]);
	Writer.WriteListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TNullValidation.ReadValidNullTypes(Reader:TReader);
Begin
	Reader.ReadListBegin;
  FValidNullTypes.Clear;
  While not Reader.EndOfList do
	  FValidNullTypes.Add(Reader.ReadString);
  Reader.ReadListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TNullValidation.WriteValidNullTypes(Writer:TWriter);
Var I:Integer;
Begin
	Writer.WriteListBegin;
  For I := 0 to FValidNullTypes.Count - 1 do
  	Writer.WriteString(FValidNullTypes.Strings[I]);
	Writer.WriteListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TNullValidation.ReadValidNullMessages(Reader:TReader);
Begin
	Reader.ReadListBegin;
  FValidNullMessages.Clear;
  While not Reader.EndOfList do
	  FValidNullMessages.Add(Reader.ReadString);
  Reader.ReadListEnd;
End;
{------------------------------------------------------------------------------}
Procedure TNullValidation.WriteValidNullMessages(Writer:TWriter);
Var I:Integer;
Begin
	Writer.WriteListBegin;
  For I := 0 to FValidNullMessages.Count - 1 do
  	Writer.WriteString(FValidNullMessages.Strings[I]);
	Writer.WriteListEnd;
End;
{------------------------------------------------------------------------------}
procedure TNullValidation.SetValidNullField(Value:TStrings);
begin
	IF Value.Text <> FValidNullFields.Text Then
  	FValidNullFields.Assign(Value);
end;
{------------------------------------------------------------------------------}
procedure TNullValidation.SetValidNullType(Value:TStrings);
begin
	IF Value.Text <> FValidNullTypes.Text Then
  	FValidNullTypes.Assign(Value);
end;
{------------------------------------------------------------------------------}
procedure TNullValidation.SetValidNullMessage(Value:TStrings);
begin
	IF Value.Text <> FValidNullMessages.Text Then
  	FValidNullMessages.Assign(Value);
end;
{------------------------------------------------------------------------------}



{--- TMcValidation ------------------------------------------------------------}
constructor TMcValidation.Create(AOwner:TComponent);
begin
  inherited Create(AOwner);
  FOwner := AOwner;
  FFont := TFont.Create;
  FNullValidation := TNullValidation.Create;
  FKeyValidation  := TKeyValidation.Create;
  IF Owner is TForm then
  	Begin
	    FFont.Assign(TForm(Owner).Font);
    End
	Else
  	Begin
    	FFont := TFont.Create;
	    FFont.Name  := 'MS Sans Serif';
  	  FFont.Size  := 8;
    	FFont.Style := [];
	  End;
  FormValidDlg := TFormValidDlg.Create(Self);
end;
{------------------------------------------------------------------------------}
destructor TMcValidation.Destroy;
begin
  FFont.Free;

	FNullValidation.Destroy;
	FNullValidation := nil;

	FKeyValidation.Destroy;
	FKeyValidation := nil;

  {FormValidDlg.Release;
  FormValidDlg := nil;}

  inherited Destroy;
end;
{------------------------------------------------------------------------------}
procedure TMcValidation.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  IF (Operation = opRemove) Then
  	begin
    	IF (AComponent = DataSet) Then
      	DataSet := nil;
	  end;
end;
{------------------------------------------------------------------------------}
function TMcValidation.GetValidationType(TypeValid:String):TValidationType;
begin
	Result := etAbort;
	IF TypeValid = 'etAbort - Abort' Then
  	Result := etAbort;
	IF TypeValid = 'etError - Abort' Then
  	Result := etError;
	IF TypeValid = 'etSystem - Abort' Then
  	Result := etSystem;
	IF TypeValid = 'etWarning - Ignore' Then
  	Result := etWarning;
end;
{------------------------------------------------------------------------------}
procedure TMcValidation.SetNullValidation(Value: TNullValidation);
begin
	IF Value <> FNullValidation Then
		FNullValidation := Value;
end;
{------------------------------------------------------------------------------}
procedure TMcValidation.SetKeyValidation(Value: TKeyValidation);
begin
	IF Value <> FKeyValidation Then
		FKeyValidation := Value;
end;
{------------------------------------------------------------------------------}
procedure TMcValidation.SetFont(Value:TFont);
begin
  FFont.Assign(Value);
end;
{------------------------------------------------------------------------------}
procedure TMcValidation.SetDataSet(Value: TDBDataSet);
begin
	IF Value <> FDataSet Then
  	Begin
    	{IF Value = nil Then
				FDataSet.FreeNotification(self);}
			FDataSet := Value;
    End;
end;
{------------------------------------------------------------------------------}
function TMcValidation.Validate:Boolean;
begin
	Result := True;

  // Form da valida��o do TValidation
  FormValidDlg := TFormValidDlg.Create(Self);

  Clear;
  ValidNullValues;
	ValidKeyFields;
  IF Assigned(FOnUserValidation) Then
   	FOnUserValidation(Self,Self.DataSet,Result);
  IF Result Then
	  Result := DisplayHandleValidation;
end;
{------------------------------------------------------------------------------}
Procedure TMcValidation.ValidNullValues;
Var Cont:Integer;
		sValidType:String;
begin
	IF FNullValidation.ValidNullField.Count <> 0 Then
  	Begin
    	For Cont := 0 to FNullValidation.ValidNullField.Count - 1 do
      	Begin
					IF (FDataSet.FieldByName(FNullValidation.ValidNullField.Strings[Cont]).IsNull) or
          (Trim(FDataSet.FieldByName(FNullValidation.ValidNullField.Strings[Cont]).Text) = '') Then
          	Begin
              sValidType := FNullValidation.ValidNullType.Strings[Cont];
	          	AddValidation(FNullValidation.ValidNullMessage.Strings[Cont],
              GetValidationType(sValidType));
						End;
        End;
    End;
end;
{------------------------------------------------------------------------------}
Procedure TMcValidation.ValidKeyFields;
begin
	IF FDataSet is TTable Then
		ValidKeyFieldsTable;

	IF FDataSet is TQuery Then
  	ValidKeyFieldsQuerye;
end;
{------------------------------------------------------------------------------}
procedure TMcValidation.ValidKeyFieldsQuerye;
Var MemQuery:TQuery;
		Modificando:Boolean;
		I,J,Cont:Integer;
		sValidType,sListField:String;
		ListFields:Array of String; //Armazena o nome dos campos da pesquisa

		// Fun��o interna que recupera o n�mero de campos a seren validados
    Function GetNumKeyFields(sVarListField:String):Integer;
    Var Cont:Integer;
        Campo:String;
    begin
      Result := 1;
      Campo := '';
      For Cont := 1  to Length(Trim(sVarListField)) do
        Begin
          IF sVarListField[Cont] = ';' Then
            Begin
              Inc(Result);
            End;
        End;
    End;

begin
	Modificando := False;
	MemQuery := TQuery.Create(self);
  MemQuery.DatabaseName := TTable(FDataSet).DataBaseName;
  MemQuery.SessionName  := TQuery(FDataSet).SessionName;
  MemQuery.Sql.Assign(TQuery(FDataSet).SQL);
  MemQuery.Params.AssignValues(TQuery(FDataSet).Params);
  MemQuery.Prepare;
	// Abre a tabela em mem�ria
	Try
  	MemQuery.Open;
   	For Cont := 0 to FKeyValidation.ValidKeyField.Count - 1 do
     	Begin
				// Recupera os campos de pesquisa
        sListField := FKeyValidation.FValidKeyFields.Strings[Cont];

        // ***** Rotina que recupera os valores dos campos para pesquisa *****
        // Altera o tamanho da matris de nomes de campos
        SetLength(ListFields,GetNumKeyFields(sListField));

        // Executa a limpeza da mem�ria alocada para os campos
        For I := Low(ListFields) to High(ListFields) do
          ListFields[I] := '';

        // Recupera cada nome de campo utilizado na chave de pesquisa
        J := 0;
        For I := 1  to Length(sListField) do
          Begin
            IF sListField[I] <> ';' Then
              Begin
                ListFields[J] := ListFields[J] + Trim(sListField[I]);
              End
            Else
              Begin
                Inc(J);
              End;
          End;

				// Posiciona o ponteiro da tabela nos valores informados em EditKey
        IF MemQuery.Locate(sListField,FDataSet.FieldValues[sListField],[loCaseInsensitive]) Then
        	Begin
          	IF FDataSet.State = dsEdit Then
            	Begin
                For I := Low(ListFields) to High(ListFields) do
                  IF FDataSet.FieldByName(ListFields[I]).OldValue = MemQuery.FieldByName(ListFields[I]).Value Then
                    Begin
                      IF not Modificando Then
                        Modificando := True;
                    End;
							End;
            IF not Modificando Then
            	Begin
                sValidType := FKeyValidation.ValidKeyType.Strings[Cont];
                AddValidation(FKeyValidation.ValidKeyMessage.Strings[Cont],
                GetValidationType(sValidType));
            	End;
          End;
      End;
  Finally
  	MemQuery.Close;
  	MemQuery.UnPrepare;
    MemQuery.Destroy;
  End;
end;
{------------------------------------------------------------------------------}
procedure TMcValidation.ValidKeyFieldsTable;
Var MemTable:TTable;
		I,J,Cont:Integer;
		sValidType,sListField:String;
		ListFields:Array of String;   // Armazena o nome dos campos da pesquisa
    VarValFields:Array of Variant; // Armazena os valores de pesquisa

		// Fun��o interna que recupera o n�mero de campos a seren validados
    Function GetNumKeyFields(sVarListField:String):Integer;
    Var Cont:Integer;
        Campo:String;
    begin
      Result := 1;
      Campo := '';
      For Cont := 1  to Length(Trim(sVarListField)) do
        Begin
          IF sVarListField[Cont] = ';' Then
            Begin
              Inc(Result);
            End;
        End;
    End;

begin
	//MemTable := TTable(FDataSet).Create(self);
	MemTable := TTable.Create(self);
  MemTable.DatabaseName := TTable(FDataSet).DataBaseName;
  MemTable.TableName    := TTable(FDataSet).TableName;
  MemTable.SessionName  := TTable(FDataSet).SessionName;
	// Abre a tabela em mem�ria
	Try
  	MemTable.Open;
   	For Cont := 0 to FKeyValidation.ValidKeyField.Count - 1 do
     	Begin
				// Recupera os campos de pesquisa
        sListField := FKeyValidation.FValidKeyFields.Strings[Cont];

        // ***** Rotina que recupera os valores dos campos para pesquisa *****
        // Altera o tamanho da matris de nomes de campos
        SetLength(ListFields,GetNumKeyFields(sListField));
        // Altera o tamanho da matris de valores
        SetLength(VarValFields,GetNumKeyFields(sListField));

        // Executa a limpeza da mem�ria alocada para os campos
        For I := Low(ListFields) to High(ListFields) do
          ListFields[I] := '';

        // Recupera cada nome de campo utilizado na chave de pesquisa
        J := 0;
        For I := 1  to Length(sListField) do
          Begin
            IF sListField[I] <> ';' Then
              Begin
                ListFields[J] := ListFields[J] + Trim(sListField[I]);
              End
            Else
              Begin
                Inc(J);
              End;
          End;
        // Carrega os valores dos campos de dados;
        For I := Low(ListFields) to High(ListFields) do
          VarValFields[I] := TTable(FDataSet).FieldByName(ListFields[I]).Value;

				// Muda o �ndice da tabela para a sequ�ncia de campos para pesquisa
        MemTable.IndexFieldNames := sListField;

        // Armazena os valores de pesquisa
        MemTable.EditKey;
        For I := Low(VarValFields) to High(VarValFields) do
					MemTable.FieldByName(ListFields[I]).Value := VarValFields[I];

				// Posiciona o ponteiro da tabela nos valores informados em EditKey
        IF MemTable.GotoKey Then
        	Begin
          	// Verifica se o registro encontrado � o mesmo que est� sendo editado
          	IF (MemTable.RecNo <> FDataset.RecNo) Then
							Begin
                sValidType := FKeyValidation.ValidKeyType.Strings[Cont];
                AddValidation(FKeyValidation.ValidKeyMessage.Strings[Cont],
                GetValidationType(sValidType));
              End;
          End;
      End;
  Finally
  	MemTable.Close;
    MemTable.Destroy;
  End;
end;
{------------------------------------------------------------------------------}
procedure TMcValidation.Clear;
begin
	IF FormValidDlg <> nil Then
	  FormValidDlg.ClearMessages;
end;
{------------------------------------------------------------------------------}
procedure TMcValidation.AddValidation(Msg:String;ValidationType:TValidationType);
begin
  FormValidDlg.AddError(Msg,ValidationType);
end;
{------------------------------------------------------------------------------}
function TMcValidation.GetTypeOfErrors:TValidationErrors;
Var I,ErrorCount,WarningCount:Integer;
begin
  ErrorCount := 0;
  WarningCount := 0;

  For I := 0 to Pred(FormValidDlg.lstErrors.Items.Count) do
	  begin
  	  case TValidationType(FormValidDlg.lstErrors.Items.Objects[I]) of
    	  etWarning:Inc(WarningCount);
      	etAbort:Inc(ErrorCount);
				etError:Inc(ErrorCount);
        etSystem:Inc(ErrorCount);
	    end;
  	end;

  IF (ErrorCount > 0) and (WarningCount > 0) then
  	Begin
	    Result := veErrorsAndWarnings;
    End
  Else
    Begin
			IF ErrorCount > 0 then
		    Result := veErrorsOnly
  		Else
		    Result := veWarningsOnly;
    End;
end;
{------------------------------------------------------------------------------}
function TMcValidation.DisplayHandleValidation:Boolean;
begin
	//IF FormValidDlg <> nil Then
	//  FormValidDlg := TFormValidDlg.Create(Self);

  IF FormValidDlg.NumberOfErrors = 0 Then
	  begin
    	Result := True;
	    Exit;
  	end
  Else
  	Begin
      IF FCaption <> '' Then
        FormValidDlg.Caption := FCaption;

      FormValidDlg.SetButtons(GetTypeOfErrors = veWarningsOnly);

      FormValidDlg.Font := FFont;
      Result := (FormValidDlg.ShowModal = mrIgnore);
    End;
end;
{------------------------------------------------------------------------------}

{--- Register -----------------------------------------------------------------}
procedure Register;
begin
  RegisterComponents('McBizComps',[TMcValidation]);
end;
{------------------------------------------------------------------------------}
end.
