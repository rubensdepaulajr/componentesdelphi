//===============================================================
//
//       sak_util unit, part of SakEmail
//
//       Contains: a lot of helper functions
//
//
//---------------------------------------------------------------
//
//      Copyrigth (c) 1997, 1998, 1999 Sergio A. Kessler
//      and authors cited
//      http://sak.org.ar
//
//===============================================================


unit sak_util;

interface

uses classes;

type
  TOnError = procedure(Sender: TObject; Error: integer; Msg: string) of object;
  TOnCodeStartEvent = procedure( Sender: TObject; FileName: string; BytesCount: longint) of object;
  TOnCodeProgressEvent = procedure( Sender: TObject; Percent: word) of object;

// this function by hou yg <yghou@yahoo.com>
function sak_IsIPAddress(ss : string):boolean;

// this function by Dmitry Bondarenko & Sergio Kessler
function sak_StrWord2Int( s: string;       //input text
                          wordNum: word;   //number of the word to convert
                          defVal: integer  //default value
                         ): integer;

function sak_GetBoundaryOutOfLine( s: string): string;

// this function by Matjaz Bravc *)
function sak_GetInternetDate( Date: TDateTime):string;

function sak_MakeUniqueID( HostName: string): string;

function sak_GenerateBoundary: string;

function sak_GetTempFileName: string;

// this function by Chris G�nther, bugfixed by Sergio ;-)
function sak_QuotedPrintableDecode( FCurrentData : PChar ) : string;

// this function by Chris G�nther
//function sak_Base64Decode( sOrg : PChar ) : string;

function sak_ConvertCharSet( Line : string ) : string;

function sak_GetMIMEType(const FileName : string) : string;

// this function by Chris G�nther
function sak_ExtractAddress( sAddress : string; WithDelimit : Boolean ) : string;

// this function by Chris G�nther
function sak_ExtractAlias( sAddress : string; WithDelimit : Boolean ) : string;

function sak_DelFromStr( sSource: string; sChar: Char ): string;

function sak_CleanUpAddresses( AddressList: string): TStringList;

function sak_UnQuote( s: string): string;

function sak_FindFieldInHeaders( const field: string; const Headers: TStringList;
                                 var line: string): boolean;

function sak_GetFieldValueFromLine( Field, Line: string): string;

procedure sak_DeleteHeaders( aText: TStringList);

function sak_FormatAddress( sAddress: string): string;

const
  sak_esNotConnected = 'Not connected.';
  sak_esInvalidOp = 'Invalid op.';
  sak_esNotOrigin = '"From" field not specified.';
  sak_esNotDestination = '"SendTo" field not specified.';
  // es = exception string
  // this strings are used only in design stage



implementation

uses sysutils, Windows, Registry, SakMIME;

// this function by hou yg <yghou@yahoo.com>
function sak_IsIPAddress(ss : string):boolean;
var
  s,s1 : string;
  ii,p : integer;
begin
  s1 := ss;
  result := false;
  for ii := 1 to 3 do
  begin
    p := pos( '.', s1);
    if p = 0 then exit;
    s := Copy( s1, 1, p-1);
    s1 := copy( s1, p+1, Length( s1));
    p := StrToIntDef( s, -1);
    if (p < 0) or (p > 255) then exit;
  end;
  p := StrToIntDef( s1, -1);
  if (p < 0) or (p > 255) then exit;
  result := true;
end;


// this function by Dmitry Bondarenko & Sergio Kessler
function sak_StrWord2Int( s: string;       //input text
                          wordNum: word;   //number of the word to convert
                          defVal: integer  //default value
                         ): integer;
var
  space_index, i: integer;
begin
  result := defVal;
  s := trim( s);

  for i := 1 to WordNum-1 do
  begin
    space_index := pos( ' ', s);
    if space_index = 0 then
    begin
      exit;
    end else
    begin
      s := trim( copy( s, space_index, length(s)));
    end;
  end;

  space_index := pos( ' ', s);
  if space_index > 0 then
  begin
    // here we delete the trailing words
    s := trim( copy( s, 1, space_index));
  end;

  result := StrToIntDef( s, defVal);
end;

function sak_GetBoundaryOutOfLine( s: string): string;
var
  x, position: integer;
begin
  position := pos( 'BOUNDARY', UpperCase( s));
  result := trim( copy( s, position + length('BOUNDARY'), 72));
  delete( result, 1, 1); // remove the =
  result := trim( result);  // remove any leading spaces
  if result[1] = '"' then
  begin
    delete( result, 1, 1);
    x := pos( '"', result);      //sometimes there are a ; in the end
    if x > 0 then delete( result, x, 72);
  end;
  x := pos( ';', result);
  if x > 0 then
  begin
    delete( result, x, 72);
  end;
  result := '--' + trim( result);
end;


// this function by Matjaz Bravc
function sak_GetInternetDate( Date: TDateTime):string;
(* The date in RFC 822 conform string format *)

 function int2str(value:integer; len:byte):string;
  begin
    result := IntToStr( value);
    while length( result) < len do result := '0' + result;
  end;

 function GetTimeZoneBias:longint;
 (* The offset to UTC/GMT in minutes of the local time zone *)
 var tz_info: TTimeZoneInformation;
 begin
   case GetTimeZoneInformation(tz_info) of
        1: result := tz_info.StandardBias+tz_info.Bias;
        2: result := tz_info.DaylightBias+tz_info.Bias;
    else
        result := 0;
   end;
 end;

 function GetTimeZone:string;
 var bias: longint;
 begin
   bias := GetTimeZoneBias;
   if bias = 0 then
   begin
     result := 'GMT';
   end else
   begin
     if bias < 0 then
     begin
       result := '+' + int2str(abs(bias) div 60,2)+int2str(abs(bias) mod 60,2);
     end else
     begin
       if bias > 0 then
       begin
         result := '-' + int2str(bias div 60,2)+int2str(bias mod 60,2);
       end;
     end;
   end;
 end;

var
  d, m, y, w, h, mm, s, ms: word;
const
  WeekDays: array [1..7] of string[3] = ('Sun','Mon','Tue','Wed',
                                         'Thu','Fri','Sat');
  Months: array [1..12] of string[3] = ('Jan','Feb','Mar','Apr','May','Jun',
                                        'Jul','Aug','Sep','Oct','Nov','Dec');
begin
  DecodeDate( date, y, m, d);
  DecodeTime( date, h, mm, s, ms);
  w := DayOfWeek( date);
  Result := weekdays[w] + ', ' +
            inttostr(d) + ' ' +
            months[m] + ' ' +
            inttostr(y) + ' ' +
            int2str(h,2) + ':' +
            int2str(mm,2) + ':' +
            int2str(s,2) + ' ' +
            GetTimeZone;
end;

function sak_MakeUniqueID( HostName: string) : string;
var
  s: string;
  i : Integer;
begin
  Randomize;
  s := '';
  for i:=1 to 8 do
  begin
    s := Concat( s, chr( 97 + Random(20)));
  end;
  result := '<' + 'SAK.' +  formatDateTime( 'yyyy"."mm"."dd"."', date) +
            s + '@' + HostName + '>';
end;


function sak_GenerateBoundary : string;
var
  s: string;
  i : integer;
begin
  randomize;
  s := '';
  for i := 1 to 12 do
  begin
    s := s + chr( 97 + random( 25));
  end;
  result := '--_SAK_bound_' + s;
end;

function sak_GetTempFileName: string;
var
  tmpFileName: string;
begin
  SetLength( tmpFileName, Max_Path);
  GetTempPath( Max_Path-1, PChar( tmpFileName));
  GetTempFileName( PChar( tmpFileName), 'sak', 0, PChar( tmpFileName));
  SetLength( tmpFileName, StrLen( PChar( tmpFileName)));
  if FileExists( tmpFileName) then
  begin
    DeleteFile( PChar( tmpFileName));
  end;
  result := tmpFileName;
end;


// this function by Chris G�nther, bugfixed by Sergio ;-)
function sak_QuotedPrintableDecode( FCurrentData : PChar ): string;
{ This works if charset="iso-8859-1" ! }
var
  SourceIndex                : Integer;
  DecodedIndex               : Integer;
  Ch                         : Char;
  CodeHex                    : String;
begin
  SourceIndex := 0;
  DecodedIndex := 0;
  if ( FCurrentData <> '' ) and ( FCurrentData^ <> #0 ) then
  begin
    while TRUE do
    begin
      Ch := FCurrentData[ SourceIndex ];
      if Ch = #0 then
         break;

      if Ch = '_' then
      begin
        FCurrentData[ DecodedIndex ] := ' ';
        Inc( SourceIndex );
        Inc( DecodedIndex );
      end else
      begin
        if Ch <> '=' then
        begin
          FCurrentData[ DecodedIndex ] := Ch;
          Inc( SourceIndex );
          Inc( DecodedIndex );
        end else
        begin
          Inc( SourceIndex );
          Ch := FCurrentData[ SourceIndex ];
          if (Ch = #13) or (Ch = #10) then
          begin
            Inc( SourceIndex );
            Inc( SourceIndex );
          end else
          begin
            CodeHex := '$' + Ch;
            Inc( SourceIndex );
            Ch := FCurrentData[ SourceIndex ];
            if Ch = #0 then break;
            CodeHex := CodeHex + Ch ;
            FCurrentData[ DecodedIndex ] := Chr( StrToIntDef( CodeHex, 64));
            Inc( SourceIndex );
            Inc( DecodedIndex );
          end;
        end;
      end;
    end;
    FCurrentData[ DecodedIndex ] := #0;
  end;
  Result := FCurrentData;
end;


// this function by Chris G�nther and Sergio Kessler
function sak_ConvertCharSet( Line : string ) : string;
var
  strToDecode, RestBefore, RestAfter, strActual, decoded: string;
  iLast, iFirst, eFirst, i: integer;
  Encoding, c: char;
  b64Decode: TBase64DecodingStream;
  Dest: TMemoryStream;
begin
  strActual := Line;

  iFirst := Pos( '=?', strActual );
  while (iFirst > 0) do
  begin
    RestBefore := copy( strActual, 1, iFirst-1 );
    strToDecode := copy( strActual, iFirst+2, length( strActual) );
    eFirst := pos( '?', strToDecode);
    if eFirst > 0 then
    begin
      Encoding := UpperCase( strToDecode[eFirst+1])[1];
      delete( strToDecode, 1, eFirst + 2);  // remove until ?Q? or ?B? inclusive
      iLast := Pos( '?=', strToDecode );
      if iLast > 0 then
      begin
        RestAfter := copy( strToDecode, iLast+2, length( strToDecode) );
        delete( strToDecode, iLast, length( strToDecode));  // remove the ?= and the rest

        strActual := RestBefore + RestAfter;
        if Encoding = 'Q' then
        begin
           strActual := RestBefore +
                        sak_QuotedPrintableDecode( PChar( strToDecode) ) +
                        RestAfter;
        end else
        if Encoding = 'B' then
        begin
          Dest := TMemoryStream.Create;

          b64Decode := TBase64DecodingStream.Create( Dest);
          b64Decode.Write( pointer(strToDecode)^, length( strToDecode));
          b64Decode.Free;

          decoded := '';
          Dest.Position := 0;
          for i:= 1 to Dest.Size do
          begin
            Dest.Read( c, 1);
            decoded := decoded + c;
          end;
          Dest.Free;
          strActual := RestBefore + decoded + RestAfter;
        end;

        iFirst := Pos( '=?', strActual );
      end
      else iFirst := 0;
    end
    else iFirst := 0;
  end;
  Result := strActual;
end;


function sak_GetMIMEType(const FileName : string) : string;
var
  Key : string;
begin
  Result:='';
  with TRegistry.Create do
  try
    RootKey:=HKEY_CLASSES_ROOT;
    Key:=ExtractFileExt(FileName);
    if KeyExists(Key) then
    begin
      OpenKey(Key,false);
      Result:=ReadString('Content Type');
      CloseKey;
    end;
  finally
    if Result='' then
      Result:='application/octet-stream';
    free;
  end;
end;


// this function by Chris G�nther
function sak_ExtractAddress( sAddress: string; WithDelimit: Boolean ): string;
var
  iStart, iEnd, par1, par2 : integer;
begin
  result := sAddress;
  repeat
    par1 := pos( '(', result);
    par2 := pos( ')', result);
    if (par1 > 0) and (par2 > 0) then
    begin
      delete( result, par1, par2-par1+1);
    end;
  until (par1 = 0) or (par2 = 0);

  result := trim( result);
  iStart := Pos( '<', result );
  iEnd := Pos( '>', result );
  if ( iStart > 0 ) and ( iEnd > 0 ) then
  begin
    Result := copy( result, iStart + 1, iEnd - iStart - 1 );
  end;

  if WithDelimit then result := '<' + result + '>';
end;


function sak_ExtractAlias( sAddress: string; WithDelimit: Boolean ): string;
var
  iStart, iEnd: integer;
begin
  result := sAddress;
  iStart := Pos( '<', result );
  iEnd := Pos( '>', result );
  if (iStart > 0) and (iEnd > 0) then
  begin
    delete( result, iStart, iEnd-iStart+1);
  end;

  iStart := Pos( '(', result );
  iEnd := Pos( ')', result );
  if (iStart > 0) and (iEnd > 0) then
  begin
    result := copy( result, iStart+1, iEnd-iStart-1);
  end;

  Result := sak_DelFromStr( Result, '"' );
  Result := sak_DelFromStr( Result, '''' );
  Result := trim( Result);

  if WithDelimit then
  begin
    Result := '"' + Result + '"'
  end
end;

function sak_DelFromStr( sSource: string; sChar: Char ): string;
// delete all the ocurrences of sChar in the string sSource
var
  i: Integer;
begin
  Result := sSource;
  i := pos( sChar, Result);
  while i > 0 do
  begin
      delete( Result, i, 1);
      i := pos( sChar, Result);
  end;
end;


function sak_CleanUpAddresses( AddressList: string): TStringList;
var
  i, quote1, quote2: integer;
begin
  result := TStringList.Create;
  AddressList := trim( AddressList);
  if AddressList = '' then exit;

  // here we must remove all that is betwen ""
  repeat
    quote1 := pos( '"', AddressList);
    if (quote1 > 0) then
    begin
      delete( AddressList, quote1, 1);
    end;
    quote2 := pos( '"', AddressList);
    if (quote2 > 0) then
    begin
      delete( AddressList, quote1, quote2-quote1+1);
    end;
  until (quote1 = 0) or (quote2 = 0);

  AddressList := lowercase( AddressList);

  i := pos( ',', AddressList);
  while i > 0 do
  begin
    result.Add( sak_ExtractAddress( copy( AddressList, 1, i-1), False));
    delete( AddressList, 1, i);
    i := pos( ',', AddressList);
  end;
  result.Add( sak_ExtractAddress( AddressList, False));
end;

// convert the addresses in the form: "alias" <address>
function sak_FormatAddress( sAddress: string): string;
var
  i, quote1, quote2: integer;
  addr: string;
begin
  result := '';

  // here we must remove all that is betwen ""
  // sorry, but could be a ',' within quotes
  // and there are no rules to deal with this, I've checked Netscape
  // and M$ and they do a really poor job parsing addresses, so don't
  // expect much from me
  repeat
    quote1 := pos( '"', sAddress);
    if (quote1 > 0) then
    begin
      delete( sAddress, quote1, 1);
    end;
    quote2 := pos( '"', sAddress);
    if (quote2 > 0) then
    begin
      delete( sAddress, quote1, quote2-quote1+1);
    end;
  until (quote1 = 0) or (quote2 = 0);

  i := pos( ',', sAddress);
  while i > 0 do
  begin
    addr := copy( sAddress, 1, i-1);
    result := result + sak_ExtractAlias( sak_ConvertCharSet( addr), True) +
              ' ' + sak_ExtractAddress( addr, True) + ', ';
    delete( sAddress, 1, i);
    i := pos( ',', sAddress);
  end;
  result := result + sak_ExtractAlias( sak_ConvertCharSet( sAddress), True) +
               ' ' + sak_ExtractAddress( sAddress, True);
end;

function sak_UnQuote( s: string): string;
// return the same string unquoted, ie: "hola" -> hola  or 'hola' -> hola
begin
  if (s[ 1] = #34) or (s[ 1] = #39) then
      delete( s, 1, 1);           // here we delete the first "
  if (s[ length( s)] = #34) or (s[ length( s)] = #39) then
      delete( s, length( s), 1);  // here we delete the last "
  result := s;
end;

function sak_GetFieldValueFromLine( Field, Line: string): string;
var
  position, i: integer;
begin
  result := '';
  position := pos( field, uppercase( line));
  if position > 0 then
  begin
    result := trim( copy( line, position + length( field), length(line)));
    i := pos( ';', result);
    if i > 0 then
    begin
      result := trim( copy( result, 1, i-1));
    end;
  end;
end;

function  sak_FindFieldInHeaders( const field: string; const Headers: TStringList;
                                  var line: string): boolean;
var
  s: string;
  lineNumber, i: integer;
begin
  result := false;
  if Headers.Count = 0 then exit;
  lineNumber := 0;
  while (lineNumber < Headers.count) and
        (Headers[ lineNumber] <> '') do
  begin
    s := upperCase( Headers[ lineNumber]);
    if pos( field, s) = 1 then
    begin
      line := Headers[ lineNumber];
      i := lineNumber+1;
      while (i < Headers.Count) and (Headers[ i] <> '') and
            ((Headers[ i][1] = ' ') or (Headers[ i][1] = #9)) do
      begin
        line := line + trim( Headers[ i]);
        inc( i);
      end;
      result := true;
      break;
    end;
    inc( lineNumber);
  end;
end;

procedure sak_DeleteHeaders( aText: TStringList);
begin
  // delete the headers
  while (aText.Count > 0) and (trim(aText[ 0]) <> '') do
  begin
    aText.delete( 0);
  end;
  // delete the blank lines
  while (aText.Count > 0) and (trim(aText[ 0]) = '') do
  begin
    aText.delete( 0);
  end;
end;


end.
