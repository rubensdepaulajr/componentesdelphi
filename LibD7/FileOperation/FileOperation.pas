unit FileOperation;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TdgFileOperation = class(TComponent)
  private
    FSourceFolder: String;
    FTargetFolder: String;
    FShowProgress: Boolean;
    FProgressTitle: String;
    FFilesOnly: Boolean;
    FRenameOnCollision: Boolean;
    { Private declarations }
    function DoFileOp(FileOp: UINT): Boolean;
  protected
    { Protected declarations }
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    function DoCopy: Boolean;
    function DoCreateFolder: Boolean;
    function DoDelete: Boolean;
    function DoMove: Boolean;
    function DoRename: Boolean;
  published
    { Published declarations }
    property FilesOnly: Boolean read FFilesOnly write FFilesOnly default False;
    property ProgressTitle: String read FProgressTitle write FProgressTitle;
    property RenameOnCollision: Boolean read FRenameOnCollision
               write FRenameOnCollision default True;
    property ShowProgress: Boolean read FShowProgress
               write FShowProgress default True;
    property SourceFolder: String read FSourceFolder write FSourceFolder;
    property TargetFolder: String read FTargetFolder write FTargetFolder;
  end;

procedure Register;

implementation

uses ShellApi, FileCtrl;

procedure Register;
begin
  RegisterComponents('Extras', [TdgFileOperation]);
end;

{ TFileOperation }

constructor TdgFileOperation.Create(AOwner: TComponent);
begin
  inherited;
  FShowProgress := True;
  FFilesOnly := False;
  FRenameOnCollision := True;
end;

function TdgFileOperation.DoCopy: Boolean;
begin
  Result := DoFileOp(FO_COPY);
end;

function TdgFileOperation.DoCreateFolder: Boolean;
begin
  Result := ForceDirectories(FSourceFolder);
end;

function TdgFileOperation.DoDelete: Boolean;
begin
  Result := DoFileOp(FO_DELETE);
end;

function TdgFileOperation.DoFileOp(FileOp: UINT): Boolean;
var
  SHFileOpStruct:     TSHFileOpStruct;
  Flags:              FILEOP_FLAGS;
  SourceDir:          PChar;
  TargetDir:          PChar;
  ProgressTitle:      PChar;
begin
  Result := False;
  ProgressTitle := AllocMem(Length(FProgressTitle) + 2);
  try
    SourceDir := AllocMem(Length(FSourceFolder) + 2);
    try
      TargetDir := AllocMem(Length(FTargetFolder) + 2);
      try
        StrCopy(ProgressTitle, PChar(FProgressTitle));
        StrCopy(SourceDir, PChar(FSourceFolder));
        StrCopy(TargetDir, PChar(FTargetFolder));
        Flags := FOF_NOCONFIRMATION;
        if FRenameOnCollision then
          Flags := Flags or FOF_RENAMEONCOLLISION;
        if FFilesOnly then
          Flags := Flags or FOF_FILESONLY;
        if not FShowProgress then
          Flags := Flags or FOF_SILENT
        else
          Flags := FOF_SIMPLEPROGRESS;

        with SHFileOpStruct do
        begin
          Wnd    := Application.Handle;
          wFunc  := FileOp;
          pFrom  := SourceDir;
          pTo    := TargetDir;
          fFlags := Flags;
          fAnyOperationsAborted := False;
          hNameMappings := nil;
          lpszProgressTitle := ProgressTitle;
          if SHFileOperation(SHFileOpStruct) <> 0 then
            RaiseLastWin32Error;
          Result := fAnyOperationsAborted;
        end;
      finally
        FreeMem(TargetDir, Length(FTargetFolder) + 2);
      end;
    finally
      FreeMem(SourceDir, Length(FSourceFolder) + 2);
    end;
  finally
    FreeMem(ProgressTitle, Length(FProgressTitle) + 2);
  end;
end;

function TdgFileOperation.DoMove: Boolean;
begin
  Result := DoFileOp(FO_MOVE);
end;

function TdgFileOperation.DoRename: Boolean;
begin
  Result := DoFileOp(FO_RENAME);
end;

end.
 