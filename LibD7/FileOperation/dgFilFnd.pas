unit dgFilFnd;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, Variants;

type
  EFileFinderError = class(Exception);
  TOnFileFound = procedure( const Path: String;
                            const FileName,
                                  ShortName,
                                  AttributeString: String;
                            const Created,
                                  Accessed,
                                  Updated: Variant;
                            const FileSize: Int64;
                            var ContinueSearch: Boolean )of object;
  TBeforeDirectoryScan =
                 procedure( const Path: String;
                            const FileName,
                                  ShortName: String;
                            var ContinueSearch: Boolean )of object;
  TAfterDirectoryScan =
                 procedure( const Path: String;
                            const FileName,
                                  ShortName: String;
                            const LastFileCreated,
                                  LastFileAccessed,
                                  LastFileUpdated: Variant;
                            const DirSize: Int64;
                            var ContinueSearch: Boolean )of object;
  TdgFileFinder = class(TComponent)
  private
    { Private declarations }
    FCancelScan:            Boolean;
    FIncludeSystemFiles:    Boolean;
    FAfterDirectoryScan:    TAfterDirectoryScan;
    FBeforeDirectoryScan:   TBeforeDirectoryScan;
    FOnFileFound:           TOnFileFound;
    function Win32FileTimeToDateTime(FTime: _FILETIME): TDateTime;
    function GetWindowsErrorMsg(ErrorCode: DWORD): String;
    procedure RaiseWindowsError(ErrorCode: DWORD);
  protected
    { Protected declarations }
    function ScanDirectory( var Path: String;
                            const Mask: String;
                            const ScanSubdirectories: Boolean): Boolean;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure FindFiles( const Path: String;
                         const Mask: String;
                         const ScanSubdirectories: Boolean);
    property CancelScan: Boolean read FCancelScan write FCancelScan;
  published
    { Published declarations }
    property IncludeSystemFiles: Boolean read FIncludeSystemFiles
               write FIncludeSystemFiles default False;
    property AfterDirectoryScan: TAfterDirectoryScan
               read FAfterDirectoryScan write FAfterDirectoryScan;
    property BeforeDirectoryScan: TBeforeDirectoryScan
               read FBeforeDirectoryScan write FBeforeDirectoryScan;
    property OnFileFound: TOnFileFound
               read FOnFileFound write FOnFileFound;
  end;

procedure Register;

implementation

uses FileCtrl;

constructor TdgFileFinder.Create(AOwner: TComponent);
begin
  inherited;
  FIncludeSystemFiles := False;
end;

function TdgFileFinder.GetWindowsErrorMsg(ErrorCode: DWORD): String;
const
  BuffSize = 1024;
var
  MsgBuff:        PChar;
begin
  MsgBuff := AllocMem(BuffSize);
  try
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, nil,
      ErrorCode, 0, MsgBuff, BuffSize, nil);
    Result := StrPas(MsgBuff);
  finally
    FreeMem(MsgBuff);
  end; //try
end;

procedure TdgFileFinder.RaiseWindowsError(ErrorCode: DWORD);
begin
  if (ErrorCode <> ERROR_NO_MORE_FILES) and
     (ErrorCode <> ERROR_FILE_NOT_FOUND) then
    raise EFileFinderError.Create(GetWindowsErrorMsg(ErrorCode) +
      ' (' + IntToStr(ErrorCode) + ')');
end;

function TdgFileFinder.Win32FileTimeToDateTime(FTime: _FILETIME): TDateTime;
var
  LocalTime: _FILETIME;
  SysTime:   _SYSTEMTIME;
begin
  FileTimeToLocalFileTime(_FILETIME(FTime), LocalTime);
  FileTimeToSystemTime(LocalTime, SysTime);
  Result := SystemTimeToDateTime(SysTime);
end;

function TdgFileFinder.ScanDirectory( var Path: String;
                                      const Mask: String;
                                      const ScanSubdirectories: Boolean): Boolean;
{Called by FindFiles to scan the directory tree. This
 method calls itself recursively.

 Parameters:
   Path:               The path to the directory to scan.
   Mask:               The mask for the files to be found.
   ScanSubdirectories: True to scan entire directory tree.}

{
  TSearchRec = record
    Time: Integer;    Size: Integer;    Attr: Integer;    Name: TFileName;    ExcludeAttr: Integer;    FindHandle: THandle;    FindData: TWin32FindData;  end;

  TWin32FindData = packed record
    dwFileAttributes: Integer;
    ftCreationTime: Int64;
    ftLastAccessTime: Int64;
    ftLastWriteTime: Int64;
    nFileSizeHigh: Integer;
    nFileSizeLow: Integer;
    dwReserved0: Integer;
    dwReserved1: Integer;
    cFileName: array[0..259] of Char;
    cAlternateFileName: array[0..13] of Char;
  end;
}   
var
  SRec:           TSearchRec;
  SearchResult:   Integer;
  PathLength:     Integer;
  ContinueSearch: Boolean;
  {File information.}
  LongName,
  ShortName:      String;
  AttributeStr:   String;
  Created,
  Accessed,
  Updated:        Variant;
  FileSize:       Int64;
  {Directory information.}
  DirLongName,
  DirShortName:   String;
  DirLastCreated,
  DirLastAccessed,
  DirLastUpdated: Variant;
  DirSize:        Int64;
begin
  Result := True;
  DirSize := 0;
  DirLastCreated := 0;
  DirLastAccessed := 0;
  DirLastUpdated := 0;
  {See if the user wants to cancel the scan.}
  Application.ProcessMessages;
  if FCancelScan then Exit;

  {Get the record for this directory.}
  with SRec.FindData do
  begin
    if ((ExtractFileDrive(Path) <> '') and (Length(Path) = 3)) or
       (Path = '\') then
    begin
      DirLongName := '\';
      DirShortName := '\';
    end else begin
      SearchResult := FindFirst(Copy(Path, 1, Length(Path) - 1),
        faDirectory, SRec);
      try
        if SearchResult <> 0 then RaiseWindowsError(SearchResult);
        DirLongName := StrPas(cFileName);
        DirShortName := StrPas(cAlternateFileName);
      finally
        FindClose(SRec);
      end; //try
    end; //if
  end; //with

  {Fire the BeforeDirectoryScan event.}
  ContinueSearch := True;
  if Assigned(FBeforeDirectoryScan) then
  begin
    FBeforeDirectoryScan(Path,
               DirLongName,
               DirShortName,
               ContinueSearch);
  end; //if

  if not ContinueSearch then
  begin
    Result := False;
    FindClose(SRec);
    Exit;
  end; //if

  {Save the Path length}
  PathLength := Length(Path);
  {Search for all files in the current directory.}
  SearchResult := FindFirst(Path + Mask, faAnyFile, SRec);
  if SearchResult <> 0 then RaiseWindowsError(SearchResult);
  try
    while SearchResult = 0 do
    begin
      {If the found file is not a directory or volume label process it.}
      if (SRec.Attr and (faDirectory or faVolumeID)) = 0 then
      begin
        with SRec.FindData do
        begin
          {If IncludeSystemFiles is False and this is a system file
           skip it.}
          if (not FIncludeSystemFiles) and
             (dwFileAttributes and FILE_ATTRIBUTE_HIDDEN <> 0) and
             (dwFileAttributes and FILE_ATTRIBUTE_SYSTEM <> 0) then
          begin
            SearchResult := FindNext(SRec);
            if SearchResult <> 0 then RaiseWindowsError(SearchResult);
            Continue;
          end;
          {Process this file.}
          LongName := StrPas(cFileName);
          ShortName := StrPas(cAlternateFileName);
          if ShortName = '' then ShortName := LongName;
          try
            Created := Win32FileTimeToDateTime(ftCreationTime);
          except
            Created := Null;
          end; //try
          try
            Accessed := Win32FileTimeToDateTime(ftLastAccessTime);
          except
            Accessed := Null;
          end; //try
          try
            Updated := Win32FileTimeToDateTime(ftLastWriteTime);
          except
            Updated := Null;
          end; //try
          FileSize := (nFileSizeHigh * MAXINT) + nFileSizeLow;
          {Build the string of attibutes.}
          AttributeStr := '';
          if dwFileAttributes and FILE_ATTRIBUTE_NORMAL = 0 then
          begin
            if dwFileAttributes and FILE_ATTRIBUTE_ARCHIVE <> 0 then
              AttributeStr := AttributeStr + 'A';
            if dwFileAttributes and FILE_ATTRIBUTE_COMPRESSED <> 0 then
              AttributeStr := AttributeStr + 'C';
            if dwFileAttributes and FILE_ATTRIBUTE_HIDDEN <> 0 then
              AttributeStr := AttributeStr + 'H';
            if dwFileAttributes and FILE_ATTRIBUTE_SYSTEM <> 0 then
              AttributeStr := AttributeStr + 'S';
            if dwFileAttributes and FILE_ATTRIBUTE_OFFLINE <> 0 then
              AttributeStr := AttributeStr + 'O';
            if dwFileAttributes and FILE_ATTRIBUTE_READONLY <> 0 then
              AttributeStr := AttributeStr + 'R';
            if dwFileAttributes and FILE_ATTRIBUTE_TEMPORARY <> 0 then
              AttributeStr := AttributeStr + 'T';
          end; //if
        end; //with
        {Update the totals for the current directory.}
        DirSize := DirSize + FileSize;
        if DirLastCreated < Created then DirLastCreated := Created;
        if DirLastAccessed < Accessed then DirLastAccessed := Accessed;
        if DirLastUpdated < Updated then DirLastUpdated := Updated;
        {Fire the OnFileFound event.}
        ContinueSearch := True;
        if Assigned(FOnFileFound) then
        begin
          FOnFileFound(Path,
                       LongName,
                       ShortName,
                       AttributeStr,
                       Created,
                       Accessed,
                       Updated,
                       FileSize,
                       ContinueSearch);
        end; //if
        if not ContinueSearch then
        begin
          Result := False;
          Break;
        end; //if
      end;
      {Find the next file.}
      SearchResult := FindNext(SRec);
      if SearchResult <> 0 then RaiseWindowsError(SearchResult);
    end; //while
  finally
    FindClose(SRec);
  end; //try
  if not Result then Exit;

    {Fire the AfterDirectoryScan event.}
  ContinueSearch := True;
  if Assigned(FAfterDirectoryScan) then
  begin
    FAfterDirectoryScan(Path,
               DirLongName,
               DirShortName,
               DirLastCreated,
               DirLastAccessed,
               DirLastUpdated,
               DirSize,
               ContinueSearch);
  end; //if

  if not ContinueSearch then
  begin
    Result := False;
    Exit;
  end; //if

  {Find subdirectories in the current directory.}
  if ScanSubdirectories then
  begin
    SearchResult := FindFirst(Path + '*.*', faDirectory, SRec);
    if SearchResult <> 0 then RaiseWindowsError(SearchResult);
    try
      while SearchResult = 0 do
      begin
        if (SRec.Attr and faDirectory) <> 0 then
          {If this is the current directory or parent directory symbol
           skip it.}
          if (SRec.Name <> '.') and (SRec.Name <> '..') then
          begin
            {Add the directory name to the path.}
            Path := Path + SRec.Name + '\';
            {Scan the directory.}
            if not ScanDirectory(Path, Mask, ScanSubdirectories) then
            begin
              Result := False;
              Break;
            end;
            {Delete the directory name from the path.}
            Delete(Path, PathLength + 1, 255);
          end;
        SearchResult := FindNext(SRec);
        if SearchResult <> 0 then RaiseWindowsError(SearchResult);
      end; //while
    finally
      FindClose(SRec);
    end; //try
  end; //if
end;

procedure TdgFileFinder.FindFiles( const Path: String;
                                   const Mask: String;
                                   const ScanSubdirectories: Boolean);
{Searches the specified directory tree for files that
 match the specified path.

 Parameters:
   Path:       The path to the directory in which to start
               the search. If the path is a null stirng
               the current directory is used.
   Mask:       The mask that describes the files to search
               for. DOS wild cards (* and ?) may be used.
               If the mask is a null string *.* is used.}
var
  SearchPath: String;
begin
  FCancelScan := False;
  {If no path is supplied use the current directory.}
  if Path = '' then
    GetDir(0, SearchPath)
  else begin
    if not DirectoryExists(Path) then
    begin
      raise EFileFinderError.Create('The folder ' + Path + ' does not exist.');
      Exit;
    end; //if
    SearchPath := Path;
  end; //if
  {Append a backslash to the path.}
  if SearchPath[Length(SearchPath)] <> '\' then
    SearchPath := SearchPath + '\';
  {If no mask is supplied use *.*.}
  if Mask = '' then
    ScanDirectory(SearchPath, '*.*', ScanSubdirectories)
  else
    ScanDirectory(SearchPath, Mask, ScanSubdirectories);
end;

procedure Register;
begin
  RegisterComponents('Extras', [TdgFileFinder]);
end;

end.
