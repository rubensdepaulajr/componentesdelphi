{    ***********************************************************
     * Esta � a primeira vers�o de um componente que           *
     * permite a impress�o direta  para impressora matricial   *
     * possibitando a defini�ao de linha e coluna onde ser�    *
     * impresso o texto.                                       *
     *---------------------------------------------------------*
     * This is a first version of a component that allow       *
     * a direct port (LTP1, COM1..) print, and you can define  *
     * line and column where that text will be printed         *
     *---------------------------------------------------------*
     * Por...: Paulo Roberto Quicoli                           *
     * e-mail: paulo_quicoli@hotmail.com                       *
     * data..: 09/11/2001                                      *
     * Jaboticabal-S�o Paulo-Brasil                            *
     ***********************************************************
}

unit MatrixPrinter;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs;

type
  TMatrixPrinter = class(TComponent)


  private
      Arquivo       : TextFile;
      iLinha, iColuna : Integer;
      function fAlignString(sValue: string; iNum: integer; One: TAlignment): string;
      procedure pImprimir(iLn : Integer; iCl: Integer; iLen: Integer; sTxt: string; Aln: TAlignment);
      procedure pIniciaImpressora;

          //     fLineFeed := '#13#10';
//     fEjectPage := '#12' ;
//     fBoldOn := '#27#69';
//     fBoldOff := '#27#70';
//     fItalicOn := '#27#52';
//     fItalicOff := '#27#53';
//     fCondensedOn := '#15';
//     fCondensedOff := '#18';
////     fDoubleStrikeOn := '#27#87#49';
////     fDoubleStrikeOff := '#27#87#48';
//     fUnderlineOn := '#27#45#49';
//     fUnderlineOff := '#27#45#48';
//     fReset := '#27#64';      iNICIA IMPRESSORA


      procedure pNegritoOn;
      procedure pNegritoOff;
      procedure pExpandidoOn;
      procedure pExpandidoOff;
      procedure pCondensadoOn;
      procedure pCondensadoOff;
      procedure pItalicoOn;
      procedure pItalicoOff;
      procedure pNormaliza;
      procedure pEjetar;
      procedure pReduz12cpi;
      procedure pSublinhadoOn;
      procedure pSublinhadoOff;
      procedure pTamanhoPagina(iLinhas : Integer);
      procedure pEspacamentodaLinha(iNumero: Integer);
      function  fTrocaAcentos(sTexto: string):string;
  protected
    { Protected declarations }
  public
    { Public declarations }
      procedure pBeginPrintProcess(sPort: string);
      procedure pEndPrintProcess;
      procedure pInitializePrinter;
      procedure pPrint(iLine : Integer; iColumn : Integer; iLength: Integer; sText: string; Align: TAlignment = taLeftJustify);
      procedure pBoldOn;
      procedure pBoldOff;
      procedure pExpOn;
      procedure pExpOff;
      procedure pCondensedOn;
      procedure pCondensedOff;
      procedure pItalicOn;
      procedure pItalicOff;
      procedure pNormal;
      procedure pEject;
      procedure pR12cpi;
      procedure pUnderlineOn;
      procedure pUnderlineOff;
      procedure pSetLengthPageinLines(iLines: Integer);
      procedure pLineSpacingMode(iMode : Integer);
      function  fPCol : Integer;
      function  fPRow : Integer;
  published
    { Published declarations }
  end;

procedure Register;


implementation

procedure Register;
begin
  RegisterComponents('allSoft', [TMatrixPrinter]);
end;

// Alinha a String de acordo com o alinhamento selecionado
function TMatrixPrinter.fAlignString(sValue: string; iNum: integer; One: TAlignment): string;
var
   k, iValue, iSize: Integer;
begin
  iSize := length(sValue);
  if iSize < iNum then
    begin
      case one of
        taCenter        : begin
                            iValue := (iNum - iSize) div 2;
                            for k := 1 to iValue + 1 do
                              sValue := ' ' + sValue + ' ' ;
                            // Se o tamanho da string for um n� impar, o �tlimo espa�o deve ser retirado
                            if Length(sValue) > iSize then
                              Delete(sValue,Length(sValue),1);
                          end;
        taRightJustify  : for k := 1 to iNum - iSize {+ 1} do
                            sValue := ' ' + sValue;
        taLeftJustify   : for k := 1 to iNum - iSize {+ 1} do
                            sValue := sValue + ' ' ;
      end;

      Result := sValue;
      //for k := 1 to Length(TheStr) do
      //    PrintCaracters(TheStr[k]);
    end
  else
     Result := sValue;
    //for k := 1 to Num do
      //PrintCaracters(TheStr[k]);
end;

//***************************************** Troca Acentos
function TMatrixPrinter.fTrocaAcentos(sTexto : string):string;
const
  ComAcento = '����������������������������';
var
  x : Integer;
begin
  for x := 1 to Length(ComAcento) do
    if Pos(ComAcento[x],sTexto) <> 0 then
      case x of
       01 : sTexto[Pos(ComAcento[x],sTexto)] := chr(133);
       02 : sTexto[Pos(ComAcento[x],sTexto)] := chr(131);
       03 : sTexto[Pos(ComAcento[x],sTexto)] := chr(136);
       04 : sTexto[Pos(ComAcento[x],sTexto)] := chr(147);
       05 : sTexto[Pos(ComAcento[x],sTexto)] := chr(150);
       08 : sTexto[Pos(ComAcento[x],sTexto)] := chr(160);
       09 : sTexto[Pos(ComAcento[x],sTexto)] := chr(130);
       10 : sTexto[Pos(ComAcento[x],sTexto)] := chr(161);
       11 : sTexto[Pos(ComAcento[x],sTexto)] := chr(162);
       12 : sTexto[Pos(ComAcento[x],sTexto)] := chr(163);
       13 : sTexto[Pos(ComAcento[x],sTexto)] := chr(135);
       14 : sTexto[Pos(ComAcento[x],sTexto)] := chr(129);
       23 : sTexto[Pos(ComAcento[x],sTexto)] := chr(144);
       27 : sTexto[Pos(ComAcento[x],sTexto)] := chr(128);
       28 : sTexto[Pos(ComAcento[x],sTexto)] := chr(154);
      end;
  Result := sTexto;
end;


//***************************************** Espa�amento da Linha;
// mode 2 � default
procedure TMatrixPrinter.pLineSpacingMode(iMode :Integer);
begin
  pEspacamentodaLinha(iMode);
end;

procedure TMatrixPrinter.pEspacamentodaLinha(iNumero : integer);
begin
  if not ((iNumero < 0) or (iNumero > 2)) then
    Write(Arquivo,CHR(27)+CHR(45)+ '1' );
end;


//***************************************** Sublinhados;
procedure TMatrixPrinter.pUnderlineOn;
begin
  pSublinhadoOn;
end;

procedure TMatrixPrinter.pUnderlineOff;
begin
  pSublinhadoOff;
end;

procedure TMatrixPrinter.pSublinhadoOn;
begin
  Write(arquivo,CHR(27)+CHR(45)+ CHR(49){'1'} );
end;

procedure TMatrixPrinter.pSublinhadoOff;
begin
  Write(arquivo,CHR(27)+CHR(45)+ CHR(48){'0'} );
end;


//*********************************** Tamanho da pagina em Linhas;
procedure TMatrixPrinter.pSetLengthPageinLines(iLines : Integer);
begin
  pTamanhoPagina(iLines);
end;

procedure TMatrixPrinter.pTamanhoPagina(iLinhas : integer);
begin
  Write(Arquivo,CHR(27)+CHR(67)+ InttoStr(iLinhas) );
end;

//*********************************** Reduz 12 cpi;
procedure TMatrixPrinter.pR12cpi;
begin
  pReduz12cpi;
end;

procedure TMatrixPrinter.pReduz12cpi;
begin
  Write(arquivo,CHR(27)+CHR(77));
end;

//*********************************** Eject;
procedure TMatrixPrinter.pEject;
begin
  pEjetar;
end;

procedure TMatrixPrinter.pEjetar;
begin
  Write(arquivo,chr(12));
end;

//*********************************** Normal;
procedure TMatrixPrinter.pNormal;
begin
  pNormaliza;
end;

procedure TMatrixPrinter.pNormaliza;
begin
  Write(Arquivo,CHR(27)+CHR(080));
end;

//************************************* It�licos
procedure TMatrixPrinter.pItalicOn;
begin
  pItalicoOn;
end;

procedure TMatrixPrinter.pItalicOff;
begin
  pItalicoOff;
end;

procedure TMatrixPrinter.pItalicoOn;
begin
  Write(Arquivo,CHR(27)+CHR(52));
end;

procedure TMatrixPrinter.pItalicoOff;
begin
  Write(Arquivo,CHR(27)+CHR(53));
end;

//**************************************  Condensados
procedure TMatrixPrinter.pCondensedOn;
begin
  pCondensadoOn;
end;

procedure TMatrixPrinter.pCondensedOff;
begin
  pCondensadoOff;
end;

procedure TMatrixPrinter.pCondensadoOn;
begin
  Write(Arquivo,CHR(15));
end;

procedure TMatrixPrinter.pCondensadoOff;
begin
  Write(Arquivo,CHR(18));
end;

//******************************** Expandidos
procedure TMatrixPrinter.pExpOn;
begin
  pExpandidoOn;
end;

procedure TMatrixPrinter.pExpOff;
begin
  pExpandidoOff;
end;

procedure TMatrixPrinter.pExpandidoOn;
begin
  Write(Arquivo,CHR(27)+CHR(87)+CHR(1));
end;

procedure TMatrixPrinter.pExpandidoOff;
begin
  Write(Arquivo,CHR(27)+CHR(87)+CHR(0));
end;

//*********************************** Negritos
procedure TMatrixPrinter.pBoldOn;
begin
  pNegritoOn;
end;

procedure TMatrixPrinter.pBoldOff;
begin
  pNegritoOff;
end;

procedure TMatrixPrinter.pNegritoOn;
begin
  Write(Arquivo,CHR(27)+CHR(69));
end;

procedure TMatrixPrinter.pNegritoOff;
begin
  Write(Arquivo,CHR(27)+CHR(70));
end;

//******************************* Retorna coluna
function  TMatrixPrinter.fPCol: Integer;
var
  iCol1 : integer;
begin
  iCol1   := iColuna;
  Result  := iCol1;
end;

//******************************* Retorna Linha
function TMatrixPrinter.fPRow: Integer;
var
  iLn1 : integer;
begin
  iLn1 := iLinha;
  Result := iLn1;
end;

//********************************************* Inicia Impressora
procedure TMatrixPrinter.pInitializePrinter;
begin
  pIniciaImpressora;
end;

procedure TMatrixPrinter.pIniciaImpressora;
begin
  Write(Arquivo,Chr(27) + chr(64));
end;

//*******************************************
// Prepara impressora e arquivo
// de impress�o
//*******************************************
procedure TMatrixPrinter.pBeginPrintProcess(sPort : string);
begin
  AssignFile(Arquivo,sPort);
  Rewrite(Arquivo);
  Write(Arquivo,Chr(27) + chr(64)); // reseta impressora;
  iLinha  := 0;
  iColuna := 0;
end;

//*******************************************
// Finaliza a impress�o com um Eject e
// fecha arquivo de impress�o
//*******************************************
procedure TMatrixPrinter.pEndPrintProcess;
begin
  Write(Arquivo,chr(12));
  CloseFile(Arquivo);
end;

//*******************************************
// Chama o m�todo que imprime
//*******************************************
procedure TMatrixPrinter.pPrint(iLine: Integer; iColumn: Integer; iLength: Integer;sText : string; Align: TAlignment = taLeftJustify);
begin
  pImprimir(iLine, iColumn, iLength, sText, Align);
end;

//**************************************************
// M�todo que imprime na linha e coluna especificada
//**************************************************
procedure TMatrixPrinter.pImprimir(iLn : Integer; iCl: Integer; iLen: Integer; sTxt : string; Aln: TAlignment);
var
  iSalto    : Integer;
  x         : Integer;
  iDiferenca: Integer;
begin
  if iLn < iLinha then
    raise exception.Create('Linha informada inv�lida !!! ' + #13 +'n� ' + IntToStr(iLn))
  else
    begin
      if iLn > iLinha then
        begin
          iSalto := iLn - iLinha;
          iColuna := 0;
          for x := 1 to iSalto do
            Writeln(arquivo,'');
        end;
      iLinha := iLn;
    end;

  if iCl < iColuna then
    raise Exception.Create('Coluna informada inv�lida !!!'+ #13 +'n� '+ InttoStr(iCl))
  else
  if iCl > 0 then
    iDiferenca := (iCl - iColuna);

  for x := 0 to iDiferenca do
    begin
      Write(Arquivo,' ');
      Inc(iColuna);
    end;

  // Definindo o Alinhamento da string a ser impressa
  sTxt := fAlignString(sTxt,iLen, Aln);

  for x:= 0 to iLen do // length(sTxt)
    begin
      if sTxt[x] = '�' then
        Write(Arquivo,chr(96) + chr(8) + chr(65))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(94) + chr(8) + chr(69))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(94) + chr(8) + chr(79))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(94) + chr(8) + chr(85))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(126) + chr(8) + chr(65))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(126) + chr(8) + chr(79))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(39) + chr(8) + chr(65))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(39) + chr(8) + chr(69))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(39) + chr(8) + chr(79))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(39) + chr(8) + chr(73))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(39) + chr(8) + chr(85))
      else
      if sTxt[x] = '�' then
        Write(Arquivo,chr(126) + chr(8) + chr(97))
      else
      if sTxt[x] = '�' then
        Write(Arquivo, chr(126) + chr(8) + chr(111))
      else
        Write(Arquivo,fTrocaAcentos(sTxt[x]));
      Inc(iColuna);
    end; //for x:= 0 to length(Texto) do ...
  Dec(iColuna);
end;

end.
