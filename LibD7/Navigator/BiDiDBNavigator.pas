unit BiDiDBNavigator;

{

 TBiDiDBNavigator v1.0 for Delphi
 by Kambiz R. Khojasteh

 mail: khojasteh@www.dci.co.ir
 web: http://www.crosswinds.net/~khojasteh/

 This component is freeware and may be used in any software product (free or commercial).

}

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, DBCtrls, DsgnIntf;

type


{ TBiDiDBNavigator }

  TBiDiDBNavigator = class(TDBNavigator)
  private
    procedure SwapButtons;
    procedure SwapGlyphs;
    function Swaped: Boolean;
    procedure SetGlyphs(Index: TNavigateBtn; Glyph: TBitmap);
    function GetGlyphs(Index: TNavigateBtn): TBitmap;
  protected
    procedure Loaded; override;
    procedure WMSize(var Msg: TWMSize); message WM_SIZE;
    procedure CMBiDiModeChanged(var Msg: TMessage); message CM_BIDIMODECHANGED;
  public
    property Glyphs[Index: TNavigateBtn]: TBitmap read GetGlyphs write SetGlyphs;
  published
    property BiDiMode;
    property ParentBiDiMode;
    property GlyphFirst: TBitmap index nbFirst read GetGlyphs write SetGlyphs;
    property GlyphPrior: TBitmap index nbPrior read GetGlyphs write SetGlyphs;
    property GlyphNext: TBitmap index nbNext read GetGlyphs write SetGlyphs;
    property GlyphLast: TBitmap index nbLast read GetGlyphs write SetGlyphs;
    property GlyphInsert: TBitmap index nbInsert read GetGlyphs write SetGlyphs;
    property GlyphDelete: TBitmap index nbDelete read GetGlyphs write SetGlyphs;
    property GlyphEdit: TBitmap index nbEdit read GetGlyphs write SetGlyphs;
    property GlyphPost: TBitmap index nbPost read GetGlyphs write SetGlyphs;
    property GlyphCancel: TBitmap index nbCancel read GetGlyphs write SetGlyphs;
    property GlyphRefresh: TBitmap index nbRefresh read GetGlyphs write SetGlyphs;
  end;

procedure Register;

implementation

{ TBiDiDBNavigator }

procedure TBiDiDBNavigator.Loaded;
begin
  inherited Loaded;
  if UseRightToLeftAlignment and not Swaped then
    SwapButtons;
end;

procedure TBiDiDBNavigator.WMSize(var Msg: TWMSize);
begin
  inherited;
  if not (csLoading in ComponentState) and UseRightToLeftAlignment and not Swaped then
    SwapButtons;
end;

procedure TBiDiDBNavigator.CMBiDiModeChanged(var Msg: TMessage);
var
  B: TNavigateBtn;
begin
  for B := Low(Buttons) to High(Buttons) do
    Buttons[B].BidiMode := BiDiMode;             // for right to left hint
  if UseRightToLeftAlignment <> Swaped then
  begin
    SwapButtons;
    if not (csReading in ComponentState) and not (csLoading in ComponentState) then
      SwapGlyphs;
  end;
end;

procedure TBiDiDBNavigator.SwapButtons;
var
  X: Integer;
  LB, RB: TNavigateBtn;
begin
  LB := Low(Buttons);
  RB := High(Buttons);
  repeat
    while not (LB in VisibleButtons) and (LB < High(Buttons)) do Inc(LB);
    while not (RB in VisibleButtons) and (RB > Low(Buttons)) do Dec(RB);
    if LB < RB then
    begin
      X := Buttons[LB].Left;
      Buttons[LB].Left := Buttons[RB].Left;
      Buttons[RB].Left := X;
      Inc(LB);
      Dec(RB);
    end;
  until LB >= RB;
end;

procedure TBiDiDBNavigator.SwapGlyphs;
var
  Glyph: TBitmap;
begin
  Glyph := TBitmap.Create;
  try
    Glyph.Assign(Buttons[nbLast].Glyph);
    Buttons[nbLast].Glyph.Assign(Buttons[nbFirst].Glyph);
    Buttons[nbFirst].Glyph.Assign(Glyph);
    Glyph.Assign(Buttons[nbNext].Glyph);
    Buttons[nbNext].Glyph.Assign(Buttons[nbPrior].Glyph);
    Buttons[nbPrior].Glyph.Assign(Glyph);
  finally
    Glyph.Free;
  end;
end;

function TBiDiDBNavigator.Swaped: Boolean;
var
  LB, RB: TNavigateBtn;
begin
  LB := Low(Buttons);
  RB := High(Buttons);
  while not (LB in VisibleButtons) and (LB < High(Buttons)) do Inc(LB);
  while not (RB in VisibleButtons) and (RB > Low(Buttons)) do Dec(RB);
  Result := Buttons[LB].Left > Buttons[RB].Left;
end;

procedure TBiDiDBNavigator.SetGlyphs(Index: TNavigateBtn; Glyph: TBitmap);
begin
  Buttons[Index].Glyph.Assign(Glyph);
end;

function TBiDiDBNavigator.GetGlyphs(Index: TNavigateBtn): TBitmap;
begin
  Result := Buttons[Index].Glyph;
end;

procedure Register;
begin
  RegisterComponents('Data Controls', [TBiDiDBNavigator]);
end;

end.
