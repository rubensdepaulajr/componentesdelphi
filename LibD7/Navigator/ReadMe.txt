TBiDiDBNavigator for Delphi
---------------------------

Author: Kambiz R. Khojasteh
EMail: khojasteh@www.dci.co.ir
Web: http://www.crosswinds.net/~khojasteh/


TDBNavigator is a visual component for Delphi 4 and 5. This component is a descendent
of standard TDBNavigator of Delphi. I add only some properties to this component for 
bi-directional support and custom glyphs. 

This component is freeware. You can use it, rewrite-it. 

If you have problems, suggestions, bug reports mail me.

Thanks.

Kambiz
