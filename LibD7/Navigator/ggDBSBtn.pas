unit ggDBSBtn;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, db;

type
  TggBitBtnStyles = (ggFirst, ggPrior, ggNext, ggLast, ggEdit, ggInsert, ggPost, ggDelete, ggCancel, ggRefresh);

  TggBitBtnDataLink = Class;

  EggDBBitBtnBeforeActionClick = procedure (Sender: TObject) of object;

  TggDBSpeedButton = class(TSpeedButton)
  private
    FDataLink             : TggBitBtnDataLink;
    FggBitBtnStyle        : TggBitBtnStyles;
    FConfirmDelete        : Boolean;
    FDeleteRecordQuestion : String;
    FBeforeAction         : EggDBBitBtnBeforeActionClick;
    function GetDataSource: TDataSource;
    procedure SetDataSource(Value: TDataSource);
    procedure CMEnabledChanged(var Message: TMessage); message CM_ENABLEDCHANGED;
  protected
    procedure SetFggBitBtnStyle(Value: TggBitBtnStyles);
    procedure SetggButtonStyle;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure DataChanged;
    procedure EditingChanged;
    procedure ActiveChanged;
    procedure Loaded; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Click; override;
  published
    property ggBitBtnStyle          : TggBitBtnStyles              read FggBitBtnStyle        write SetFggBitBtnStyle;
    property ggDataSource           : TDataSource                  read GetDataSource         write SetDataSource;
    property ggConfirmDelete        : boolean                      read FConfirmDelete        write FConfirmDelete;
    property ggDeleteRecordQuestion : String                       read FDeleteRecordQuestion write FDeleteRecordQuestion;
    property ggBeforeAction         : EggDBBitBtnBeforeActionClick read FBeforeAction         write FBeforeAction;
  end;


  TggBitBtnDataLink = class(TDataLink)
  private
    FggDBBitBtn: TggDBSpeedButton;
  protected
    procedure EditingChanged; override;
    procedure DataSetChanged; override;
    procedure ActiveChanged; override;
  public
    constructor Create(aggDBBitBtn: TggDBSpeedButton);
    destructor Destroy; override;
  end;



procedure Register;

implementation

{$R ggDBSBtn}

const
  GG_FIRST   = 0;
  GG_PRIOR   = 1;
  GG_NEXT    = 2;
  GG_LAST    = 3;
  GG_EDIT    = 4;
  GG_INSERT  = 5;
  GG_POST    = 6;
  GG_DELETE  = 7;
  GG_CANCEL  = 8;
  GG_REFRESH = 9;

var
  ggBtnTypeName: array[TggBitBtnStyles] of PChar = ('FIRST', 'PRIOR', 'NEXT',
                                                    'LAST', 'EDIT', 'INSERT',
                                                    'POST', 'DELETE', 'CANCEL', 'REFRESH');



////////////////////////////////////////////////////////////////////////////////
//                non class related procedures & functions                    //
////////////////////////////////////////////////////////////////////////////////
procedure Register;
begin
  RegisterComponents('GuidoG', [TggDBSpeedButton]);
end;
////////////////////////////////////////////////////////////////////////////////
//             end of the non class related procedures & functions            //
////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////
//                            Component TggDBSpeedButton                           //
////////////////////////////////////////////////////////////////////////////////
procedure TggDBSpeedButton.SetFggBitBtnStyle(Value: TggBitBtnStyles);
begin
  if Value <> FggBitBtnStyle then
    begin
      FggBitBtnStyle := Value;
      SetggButtonStyle;
    end;
end;


constructor TggDBSpeedButton.Create(AOwner: TComponent);
begin
  inherited Create(aOwner);
  FDataLink             := TggBitBtnDataLink.Create(Self);
  FggBitBtnStyle        := TggBitBtnStyles(GG_FIRST);
  SetggButtonStyle;
  FDeleteRecordQuestion := 'Delete record ?';
  FConfirmDelete        := TRUE;
  Caption               := '';
end;


destructor TggDBSpeedButton.Destroy;
begin
  FDataLink.Free;
  FDataLink := nil;
  inherited Destroy;
end;


procedure TggDBSpeedButton.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation = opRemove) and (FDataLink <> nil) and (AComponent = ggDataSource) then
    ggDataSource := nil;
end;


procedure TggDBSpeedButton.DataChanged;
var
  UpEnable,
  DnEnable  : Boolean;
begin
  UpEnable := FDataLink.Active and not FDataLink.DataSet.BOF;
  DnEnable := FDataLink.Active and not FDataLink.DataSet.EOF;

  case ord(FggBitBtnStyle) of
    GG_FIRST   : Enabled := UpEnable;
    GG_PRIOR   : Enabled := UpEnable;
    GG_NEXT    : Enabled := DnEnable;
    GG_LAST    : Enabled := DnEnable;
    GG_DELETE  : Enabled := FDataLink.Active and
                            FDataLink.DataSet.CanModify and not
                            (FDataLink.DataSet.BOF and FDataLink.DataSet.EOF);
  end;
end;


procedure TggDBSpeedButton.EditingChanged;
var
  CanModify: Boolean;
begin
  CanModify := FDataLink.Active and FDataLink.DataSet.CanModify;

  case ord(FggBitBtnStyle) of
    GG_EDIT    : Enabled := CanModify and not FDataLink.Editing;
    GG_INSERT  : Enabled := CanModify;
    GG_POST    : Enabled := CanModify and FDataLink.Editing;
    GG_CANCEL  : Enabled := CanModify and FDataLink.Editing;
    GG_REFRESH : Enabled := CanModify;
  end;
end;


procedure TggDBSpeedButton.ActiveChanged;
begin
  if not (FDataLink.Active) then
    Enabled := FALSE
  else
    begin
      DataChanged;
      EditingChanged;
    end;
end;


procedure TggDBSpeedButton.CMEnabledChanged(var Message: TMessage);
begin
  inherited;
  if not (csLoading in ComponentState) then
    ActiveChanged;
end;


procedure TggDBSpeedButton.Loaded;
begin
  inherited Loaded;
  ActiveChanged;
end;



procedure TggDBSpeedButton.SetggButtonStyle;
var
  ResName: string;
begin
  case ord(FggBitBtnStyle) of
    GG_FIRST   : Hint := 'First';
    GG_PRIOR   : Hint := 'Prior';
    GG_NEXT    : Hint := 'Next';
    GG_LAST    : Hint := 'Last';
    GG_EDIT    : Hint := 'Edit';
    GG_INSERT  : Hint := 'Insert';
    GG_POST    : Hint := 'Post';
    GG_DELETE  : Hint := 'Delete';
    GG_CANCEL  : Hint := 'Cancel';
    GG_REFRESH : Hint := 'Refresh';
  end;

  FmtStr(ResName, 'ggDBSpeedButton_%s', [ggBtnTypeName[self.FggBitBtnStyle]]);
  Glyph.LoadFromResourceName(HInstance, ResName);
  NumGlyphs := 2;

  DataChanged;
  EditingChanged;
end;


procedure TggDBSpeedButton.SetDataSource(Value: TDataSource);
begin
  FDataLink.DataSource := Value;
  if not (csLoading in ComponentState) then
    ActiveChanged;
  if Value <> nil then
    Value.FreeNotification(Self);
end;


function TggDBSpeedButton.GetDataSource: TDataSource;
begin
  Result := FDataLink.DataSource;
end;


procedure TggDBSpeedButton.Click;
begin
  if (ggDataSource <> nil) and (ggDataSource.State <> dsInactive) then
  begin
    if not (csDesigning in ComponentState) and Assigned(FBeforeAction) then
      FBeforeAction(Self);
    with ggDataSource.DataSet do
      begin
        case ord(FggBitBtnStyle) of
          GG_FIRST   : First;
          GG_PRIOR   : Prior;
          GG_NEXT    : Next;
          GG_LAST    : Last;
          GG_EDIT    : Edit;
          GG_INSERT  : Insert;
          GG_POST    : Post;
          GG_DELETE  : if not FConfirmDelete or (MessageDlg(FDeleteRecordQuestion,
                                                           mtConfirmation,
                                                           mbOKCancel,
                                                           0) <> idCancel) then
                         Delete;

          GG_CANCEL  : Cancel;
          GG_REFRESH : Refresh;
        end;
      end;
  end;
  inherited;
end;
////////////////////////////////////////////////////////////////////////////////
//                           end of Component TggDBSpeedButton                     //
////////////////////////////////////////////////////////////////////////////////







////////////////////////////////////////////////////////////////////////////////
//                           Class TggBitBtnDataLink                          //
////////////////////////////////////////////////////////////////////////////////
constructor TggBitBtnDataLink.Create(aggDBBitBtn: TggDBSpeedButton);
begin
  inherited Create;
  FggDBBitBtn   := aggDBBitBtn;
  VisualControl := True;
end;


destructor TggBitBtnDataLink.Destroy;
begin
  FggDBBitBtn := nil;
  inherited Destroy;
end;


procedure TggBitBtnDataLink.EditingChanged;
begin
  if FggDBBitBtn <> nil then
    FggDBBitBtn.EditingChanged;
end;


procedure TggBitBtnDataLink.DataSetChanged;
begin
  if FggDBBitBtn <> nil then
    FggDBBitBtn.DataChanged;
end;


procedure TggBitBtnDataLink.ActiveChanged;
begin
  if FggDBBitBtn <> nil then
    FggDBBitBtn.ActiveChanged;
end;
////////////////////////////////////////////////////////////////////////////////
//                           end of Class TggBitBtnDataLink                   //
////////////////////////////////////////////////////////////////////////////////


end.
