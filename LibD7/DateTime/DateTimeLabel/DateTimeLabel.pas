// ***************************************
//              DateTimeLabel
//       Alwasys shows date and time
//  (C) Copyright 1997 by Oliver Killguss
// ***************************************
// Version     : 1.0
// Datum       : 30.12.1997
// Last Update : 28.09.2001
// ***************************************

unit DateTimeLabel;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls;

type
  TTimeDate = class(TLabel)
  private
    { Private-Deklarationen }
    FShowDate, FShowTime,
    FBreakLine            : Boolean;
    FWindowHandle         : HWND;
    procedure WndProc(var MSG: TMessage);
  protected
    { Protected-Deklarationen }
  public
    { Public-Deklarationen }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    { Published-Deklarationen }
    Property ShowTime: Boolean read FShowTime write FShowTime;
    Property ShowDate: Boolean read FShowDate write FShowDate;
    Property BreakLine: Boolean read FBreakLine write FBreakLine;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('DDM', [TTimeDate]);
end;

constructor TTimeDate.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  WordWrap      := True;
  Alignment     := taCenter;
  Caption       := DateToStr(Date) + #13 + TimeToStr(Time);
  ShowDate      := true;
  ShowTime      := true;
  BreakLine     := true;
  FWindowHandle := AllocateHWND(WNDProc);
  KillTimer(FWindowHandle,1);
  SetTimer(FWindowHandle,1,1000,nil);
end;

destructor TTimeDate.Destroy;
begin
  KillTimer(FWindowHandle,1);
  DeallocateHwnd(FWindowHandle);
  inherited Destroy;
end;

procedure TTimeDate.WndProc(var MSG: TMessage);
begin
  with msg do
  if Msg = WM_Timer then
  begin
    if ShowDate then
      Caption := DateToStr(Date);
    if (ShowTime and ShowDate) and (breakLine) then
      Caption := Caption + #13;
    if (ShowTime and ShowDate) and (not breakLine) then
      Caption := Caption + ' - ';
    if ShowTime and ShowDate then
      Caption := Caption + TimeToStr(Time)
    else if ShowTime and not ShowDate then
      Caption := TimeToStr(Time)
    else if not showTime and not showDate then
      Caption := '';
  end;
end;

end.
