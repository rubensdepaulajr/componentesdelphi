unit NumInt;
interface
uses
  StdCtrls,Classes,SysUtils,Controls,Forms,Graphics, ExtCtrls;
type
  TRealLabelFormat = (fInteger);
  TNumInt = class(TLabeledEdit)
  private
    { Private declarations }
    procedure SetValue(valor: Integer);
    function  GetValue: Integer;
    procedure OnFim(Sender: TObject);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    protected
    procedure KeyPress(var Key: Char); override;
    procedure Change; override;
    { Public declarations }
    public
      constructor Create( AOwner: TComponent ); override;
      property Value: Integer read GetValue write SetValue;
    published

  end;

procedure Register;

implementation


constructor TNumInt.Create( AOwner: TComponent );
begin
  inherited Create( AOwner );
  Height  := 21;
  Width	  := 121;
  Caption := 'TrLabelEdit';
  Text	  := '0';
  OnExit  := OnFim;
end;


procedure TNumInt.OnFim(Sender: TObject);
begin
  try
    SetValue(GetValue);
  except
    SetValue(0);
  end;
end;


procedure TNumInt.SetValue(valor: Integer);

begin
  Text := IntToStr(valor);
end;

function  TNumInt.GetValue: Integer;

begin
  result := StrToInt(Text);
end;



procedure TNumInt.CMEnter(var Message: TCMEnter);
begin
  SelectAll;
  inherited;
end;

procedure TNumInt.KeyPress(var Key: Char);

begin
  if Not (Key in ['0'..'9','-',chr(8)]) Then
    begin
      Key := #0;
      inherited KeyPress(Key);
      exit;
    end;
  inherited KeyPress(Key);
end;
procedure TNumInt.Change;
begin
  If Length(Trim(Text)) = 0 then
    Text := '0';
end;
procedure Register;
begin
  RegisterComponents('Extras', [TNumInt]);
end;

end.


