unit UDemo;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls, Paswd;

type
  TForm1 = class(TForm)
    Panel2: TPanel;
    Label1: TLabel;
    Bevel1: TBevel;
    Label2: TLabel;
    Label3: TLabel;
    Password2: TPassword;
    procedure Password1Validate(Sender: TObject; UserName,
      Password: String; var Valid: Boolean);
    procedure Password1Error(Sender: TObject; Error: TPasswordError);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.DFM}


procedure TForm1.Password1Validate(Sender: TObject; UserName,
  Password: String; var Valid: Boolean);
begin
  if (UserName = 'TCF') and (Password = '1234') then
    Valid := True
  else
    ShowMessage('O nome de usu�rio e senha informado n�o s�o v�lidos.');
end;

procedure TForm1.Password1Error(Sender: TObject; Error: TPasswordError);
begin
  if Error = peInput then
    begin
      Application.ShowMainForm := False;
      Application.Terminate;
    end
  else
    begin
      ShowMessage('O usu�rio atual n�o pode ser confirmado. ' +
        'A aplica��o ser� terminada.');
      Application.Terminate;
    end;
end;

end.
