unit Barra;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DsgnIntf;

type
  TMode = (modvASPMES);

  TdvASPMES = class(TComponent)
  private
    FAbout  : string;
    FInput  : string;
    FResult : string;
    FMode   : TMode;
    procedure SetInput(Value: string);
    procedure SetMode(Value: TMode);
    procedure SetASPMES(Value: string);
    procedure SetDATA(Value: string);
    procedure SetResult(Value: string);
    procedure ShowAbout;
  protected
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
  published
    property About    : string  read FAbout     write FAbout      stored  False;
    property Input    : string  read FInput     write SetInput;
    property Mode     : TMode   read FMode      write SetMode;
    property Result   : string  read FResult    write SetResult;
  end;

procedure Register;

implementation

{#######################################################################}

type
  TAboutProperty = class(TPropertyEditor)
  public
    procedure Edit; override;
    function  GetAttributes: TPropertyAttributes; override;
    function  GetValue : string; override;
  end;

procedure TAboutProperty.Edit;
{Invoke the about dialog when clicking on ... in the Object Inspector}
begin
  TdvASPMES(GetComponent(0)).ShowAbout;
end;

function TAboutProperty.GetAttributes: TPropertyAttributes;
{Make settings for just displaying a string in the ABOUT property in the
Object Inspector}
begin
  GetAttributes := [paDialog, paReadOnly];
end;

function TAboutProperty.GetValue: String;
{Text in the Object Inspector for the ABOUT property}
begin
  GetValue := '(About)';
end;

procedure TdvASPMES.ShowAbout;
var
  msg: string;
const
  carriage_return = chr(13);
begin
  msg := 'Dv da ASPMES  v1.2';
  AppendStr(msg, carriage_return);
  AppendStr(msg, 'Componente EXCLUSIVO');
  AppendStr(msg, carriage_return);
  AppendStr(msg, carriage_return);
  AppendStr(msg, 'Carlos Kretli Tesch');
  AppendStr(msg, carriage_return);
  AppendStr(msg, carriage_return);
  AppendStr(msg, '');
  ShowMessage(msg);
end;

{#######################################################################}

constructor TdvASPMES.Create( Aowner: Tcomponent);
begin
  inherited Create( Aowner );
  FInput  := '';
  FResult := '';
  FMode   := moDVASPMES;
end;

destructor TdvASPMES.Destroy;
begin
  inherited Destroy;
end;

procedure TdvASPMES.SetMode(Value: TMode);
begin
  if FMode <> Value then
  begin
    FMode := Value;
    SetInput(FInput);
  end;
end;

procedure TdvASPMES.SetInput(Value: string);
begin
  FInput := Value;
  case FMode of
    moDVASPMES: SetASPMES(Value);
  end;
end;

procedure TdvASPMES.SetASPMES(Value: string);
var
  localASPMES    : string;
  localResult    : string;
  digit1         : integer;
  ii,soma        : integer;
begin
  localASPMES := '';
  localResult := '';

  {analisa DV da ASPMES no formato 99.99999-?}
  if Length(FInput) = 7 then
    begin
    localASPMES := FInput;
    localResult := '';
    end;

  {comeca a verificacao do digito}
      soma := 0;
      for ii := 1 to 7 do Inc(soma, StrToInt(Copy(localASPMES, 8-ii, 1))*(ii+1));
      digit1 := 11 - (soma mod 11);
      if digit1 > 9 then
         digit1 := 0;
      localResult := IntToStr(digit1);
      FResult := localResult;
end;

procedure TdvASPMES.SetDATA(Value: string);
var
  localASPMES    : string;
  localResult    : string;
begin
  localASPMES := '';
  localResult := '';

    begin
    localASPMES := FInput;
    localResult := '';
    end;
    // Formato ??/?/?//????
    if (copy(localASPMES,3,1)+copy(localASPMES,5,1)+copy(localASPMES,7,2)) = '////' then
       localResult := copy(localASPMES,1,2)+
                      copy(localASPMES,4,1)+
                      copy(localASPMES,6,1)+
                      copy(localASPMES,9,4);
    // Formato ??/??/????
    if (copy(localASPMES,3,1)+copy(localASPMES,6,1)) = '//' then
       localResult := copy(localASPMES,1,2)+
                      copy(localASPMES,4,2)+
                      copy(localASPMES,7,4);

    FResult     := localResult;
end;


procedure TdvASPMES.SetResult(Value: string);
begin
  {do nothing  //  read only}
end;

procedure Register;
begin
  RegisterComponents('Samples', [TdvASPMES]);
  RegisterPropertyEditor(TypeInfo(String), TdvASPMES, 'About',
  	TAboutProperty);
end;

end.
