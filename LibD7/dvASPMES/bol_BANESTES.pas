unit bol_BANESTES;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs;

    function  CAD2(wCAD2: String): Integer;

type
  TBoleto = class(TComponent)
  private
    wValor:         Real;
    wRegistro:      String;
    wNNumero:       String;
    wVectoT:        String;
    wFVecto:        String;
    wCAsbace:       String;
    wCBarras:       String;
    wLDigitavel:    String;
    wCCorrente:     String;
    wTipo:          Integer;
    cktValor:       String;
    procedure SetValor(Value: real);
    procedure SetRegistro(Value: String);
    procedure SetNNumero(Value: String);
    procedure SetVectoT(Value: String);
    procedure SetFVecto(Value: String);
    procedure SetCAsbace(Value: String);
    procedure SetCBarras(Value: String);
    procedure SetLDigitavel(Value: string);
    procedure SetCCorrente(Value: string);
    procedure SetTipo(Value: Integer);
  protected
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
  published
    property _Valor:         real    read wValor           write SetValor;
    property _Registro:      String  read wRegistro        write SetRegistro;
    property NNumero:        String  read wNNumero         write SetNNumero;
    property _VectoT:        String  read wVectoT          write SetVectoT;
    property FVecto:         String  read wFVecto          write SetFVecto;
    property CAsbace:        String  read wCAsbace         write SetCAsbace;
    property CBarras:        String  read wCBarras         write SetCBarras;
    property LDigitavel:     String  read wLDigitavel      write SetLDigitavel;
    property _CCorrente:     String  read wCCorrente       write SetCCorrente;
    property _Tipo:          Integer read wTipo            write SetTipo;
  end;

procedure Register;

implementation


constructor TBoleto.Create( Aowner: Tcomponent);
begin
  inherited Create( Aowner );
  wTipo           := 2;
  wValor          := 0;
  wRegistro       := '0';
  wVectoT         := '03/07/2000';
  wCCorrente      := '0';
end;

destructor TBoleto.Destroy;
begin
  inherited Destroy;
end;

procedure TBoleto.SetValor(Value: real);
begin
     wValor            := Value;
     cktValor          := FloatToStr(wValor*100);
     while Length(cktValor) < 10 do cktValor := '0'+cktValor;
     if (wValor         > 0) and
        (length(wFVecto)= 4) and
        (length(wCAsbace)=25) then
        SetCBarras('0219'+wFVecto+cktValor+copy(wCAsbace,1,25));
     if (length(wCBarras)  = 44) and
        (length(wFVecto)   = 4)  and
        (wValor            > 0)  and
        (length(wCAsbace)  = 25) then
        SetLDigitavel(wCAsbace);
end;

procedure TBoleto.SetRegistro(Value: String);
begin
     wRegistro         := Value;
     SetNNumero(wRegistro);
end;

procedure TBoleto.SetNNumero(Value: String);
var
   soma, ii, Resto1, Resto2: integer;
begin
     wNNumero         := Value;
     //---------- 1 Digito -------------
     while Length(wNNumero) < 8 do wNNumero := '0'+wNNumero;
     soma             := 0;
     for ii           := 1 to 8 do Inc(soma, StrToInt(Copy(wNNumero, 9-ii, 1))*(ii+1));
     Resto1           := (soma mod 11);
     if (Resto1        = 0) or
        (Resto1        = 1) then
        Resto1        := 0;
     if Resto1         > 1 then
        Resto1        := 11-Resto1;
     wNNumero         := wNNumero+IntToStr(Resto1);
     //---------- 2 Digito -------------
     while Length(wNNumero) < 9 do wNNumero := '0'+wNNumero;
     soma             := 0;
     for ii           := 1 to 9 do Inc(soma, StrToInt(Copy(wNNumero, 10-ii, 1))*(ii+1));
     Resto2           := (soma mod 11);
     if (Resto2        = 0) or
        (Resto2        = 1) then
        Resto2        := 0;
     if Resto2         > 1 then
        Resto2        := 11-Resto2;
     wNNumero         := wNNumero+IntToStr(Resto2);
     if (Length(wNNumero)   = 10) and
        (Length(wCCorrente) = 11) then
        SetCAsbace(copy(wNNumero,1,8)+wCCorrente+IntToStr(wTipo)+'021');
end;

procedure TBoleto.SetVectoT(Value: String);
begin
     wVectoT          := Value;
     SetFVecto(FloatToStr((StrToDate(wVectoT)-StrToDate('03/07/2000'))+1000));
end;

procedure TBoleto.SetFVecto(Value: String);
begin
     wFVecto          := Value;
     if (wValor         > 0) and
        (length(wFVecto)= 4) and
        (length(wCAsbace)=25) then
        SetCBarras('0219'+wFVecto+cktValor+copy(wCAsbace,1,25));
     if (length(wCBarras)  = 44) and
        (length(wFVecto)   = 4)  and
        (wValor            > 0)  and
        (length(wCAsbace)  = 25) then
        SetLDigitavel(wCAsbace);
end;

procedure TBoleto.SetCAsbace(Value: String);
var
   d: array[1..25] of Integer;
   Digito1, Digito2, gg, Soma, Pdt: Integer;
begin
     wCAsbace          := Value;
     Soma              := 0;
     //----------- 1 Digito ----------------
     for gg            := 1 to 23 do
         begin
              d[gg]    := StrToInt(Copy(wCAsbace,gg,1));
              if gg/2  <> int(gg/2) then
                 Pdt   := d[gg] * 2
              else
                 Pdt   := d[gg] * 1;
              //--------------------------------------//
              if Pdt    > 9 then
                 Pdt   := Pdt - 9;
              Soma     := Soma + Pdt;
         end;
     Digito1           := (Soma mod 10);
     if Digito1         > 0 then
        Digito1        := 10-Digito1;
     wCAsbace          := wCAsbace + IntToStr(Digito1);
     //----------- 2 Digito ----------------
     Digito2           := CAD2(wCAsbace);
     //--------------------------------------//
     if Digito2         = 1 then
        Digito2        := CAD2(Copy(wCAsbace,1,23)+ IntToStr(Digito1+1));
     if Digito2         = 9 then
        Digito2        := CAD2(Copy(wCAsbace,1,23)+ IntToStr(0));
     if Digito2         > 1 then
        Digito2        := 11 - Digito2;
     wCAsbace          := wCAsbace + IntToStr(Digito2);
     if (wValor         > 0) and
        (length(wFVecto)= 4) and
        (length(wCAsbace)=25) then
        SetCBarras('0219'+wFVecto+cktValor+copy(wCAsbace,1,25));
     if (length(wCBarras)  = 44) and
        (length(wFVecto)   = 4)  and
        (wValor            > 0)  and
        (length(wCAsbace)  = 25) then
        SetLDigitavel(wCAsbace);
end;

function  CAD2(wCAD2: String): Integer;
var
   Soma, Resultado, gg: integer;
   d: array[1..25] of Integer;
begin
     for gg            := 1 to 24 do
         d[gg]         := StrToInt(Copy(wCAD2,gg,1));
     Soma              := (d[01]*7)+
                          (d[02]*6)+
                          (d[03]*5)+
                          (d[04]*4)+
                          (d[05]*3)+
                          (d[06]*2)+
                          (d[07]*7)+
                          (d[08]*6)+
                          (d[09]*5)+
                          (d[10]*4)+
                          (d[11]*3)+
                          (d[12]*2)+
                          (d[13]*7)+
                          (d[14]*6)+
                          (d[15]*5)+
                          (d[16]*4)+
                          (d[17]*3)+
                          (d[18]*2)+
                          (d[19]*7)+
                          (d[20]*6)+
                          (d[21]*5)+
                          (d[22]*4)+
                          (d[23]*3)+
                          (d[24]*2);
     Resultado         := (Soma mod 11);
     Result            := Resultado;
end;

procedure TBoleto.SetCBarras(Value: string);
var
   Soma, gg, Digito: integer;
   d: array[1..43] of Integer;
begin
     wCBarras          := Value;
     for gg            := 1 to 43 do
         d[gg]         := StrToInt(Copy(wCBarras,gg,1));
     Soma              := (d[01]*4)+
                          (d[02]*3)+
                          (d[03]*2)+
                          (d[04]*9)+
                          (d[05]*8)+
                          (d[06]*7)+
                          (d[07]*6)+
                          (d[08]*5)+
                          (d[09]*4)+
                          (d[10]*3)+
                          (d[11]*2)+
                          (d[12]*9)+
                          (d[13]*8)+
                          (d[14]*7)+
                          (d[15]*6)+
                          (d[16]*5)+
                          (d[17]*4)+
                          (d[18]*3)+
                          (d[19]*2)+
                          (d[20]*9)+
                          (d[21]*8)+
                          (d[22]*7)+
                          (d[23]*6)+
                          (d[24]*5)+
                          (d[25]*4)+
                          (d[26]*3)+
                          (d[27]*2)+
                          (d[28]*9)+
                          (d[29]*8)+
                          (d[30]*7)+
                          (d[31]*6)+
                          (d[32]*5)+
                          (d[33]*4)+
                          (d[34]*3)+
                          (d[35]*2)+
                          (d[36]*9)+
                          (d[37]*8)+
                          (d[38]*7)+
                          (d[39]*6)+
                          (d[40]*5)+
                          (d[41]*4)+
                          (d[42]*3)+
                          (d[43]*2);
     Digito            := (Soma mod 11);
     if (Digito         = 0) or
        (Digito         = 1) or
        (Digito         = 10) then
        Digito         := 1
     else
        Digito         := 11-Digito;
     wCBarras          := '0219'+IntToStr(Digito)+wFVecto+cktValor+copy(wCAsbace,1,25);
     if (length(wCBarras)  = 44) and
        (length(wFVecto)   = 4)  and
        (wValor            > 0)  and
        (length(wCAsbace)  = 25) then
        SetLDigitavel(wCAsbace);
end;

procedure TBoleto.SetLDigitavel(Value: String);
var
   Campo1, Campo2, Campo3: String;
   Soma, gg, Pdt, Digito1, Digito2, Digito3: Integer;
   d: array[1..10] of Integer;
begin
     wLDigitavel       := Value;
     //------------ Campo 1 ------------------//
     Soma              := 0;
     Campo1            := '0219'+copy(wCAsbace,1,5);
     for gg            := 1 to 9 do
         begin
              d[gg]    := StrToInt(Copy(Campo1,gg,1));
              if gg/2  <> int(gg/2) then
                 Pdt   := d[gg] * 2
              else
                 Pdt   := d[gg] * 1;
              if Pdt    > 9 then
                 Pdt   := Pdt - 9;
              Soma     := Soma + Pdt;
         end;
     if Soma            < 10 then
        Digito1        := Soma
     else
        Digito1        := (Soma mod 10);
     if Digito1         > 0 then
        Digito1        := 10-Digito1;
     //------------ Campo 2 ------------------//
     Soma              := 0;
     Campo2            := copy(wCAsbace,6,10);
     for gg            := 1 to 10 do
         begin
              d[gg]    := StrToInt(Copy(Campo2,gg,1));
              if gg/2  <> int(gg/2) then
                 Pdt   := d[gg] * 1
              else
                 Pdt   := d[gg] * 2;
              if Pdt    > 9 then
                 Pdt   := Pdt - 9;
              Soma     := Soma + Pdt;
         end;
     if Soma            < 10 then
        Digito2        := Soma
     else
        Digito2        := (Soma mod 10);
     if Digito2         > 0 then
        Digito2        := 10-Digito2;
     //------------ Campo 3 ------------------//
     Soma              := 0;
     Campo3            := copy(wCAsbace,16,10);
     for gg            := 1 to 10 do
         begin
              d[gg]    := StrToInt(Copy(Campo3,gg,1));
              if gg/2  <> int(gg/2) then
                 Pdt   := d[gg] * 1
              else
                 Pdt   := d[gg] * 2;
              if Pdt    > 9 then
                 Pdt   := Pdt - 9;
              Soma     := Soma + Pdt;
         end;
     if Soma            < 10 then
        Digito3        := Soma
     else
        Digito3        := (Soma mod 10);
     if Digito3         > 0 then
        Digito3        := 10-Digito3;
     //------------ Campo 4 ------------------//
     // Digito Verificador do wCBarras => Copy(wCBarras,5,1)
     //------------ Campo 5 ------------------//
     // Fator de Vecto + Valor         => wFVecto+cktValor;

     wLDigitavel       := Campo1+IntToStr(Digito1)+
                          Campo2+IntToStr(Digito2)+
                          Campo3+IntToStr(Digito3)+
                          Copy(wCBarras,5,1)+
                          wFVecto+cktValor;
end;

procedure TBoleto.SetCCorrente(Value: String);
begin
     wCCorrente    := Value;
     while length(wCCorrente) < 11 do wCCorrente := '0'+wCCorrente;
     if (Length(wNNumero)   = 10) and
        (Length(wCCorrente) = 11) then
        SetCAsbace(copy(wNNumero,1,8)+wCCorrente+IntToStr(wTipo)+'021');
end;

procedure TBoleto.SetTipo(Value: Integer);
begin
     wTipo      := Value;
     if (wTipo =  0) or
        (wTipo >  9) then
        wTipo   := 2;
     if (Length(wNNumero)   = 10) and
        (Length(wCCorrente) = 11) then
        SetCAsbace(copy(wNNumero,1,8)+wCCorrente+IntToStr(wTipo)+'021');
end;

procedure Register;
begin
  RegisterComponents('Additional', [TBoleto]);
end;

end.
