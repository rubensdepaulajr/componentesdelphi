inherited fFluxoResetarConsumidor: TfFluxoResetarConsumidor
  Left = 360
  Top = 227
  Caption = 'Selecione o pr'#243'ximo item do fluxo'
  OldCreateOrder = True
  PixelsPerInch = 96
  TextHeight = 13
  inherited Panel1: TPanel
    inherited grd: TDBGrid
      Columns = <
        item
          Expanded = False
          FieldName = 'CD_FLUXO'
          Width = 46
          Visible = True
        end
        item
          Expanded = False
          FieldName = 'TR_DESCRICAO'
          Width = 510
          Visible = True
        end>
    end
  end
  inherited setMod: TADODataSet
    Connection = dmGeral.ADOConnection
    CommandText = 
      'SELECT'#9'CD_FLUXO, TR_DESCRICAO +'#39' '#39'+ ISNULL(FX_SERVICO,'#39#39') AS TR_' +
      'DESCRICAO'#13#10'FROM dbo.FLUXO, dbo.TAREFA'#13#10'WHERE CD_TAREFA = FX_CD_T' +
      'AREFA AND'#13#10#9'TR_PERGUNTA = 0 AND'#13#10#9'FX_VERSAO = :fx_versao AND'#13#10#9'F' +
      'X_TIPO = :fx_tipo'#13#10'ORDER BY CD_FLUXO DESC'
    Parameters = <
      item
        Name = 'fx_versao'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'fx_tipo'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    object setModCD_FLUXO: TAutoIncField
      DisplayLabel = 'C'#243'digo'
      FieldName = 'CD_FLUXO'
      ReadOnly = True
    end
    object setModTR_DESCRICAO: TStringField
      DisplayLabel = 'Descri'#231#227'o'
      FieldName = 'TR_DESCRICAO'
      ReadOnly = True
      Size = 101
    end
  end
  object cmd: TADOCommand
    CommandText = 
      'INSERT dbo.fluxo_cliente'#13#10#9'(fc_cd_cliente'#13#10#9',fc_cd_fluxo'#13#10#9',fc_c' +
      'd_funcionario'#13#10#9',fc_dt_limite'#13#10#9',fc_dt_inicio'#13#10#9',fc_dt_fim'#13#10#9',fc' +
      '_situacao'#13#10#9',fc_justificativa'#13#10#9',fc_observacao)'#13#10'VALUES'#13#10#9'(:fc_c' +
      'd_cliente'#13#10#9',:fc_cd_fluxo'#13#10#9',:fc_cd_funcionario'#13#10#9',:fc_dt_limite' +
      #13#10#9',:fc_dt_inicio'#13#10#9',:fc_dt_fim'#13#10#9',:fc_situacao'#13#10#9',:fc_justifica' +
      'tiva'#13#10#9',:fc_observacao)'
    Connection = dmGeral.ADOConnection
    Parameters = <
      item
        Name = 'fc_cd_cliente'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'fc_cd_fluxo'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'fc_cd_funcionario'
        Attributes = [paSigned, paNullable]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = Null
      end
      item
        Name = 'fc_dt_limite'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'fc_dt_inicio'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'fc_dt_fim'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'fc_situacao'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 1
        Value = Null
      end
      item
        Name = 'fc_justificativa'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'fc_observacao'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end>
    Left = 57
    Top = 117
  end
end
