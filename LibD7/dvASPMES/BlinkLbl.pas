unit BlinkLbl;

interface

uses
  Classes, StdCtrls, ExtCtrls;

type
  TBlinkLabel = class(TLabel)
  private
    { Private declarations }
        FVelocidad      : Integer;
        FTimer          : TTimer;
        procedure SetVelocidad (valor: Integer);
  protected
    { Protected declarations }
        procedure ParpaDea (Sender: TObject);
  public
    { Public declarations }
        Constructor Create(aOwner: TComponent); override;       {Constructor}
        Destructor Destroy; override;                           {Destructor}
  published
    { Published declarations }
        Property Velocidad: Integer read FVelocidad write SetVelocidad default 400;
  end;

procedure Register;

implementation

Constructor TBlinkLabel.Create(AOwner: TComponent);
begin
        inherited Create(AOwner);                               {Chama construtor original}
        FTimer          := TTimer.Create(Self);                 {Cria o Timer}
        FVelocidad      := 400;                                 {Frequencia padrao}
        FTimer.Enabled  := True;                                {Ativar o Timer}
        FTimer.OnTimer  := ParpaDea;                            {Pegamos o metodo pisca-pisca}
        FTimer.Interval := FVelocidad;                          {Pegamos o intervalo Timer}
end;

Destructor TBlinkLabel.Destroy;
begin
        FTimer.Free;
        inherited destroy;
end;

procedure TBlinkLabel.SetVelocidad (valor: Integer);
begin
        if FVelocidad <> valor then
           begin
                if valor < 0 then
                   FVelocidad     := 0;
                FVelocidad        := Valor;
                if FVelocidad      = 0 then
                   FTimer.Enabled := false
                else
                   FTimer.Enabled := true;
                FTimer.Interval   := FVelocidad;
                Visible           := true;
           end;
end;

procedure TBlinkLabel.ParpaDea (Sender: TObject);
begin
        if FTimer.Enabled then
           Visible      := not(Visible);
end;

procedure Register;
begin
  RegisterComponents('Standard', [TBlinkLabel]);
end;

end.
