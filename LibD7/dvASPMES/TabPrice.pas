unit TabPrice;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DesignEditors, DesignIntf;

type
  TPrice = class(TComponent)
  private
    wFinanciado:    Real;
    wParcelas:      Real;
    wTaxa:          Real;
    wPrestacoes:    Real;
    wASPMES:        String;
    wASPMESDV:      String;
    wAFPES:         String;
    wAFPESDV:       String;
    wCNPJ:          String;
    wCNPJ_DV:       String;
    wCPF:           String;
    wCPF_DV:        String;
    wIdIN:          string;
    wIdFim:         string;
    wIdade:         string;
    wIdade_Num:     Real; 
    procedure SetFinanciado(Value: real);
    procedure SetParcelas(Value: real);
    procedure SetTaxa(Value: real);
    procedure SetPrestacoes(Value: real);
    procedure SetASPMES(Value: String);
    procedure SetASPMESDV(Value: String);
    procedure SetAFPES(Value: String);
    procedure SetAFPESDV(Value: String);
    procedure SetCNPJ(Value: string);
    procedure SetCNPJDV(Value: string);
    procedure SetCPF(Value: string);
    procedure SetCPFDV(Value: string);
    procedure SetIdIN(Value: string);
    procedure SetIdFIM(Value: string);
    procedure SetIdade(Value: string);
    procedure SetIdade_Num(Value: real);

  protected
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
  published
    property Financiado:     real    read wFinanciado      write SetFinanciado;
    property Parcelas:       real    read wParcelas        write SetParcelas;
    property Taxa:           real    read wTaxa            write SetTaxa;
    property Vl_Prestacao:   real    read wPrestacoes      write SetPrestacoes;
    property ASPMES:         String  read wASPMES          write SetASPMES;
    property ASPMES_DV:      String  read wASPMESDV        write SetASPMESDV;
    property AFPES:          String  read wAFPES           write SetAFPES;
    property AFPES_DV:       String  read wAFPESDV         write SetAFPESDV;
    property _CNPJ:          String  read wCNPJ            write SetCNPJ;
    property _CNPJ_DV:       String  read wCNPJ_DV         write SetCNPJDV;
    property _CPF:           String  read wCPF             write SetCPF;
    property _CPF_DV:        String  read wCPF_DV          write SetCPFDV;
    property Idade_Inicio:   string  read wIdIN            Write SetIdIN;
    property Idade_Fim:      string  read wIdFIM           Write SetidFIM;
    property Idade:          string  read wIdade           Write SetIdade;
    property Idade_Num:      Real    read wIdade_Num       write SetIdade_Num;
  end;

procedure Register;

implementation

{#######################################################################}

type
  TAboutProperty = class(TPropertyEditor)
  public
    procedure Edit; override;
    function  GetAttributes: TPropertyAttributes; override;
    function  GetValue : string; override;
  end;

procedure TAboutProperty.Edit;
{Invoke the about dialog when clicking on ... in the Object Inspector}
begin
end;

function TAboutProperty.GetAttributes: TPropertyAttributes;
{Make settings for just displaying a string in the ABOUT property in the
Object Inspector}
begin
  GetAttributes := [paDialog, paReadOnly];
end;

function TAboutProperty.GetValue: String;
{Text in the Object Inspector for the ABOUT property}
begin
  GetValue := '(About)';
end;

{#######################################################################}

constructor TPrice.Create( Aowner: Tcomponent);
begin
  inherited Create( Aowner );
end;

destructor TPrice.Destroy;
begin
  inherited Destroy;
end;

procedure TPrice.SetFinanciado(Value: real);
var
   iprimeiro,isegundo,iindice: Real;
   pont: integer;
   aparc: String;
begin
     wFinanciado       := Value;
     if (wFinanciado   <> 0) and
        (wParcelas     <> 0) and
        (wTaxa         <> 0) then
        begin
             aparc             := FloatToStr(wParcelas);
             iprimeiro         := 1+(wTaxa/100);
             for pont          := 2 to StrToInt(aParc) do
                 iprimeiro     := iprimeiro * (1+(wTaxa/100));
             isegundo          := iprimeiro;
             iprimeiro         := iprimeiro - 1;
             isegundo          := (wTaxa/100)*isegundo;
             iindice           := iprimeiro/isegundo;
             wPrestacoes       := wFinanciado/iindice;
        end;
end;

procedure TPrice.SetParcelas(Value: real);
begin
     wParcelas         := Value;
     SetFinanciado(wFinanciado);
end;

procedure TPrice.SetTaxa(Value: real);
begin
     wTaxa             := Value;
     SetFinanciado(wFinanciado);
end;

procedure TPrice.SetPrestacoes(Value: real);
begin
end;

procedure TPrice.SetASPMESDV(Value: String);
begin
     wASPMESDV   := Value;
     SetASPMES(wASPMES);
end;

procedure TPrice.SetASPMES(Value: String);
var
   soma, ii, digit1: integer;
begin
     wASPMES := Value;
     while Length(wASPMES) < 7 do wASPMES := '0'+wASPMES;
     soma := 0;
     for ii := 1 to 7 do Inc(soma, StrToInt(Copy(wASPMES, 8-ii, 1))*(ii+1));
     digit1 := 11 - (soma mod 11);
     if digit1 > 9 then
        digit1 := 0;
     wASPMESDV := wASPMES+IntToStr(digit1);
end;

procedure TPrice.SetAFPESDV(Value: String);
begin
     wAFPESDV   := Value;
     SetAFPES(wAFPES);
end;

procedure TPrice.SetAFPES(Value: String);
var
   tAFPES: String;
   dg1,dg2,soma: Real;
   d: array[1..7] of Integer;
   gg: Integer;

begin
     wAFPES             := Value;
     while Length(wAFPES) < 6 do wAFPES := '0'+wAFPES;
     if Length(wAFPES)   = 6 then
        begin
             tAFPES     := '65'+Copy(wAFPES,2,5);
             for gg     := 1 to 7 do
                 d[gg]  := StrToInt(Copy(tAFPES,gg,1));
             soma       := (d[1]*10)+
                           (d[2]* 9)+
                           (d[3]* 8)+
                           (d[4]* 7)+
                           (d[5]* 6)+
                           (d[6]* 5)+
                           (d[7]* 4);
             dg1        := Int(soma/11)*11;
             dg1        := soma-dg1;
             if (dg1     = 1) or (dg1   = 0) then
                dg1     := 0
             else
                dg1     := 11-dg1;
        //--------------------------------------------------------------------//
             soma       := (d[1]*11)+
                           (d[2]*10)+
                           (d[3]* 9)+
                           (d[4]* 8)+
                           (d[5]* 7)+
                           (d[6]* 6)+
                           (d[7]* 5);
             dg2        := Int(soma/11)*11;
             dg2        := soma-dg2;
             if (dg2     = 1) or (dg2   = 0) then
                dg2     := 0
             else
                dg2     := 11-dg2;
             wAFPESDV   := wAFPES+FloatToStr(dg1)+FloatToStr(dg2);
        end;
end;

procedure TPrice.SetCNPJ(Value: string);
var
  ii,soma,digit1,digit2:        integer;
begin
      wCNPJ                     := Value;
      while Length(wCNPJ)        < 12 do wCNPJ := '0'+wCNPJ;
      wCNPJ                     := Copy(wCNPJ,1,12);
      {1� digito}
      soma                      := 0;
      for ii                    := 1 to 12 do
      begin
        if ii < 5 then
          Inc(soma, StrToInt(Copy(wCNPJ, ii, 1))*(6-ii))
        else
          Inc(soma, StrToInt(Copy(wCNPJ, ii, 1))*(14-ii))
      end;
      digit1                    := 11 - (soma mod 11);
      if digit1 > 9 then digit1 := 0;
      wCNPJ_DV                  := wCNPJ+IntToStr(digit1);
//------------------------------------------------------------------------------
      {2� digito}
      soma := 0;
      for ii := 1 to 13 do
      begin
        if ii < 6 then
          Inc(soma, StrToInt(Copy(wCNPJ_DV, ii, 1))*(7-ii))
        else
          Inc(soma, StrToInt(Copy(wCNPJ_DV, ii, 1))*(15-ii))
      end;
      digit2                    := 11 - (soma mod 11);
      if digit2 > 9 then digit2 := 0;
      wCNPJ_DV                  := wCNPJ_DV+IntToStr(digit2);
end;

procedure TPrice.SetCNPJDV(Value: String);
begin
     wCNPJ_DV   := Value;
end;

procedure TPrice.SetCPF(Value: string);
var
  ii,soma,digit1,digit2:        integer;
begin
      wCPF                      := Value;
      while Length(wCPF)         < 9 do wCPF := '0'+wCPF;
      wCPF                      := Copy(wCPF,1,9);
      soma                      := 0;
      for ii                    := 1 to 9 do Inc(soma, StrToInt(Copy(wCPF, 10-ii, 1))*(ii+1));
      digit1                    := 11 - (soma mod 11);
      if digit1 > 9 then digit1 := 0;
      wCPF_DV                   := wCPF+IntToStr(digit1);
      //-----------------------------------------------------------------------
      {2� digito}
      soma                      := 0;
      for ii                    := 1 to 10 do Inc(soma, StrToInt(Copy(wCPF_DV, 11-ii, 1))*(ii+1));
      digit2                    := 11 - (soma mod 11);
      if digit2 > 9 then digit2 := 0;
      wCPF_DV                   := wCPF_DV+IntToStr(digit2);
end;

procedure TPrice.SetCPFDV(Value: String);
begin
     wCPF_DV   := Value;
end;

procedure tprice.SetIdIN(Value: string);
begin
     wIdIN      := Value;
     if (Length(wIdIN)   = 10) and
        (Length(wIdFim)  = 10) then
        SetIdade(wIdIN);
end;

procedure tprice.SetIdFIM(Value: string);
begin
     wIdFim     := Value;
     if (Length(wIdIN)   = 10) and
        (Length(wIdFim)  = 10) then
        SetIdade(wIdFim);
end;

procedure tprice.SetIdade_Num(Value: Real);
begin
     wIdade_Num := Value;
end;

procedure tprice.SetIdade(Value: string);
var
     xInicio, xFim : TDateTime;
     iYear, iMonth, iDay: Word;
     fYear, fMonth, fDay: Word;
     rrAno, rrMes, rrDia: Integer;
begin
     xInicio    := StrToDate(wIdIN);
     xFim       := StrToDate(wIdFim);
     DecodeDate(xInicio, iYear, iMonth, iDay);
     DecodeDate(xFim,    fYear, fMonth, fDay);
     rrAno      := fYear-iYear;
     rrMes      := fMonth-iMonth;
     rrDia      := fDay-iDay;
     if rrDia    < 0 then
        begin
             rrMes := rrMes - 1;
             rrDia := 30 + rrDia;
        end;
     if rrMes    < 0 then
        begin
             rrAno := rrAno - 1;
             rrMes := 12 + rrMes;
        end;
     wIdade     := IntToStr(rrAno)+'a '+IntToStr(rrMes)+'m '+IntToStr(rrDia)+'d.';
     wIdade_Num := rrAno;
end;

procedure Register;
begin
  RegisterComponents('Additional', [TPrice]);
  RegisterPropertyEditor(TypeInfo(String), TPrice, 'About',
  	TAboutProperty);
end;

end.
