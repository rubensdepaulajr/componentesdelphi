unit uFluxoResetarConsumidor;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, uModSel, DB, ADODB, Grids, DBGrids, Buttons, ExtCtrls;

type
  TfFluxoResetarConsumidor = class(TfModSel)
    setModCD_FLUXO: TAutoIncField;
    setModTR_DESCRICAO: TStringField;
    cmd: TADOCommand;
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
    bOK: Boolean;
    iCodigoCliente: Integer;
  public
    { Public declarations }
    function fInit_Form_Habitada(iCodCliente: Integer): Boolean;
  end;

var
  fFluxoResetarConsumidor: TfFluxoResetarConsumidor;

implementation

uses uGeral;

{$R *.dfm}

function TfFluxoResetarConsumidor.fInit_Form_Habitada(iCodCliente:Integer): Boolean;
begin
  bOk := False;
  iCodigoCliente := iCodCliente;
  with setMod do
    begin
      Close;
      Parameters.ParamByName('fx_versao').Value := dmGeral.iFluxoVersao;
      Parameters.ParamByName('fx_tipo').Value := 'IN';
      Open;
    end;
  Self.ShowModal;
  Result := bOk;
  Self.Free;
end;


procedure TfFluxoResetarConsumidor.btnOKClick(Sender: TObject);
begin
  if (dmGeral.fMensagemYesNo('Voc� confirma a inclus�o do item selecionado?')) then
    begin
      inherited;
      try
        with cmd do
          begin
            cmd.Parameters.ParamByName('fc_cd_cliente').Value := iCodigoCliente;
            Parameters.ParamByName('fc_cd_fluxo').Value := setModCD_FLUXO.Value;
            Parameters.ParamByName('fc_cd_funcionario').Value := dmGeral.iCodUsuario;
            Parameters.ParamByName('fc_dt_limite').Value := Null;
            Parameters.ParamByName('fc_dt_inicio').Value := Null;
            Parameters.ParamByName('fc_dt_fim').Value := Null;
            Parameters.ParamByName('fc_situacao').Value := 'P';
            Parameters.ParamByName('fc_justificativa').Value := Null;
            Parameters.ParamByName('fc_observacao').Value := 'Atualizado para ver�o '+ IntToStr(dmGeral.iFluxoVersao) + ' do fluxo.';
            dmGeral.ADOConnection.BeginTrans;
            dmGeral.fExecComando('UPDATE dbo.fluxo_cliente SET fc_situacao = ''F'' WHERE fc_cd_cliente = '+
              IntToStr(iCodigoCliente) +' AND fc_situacao = ''P''');
            Execute;
            dmGeral.ADOConnection.CommitTrans;
          end; // with ...
      except
        dmGeral.ADOConnection.RollbackTrans;
      end;
        bOk := True;
    end;
end;

end.
