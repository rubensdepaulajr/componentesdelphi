unit ExtData;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, DsgnIntf;

type
  TMode = (moData);

  TextDATA = class(TComponent)
  private
    FAbout  : string;
    FInput  : string;
    FResult : string;
    FMode   : TMode;
    procedure SetInput(Value: string);
    procedure SetMode(Value: TMode);
    procedure Set_DATA(Value: string);
    procedure SetResult(Value: string);
    procedure ShowAbout;
  protected
  public
    constructor Create(AOwner:TComponent); override;
    destructor Destroy; override;
  published
    property About    : string  read FAbout     write FAbout      stored  False;
    property Input    : string  read FInput     write SetInput;
    property Mode     : TMode   read FMode      write SetMode;
    property Result   : string  read FResult    write SetResult;
  end;

procedure Register;

implementation

{#######################################################################}

type
  TAboutProperty = class(TPropertyEditor)
  public
    procedure Edit; override;
    function  GetAttributes: TPropertyAttributes; override;
    function  GetValue : string; override;
  end;

procedure TAboutProperty.Edit;
{Invoke the about dialog when clicking on ... in the Object Inspector}
begin
  TextDATA(GetComponent(0)).ShowAbout;
end;

function TAboutProperty.GetAttributes: TPropertyAttributes;
{Make settings for just displaying a string in the ABOUT property in the
Object Inspector}
begin
  GetAttributes := [paDialog, paReadOnly];
end;

function TAboutProperty.GetValue: String;
{Text in the Object Inspector for the ABOUT property}
begin
  GetValue := '(About)';
end;

procedure TextDATA.ShowAbout;
var
  msg: string;
const
  carriage_return = chr(13);
begin
  msg := 'Data por Extenso';
  AppendStr(msg, carriage_return);
  AppendStr(msg, 'Componente EXCLUSIVO');
  AppendStr(msg, carriage_return);
  AppendStr(msg, carriage_return);
  AppendStr(msg, 'Carlos Kretli Tesch');
  AppendStr(msg, carriage_return);
  AppendStr(msg, carriage_return);
//  AppendStr(msg, 'Johnny Luiz de Azevedo');
  ShowMessage(msg);
end;

{#######################################################################}

constructor TextDATA.Create( Aowner: Tcomponent);
begin
  inherited Create( Aowner );
  FInput  := '';
  FResult := '';
  FMode   := moData;
end;

destructor TextDATA.Destroy;
begin
  inherited Destroy;
end;

procedure TextDATA.SetMode(Value: TMode);
begin
  if FMode <> Value then
  begin
    FMode := Value;
    SetInput(FInput);
  end;
end;

procedure TextDATA.SetInput(Value: string);
begin
  FInput := Value;
  case FMode of
    moData: Set_DATA(Value);
  end;
end;

procedure TextDATA.Set_DATA(Value: string);
var
  localASPMES    : string;
  localResult    : string;
begin
  localASPMES := '';
  localResult := '';

    begin
    localASPMES := FInput;
    localResult := 'Vit�ria (ES), ';
    end;
    // Formato ??/?/?//????
    if localASPMES  = '' then
       localASPMES := DateToStr(date);

    if (copy(localASPMES,3,1)+copy(localASPMES,5,1)+copy(localASPMES,7,2)) = '////' then
       localResult := copy(localASPMES,1,2)+
                      copy(localASPMES,4,1)+
                      copy(localASPMES,6,1)+
                      copy(localASPMES,9,4);

     if copy(localASPMES,4,2) = '01' then
        localResult          := localResult+copy(localASPMES,1,2)+' de janeiro de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '02' then
        localResult          := localResult+copy(localASPMES,1,2)+' de fevereiro de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '03' then
        localResult          := localResult+copy(localASPMES,1,2)+' de marco de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '04' then
        localResult          := localResult+copy(localASPMES,1,2)+' de abril de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '05' then
        localResult          := localResult+copy(localASPMES,1,2)+' de maio de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '06' then
        localResult          := localResult+copy(localASPMES,1,2)+' de junho de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '07' then
        localResult          := localResult+copy(localASPMES,1,2)+' de julho de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '08' then
        localResult          := localResult+copy(localASPMES,1,2)+' de agosto de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '09' then
        localResult          := localResult+copy(localASPMES,1,2)+' de setembro de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '10' then
        localResult          := localResult+copy(localASPMES,1,2)+' de outubro de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '11' then
        localResult          := localResult+copy(localASPMES,1,2)+' de novembro de '+
                                            copy(localASPMES,7,4);
     if copy(localASPMES,4,2) = '12' then
        localResult          := localResult+copy(localASPMES,1,2)+' de dezembro de '+
                                            copy(localASPMES,7,4);
     FResult     := localResult;
end;


procedure TextDATA.SetResult(Value: string);
begin
  {do nothing  //  read only}
end;

procedure Register;
begin
  RegisterComponents('Samples', [TextDATA]);
  RegisterPropertyEditor(TypeInfo(String), TextDATA, 'About',
  	TAboutProperty);
end;

end.
