unit NumEdit;
interface
uses
  StdCtrls,Classes,SysUtils,Controls,Forms,Graphics, ExtCtrls;
type
  TRealLabelFormat = (fNumber ,fMoney, fFixed);
  TNumEdit = class(TLabeledEdit)
  private
    { Private declarations }
    ds,sinal : Boolean;
    procedure SetValue(valor: Double);
    function  GetValue: Double;
    procedure SetFormat(Value: TRealLabelFormat);
    procedure SetFSize(Value: String);
    procedure OnFim(Sender: TObject);
    procedure CMEnter(var Message: TCMEnter); message CM_ENTER;
    protected
      ftipo: TRealLabelFormat;
      Fsize: String;
    procedure KeyPress(var Key: Char); override;
    { Public declarations }
    public
      constructor Create( AOwner: TComponent ); override;
      property Value: Double read GetValue write SetValue;
      function TextSQL: string;
    published
      property Align;
      property EditLabel;
      property BorderStyle;
      property Color;
      property Ctl3D;
      property DragCursor;
      property DragMode;
      property Enabled;
      property Font;
      property HideSelection;
      property MaxLength;
      property ParentColor;
      property ParentCtl3D;
      property ParentFont;
      property ParentShowHint;
      property PopupMenu;
      property ReadOnly;
      property ShowHint;
      property TabOrder;
      property TabStop;
      property Visible;
      //property WantTabs;
      property FormatReal: TRealLabelFormat read FTipo write SetFormat;
      property FormatSize: String read FSize write SetFSize;
      property OnChange;
      property OnClick;
      property OnDblClick;
      property OnDragDrop;
      property OnDragOver;
      property OnEndDrag;
      property OnEnter;
      property OnExit;
      property OnKeyDown;
      property OnKeyPress;
      property OnKeyUp;
      property OnMouseDown;
      property OnMouseMove;
      property OnMouseUp;
      property OnStartDrag;
  end;

procedure Register;

implementation


constructor TNumEdit.Create( AOwner: TComponent );
begin
  inherited Create( AOwner );
  Height      := 21;
  Width		    := 121;
  Caption     := 'NumEdit';
  //Alignment	:= taRightJustify;
  Value		    := 0.0;
  FSize		    := '10.2';
  FormatReal	:= fNumber;
  OnExit     	:= OnFim;
  ds := False;
  Sinal := False;
end;

function TNumEdit.TextSQL: string;
begin
  if Trim(Self.Text) = '' then
    Result := '0'
  else
    Result := Trim(StringReplace(StringReplace(Self.Text,'.','',[rfReplaceAll]),',','.',[rfReplaceAll]));
end;

procedure TNumEdit.OnFim(Sender: TObject);
begin
  SetValue(GetValue);
end;

procedure TNumEdit.SetFormat(Value: TRealLabelFormat);
begin
  if Value <> FTipo then
  begin
    FTipo := Value;
    Invalidate;
  end;
end;

procedure TNumEdit.SetFSize(Value: String);
begin
  if Value <> FSize then
    FSize := Value;
end;

procedure TNumEdit.SetValue(Valor: Double);
var	ff : string;
begin
  if FTipo = fNumber then
    ff := '%'+ FSize + 'n';
  if FTipo = fMoney  then
    ff := '%'+ FSize + 'm';
  if FTipo = fFixed  then
    ff := '%'+ FSize + 'f';
  Self.Text := StringReplace(Trim(Format(ff,[Valor])),'.','',[rfReplaceAll]); // Adicionei Trim

end;

function  TNumEdit.GetValue: Double;
var aux : string;
begin
  aux := Trim(Self.Text);      //##
  if (aux = '-') or (aux = '') then
    aux := '0';//0.00     //TrimAll
  Result := StrToFloat(Trim(aux));
end;

procedure TNumEdit.CMEnter(var Message: TCMEnter);
begin
  SelectAll;
  inherited;
end;

procedure TNumEdit.KeyPress(var Key: Char);
var
  aux : String;
begin
  if key = ThousandSeparator then
    Key := DecimalSeparator;
  if Not (Key in ['0'..'9',DecimalSeparator,'-',chr(8)]) Then
    begin
      Key := #0;
      inherited KeyPress(Key);
      exit;
    end;
   aux := DecimalSeparator;
   if key = DecimalSeparator then
     if Pos(aux,Self.Text) <> 0 then
       Key := #0;
   if key = '-' then
     if Length(Self.Text) > 0 then
       if Self.SelLength = Length(Self.Text) then
	 key := '-'
       else
         Key := #0;
  inherited KeyPress(Key);
  
end;

procedure Register;
begin
  RegisterComponents('AllSoft', [TNumEdit]);
end;

end.
