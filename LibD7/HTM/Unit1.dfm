object Form1: TForm1
  Left = 233
  Top = 309
  Width = 512
  Height = 372
  Caption = 'TmTagProducer demo'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 64
    Width = 79
    Height = 13
    Caption = 'Code generated:'
  end
  object Button1: TButton
    Left = 8
    Top = 16
    Width = 121
    Height = 25
    Caption = 'Generate HTML-code'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Memo1: TMemo
    Left = 8
    Top = 80
    Width = 489
    Height = 257
    ScrollBars = ssVertical
    TabOrder = 1
    OnChange = Memo1Change
  end
  object Button2: TButton
    Left = 136
    Top = 16
    Width = 121
    Height = 25
    Caption = 'Save to file'
    Enabled = False
    TabOrder = 2
    OnClick = Button2Click
  end
  object mTagProducer1: TmTagProducer
    Font.Face = 'Arial'
    Font.Size = 2
    Font.Color = '#0000FF'
    Font.Content = '<FONT FACE="Arial" SIZE="+2" COLOR="#0000FF">'
    PushButton.Name = 'PushButton1'
    PushButton.Value = 'Value1'
    PushButton.Content = '<INPUT TYPE="BUTTON" NAME="PushButton1" VALUE="Value1">'
    CheckBox.Name = 'CheckBox1'
    CheckBox.Value = 'Value1'
    CheckBox.Checked = True
    CheckBox.Content = 
      '<INPUT TYPE="CHECKBOX" NAME="CheckBox1" VALUE="Value1" CHECKED="' +
      'CHECKED">'
    FileUpload.Name = 'FileUpload1'
    FileUpload.Content = '<INPUT TYPE="FILE" NAME="FileUpload1">'
    ImageButton.Name = 'ImageButton1'
    ImageButton.SRC = 'Image1'
    ImageButton.Content = '<INPUT TYPE="IMAGE" NAME="ImageButton1" SRC="Image1">'
    OptionButton.Name = 'OptionButton1'
    OptionButton.Value = 'Value1'
    OptionButton.Checked = False
    OptionButton.Content = 
      '<INPUT TYPE="RADIO" NAME="OptionButton1" VALUE="Value1" CHECKED=' +
      '"UNCHECKED">'
    ResetButton.Name = 'ResetButton1'
    ResetButton.Value = 'Value1'
    ResetButton.Content = '<INPUT TYPE="RESET" NAME="ResetButton1" VALUE="Value1">'
    SubmitButton.Name = 'SubmitButton1'
    SubmitButton.Value = 'Value1'
    SubmitButton.Content = '<INPUT TYPE="SUBMIT" NAME="SubmitButton1" VALUE="Value1">'
    TextBox.Name = 'TextBox1'
    TextBox.Value = 'Value1'
    TextBox.Size = 20
    TextBox.Content = '<INPUT TYPE="TEXT" NAME="TextBox1" VALUE="Value1" SIZE="20">'
    TextArea.Name = 'TextArea1'
    TextArea.Cols = 20
    TextArea.Rows = 5
    TextArea.Content = 
      '<TEXTAREA NAME="TextArea1" ROWS="5" COLS="20">This is a textarea' +
      '.</TEXTAREA>'
    DropDownList.Name = 'DropDownList1'
    DropDownList.OptionNames.Strings = (
      'Option1'
      'Option2'
      'Option3'
      'Option4'
      'Option5')
    DropDownList.Content = 
      '<SELECT NAME="DropDownList1" SIZE="1"><OPTION>Option1</OPTION><O' +
      'PTION>Option2</OPTION><OPTION>Option3</OPTION><OPTION>Option4</O' +
      'PTION><OPTION>Option5</OPTION></SELECT>'
    ListBox.Name = 'ListBox1'
    ListBox.OptionNames.Strings = (
      'Option1'
      'Option2'
      'Option3'
      'Option4')
    ListBox.Size = 5
    ListBox.Content = 
      '<SELECT NAME="ListBox1" SIZE="5"><OPTION>Option1</OPTION><OPTION' +
      '>Option2</OPTION><OPTION>Option3</OPTION><OPTION>Option4</OPTION' +
      '></SELECT>'
    Password.Name = 'Password1'
    Password.Size = 20
    Password.Content = '<INPUT TYPE="PASSWORD" NAME="Password1" VALUE="" SIZE="20">'
    HR.Color = '#FF0000'
    HR.Size = 3
    HR.Width = '80%'
    HR.Content = '<HR WIDTH="80%" SIZE="3" COLOR="#FF0000">'
    About = 
      'Version 3.1, 2001'#174' Mats Asplund, EMail: masprod@telia.com, Site:' +
      ' http://go.to/masdp'
    Left = 464
    Top = 8
  end
  object SaveDialog1: TSaveDialog
    DefaultExt = '*.htm'
    FileName = '*.htm'
    Filter = 'HTML-files|*.htm;*.html|All files|*.*'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofEnableSizing]
    Title = 'TmTagProducer demo'
    Left = 464
    Top = 40
  end
end
