�
 TFORM1 0o   TPF0TForm1Form1LeftcTop� BorderIconsbiSystemMenu
biMinimize BorderStylebsDialogCaptionMail2000 DemoClientHeight�ClientWidthsColor	clBtnFaceFont.CharsetDEFAULT_CHARSET
Font.ColorclWindowTextFont.Height�	Font.NameMS Sans Serif
Font.Style OldCreateOrderPositionpoDesktopCenterOnClose	FormCloseOnShowFormShowPixelsPerInch`
TextHeight TPageControlPageControl1LeftTopWidthQHeight�
ActivePage	TabSheet1TabOrder  	TTabSheet	TabSheet1CaptionPOP3 TLabelLabel1LeftTopWidthyHeightAutoSizeCaptionHostFocusControleHost  TLabelLabel2LeftTopDWidthyHeightAutoSizeCaptionUserNameFocusControl	eUsername  TLabelLabel3LeftToppWidthyHeightAutoSizeCaptionPasswordFocusControl	ePassword  TLabellStatusLeftTop� Width� Height	AlignmenttaCenterAutoSizeCaptionDisconnected  TLabelLabel4Left,TopWidthyHeightAutoSizeCaptionLast response  TLabelLabel25LeftTop\Width� HeightCaptionComments to: mpanda@bigfoot.com  TButtonbConnectLeftTop� WidthKHeightCaptionConnectTabOrderOnClickbConnectClick  TEditeHostLeftTop(WidthyHeightTabOrder   TEdit	eUsernameLeftTopTWidthyHeightTabOrder  TEdit	ePasswordLeftTop� WidthyHeightPasswordChar*TabOrder  TButtonbDisconnectLefthTop� WidthMHeightCaption
DisconnectEnabledTabOrderOnClickbDisconnectClick  TButton	bRetrieveLeftTop� WidthiHeightCaptionRetrieve msg #EnabledTabOrderOnClickbRetrieveClick  TEditeMsgNumLeft� Top� Width!HeightTabOrderText1  TUpDownUpDown1Left� Top� WidthHeight	AssociateeMsgNumMinPositionTabOrderWrap  TProgressBarProgressBar1LeftTop Width� HeightMin MaxdTabOrder  TProgressBarProgressBar2LeftTop8Width� HeightMin MaxdTabOrder	  TListBox	mResponseLeft,Top(WidthHeight1
ItemHeightTabOrder
   	TTabSheet	TabSheet2CaptionMessage
ImageIndex TPageControlPageControl2LeftTopWidth9Heighta
ActivePage	TabSheet4TabOrder  	TTabSheet	TabSheet4CaptionSource TLabelLabel19LeftTopWidthyHeightAutoSizeCaptionHeaderFocusControl	eFromName  TLabelLabel20Left�TopWidthyHeightAutoSizeCaptionLabelFocusControleLabel  TLabelLabel21Left�Top<WidthyHeightAutoSizeCaptionValueFocusControleValue  TLabelLabel22LeftTop� WidthyHeightAutoSizeCaption	Main bodyFocusControl	eFromName  TMemomHeaderLeftTop WidthqHeightiReadOnly	
ScrollBarsssBothTabOrder   TEditeLabelLeft�Top WidthxHeightTabOrder  TEditeValueLeft�TopLWidthxHeightTabOrder  TButtonbLabelLeft�ToppWidth9HeightCaptionSetTabOrderOnClickbLabelClick  TButton	bDelLabelLeft�ToppWidth9HeightCaptionDeleteTabOrderOnClickbDelLabelClick  TMemomBodyLeftTop� WidthqHeight� ReadOnly	
ScrollBarsssBothTabOrder  TButtonbRebuildLeft�Top� WidthyHeightCaptionRebuild bodyTabOrderOnClickbRebuildClick  TButtonbResetLeft�Top� WidthyHeightCaptionReset messageTabOrderOnClickbResetClick  TButtonbRefreshLeft�Top� WidthyHeightCaptionRefresh screenTabOrderOnClickbRefreshClick  TButtonbSaveMsgLeft�Top� WidthyHeightCaptionSave messageTabOrder	OnClickbSaveMsgClick  TButtonbLoadMsgLeft�TopWidthyHeightCaptionLoad messageTabOrder
OnClickbLoadMsgClick   	TTabSheet	TabSheet5CaptionHeader
ImageIndex TLabelLabel7LeftTopWidthyHeightAutoSizeCaption	From nameFocusControl	eFromName  TLabelLabel8LeftTop@WidthyHeightAutoSizeCaptionFrom addressFocusControleFromAddress  TLabelLabel10Left� TopWidthyHeightAutoSizeCaptionReply-to nameFocusControl
eReplyName  TLabelLabel11Left� Top@WidthyHeightAutoSizeCaptionReply-to addressFocusControleReplyAddress  TLabelLabel12LeftToplWidthyHeightAutoSizeCaptionSubjectFocusControleSubject  TLabelLabel13Left� ToplWidthaHeightAutoSizeCaptionDateFocusControldtDate  TLabelLabel14LeftpToplWidthaHeightAutoSizeCaptionTimeFocusControldtTime  TLabelLabel15LeftTop� WidthyHeightAutoSizeCaptionTo namesFocusControleSubject  TLabelLabel16Left� Top� WidthyHeightAutoSizeCaptionTo addressesFocusControleSubject  TLabelLabel17LeftTop� WidthyHeightAutoSizeCaptionCc namesFocusControleSubject  TLabelLabel18Left� Top� WidthyHeightAutoSizeCaptionCc addressesFocusControleSubject  TEdit	eFromNameLeftTop$Width� HeightTabOrder   TEditeFromAddressLeftTopPWidth� HeightTabOrder  TEdit
eReplyNameLeft� Top$Width� HeightTabOrder  TEditeReplyAddressLeft� TopPWidth� HeightTabOrder  TEditeSubjectLeftTop|Width� HeightTabOrder  TDateTimePickerdtDateLeft� Top|WidtheHeightCalAlignmentdtaLeftDate H���U��@Time H���U��@
DateFormatdfShortDateMode
dmComboBoxKinddtkDate
ParseInputTabOrder  TDateTimePickerdtTimeLeftpTop|WidtheHeightCalAlignmentdtaLeftDate H���U��@Time H���U��@
DateFormatdfShortDateMode
dmComboBoxKinddtkTime
ParseInputTabOrder  TMemomToNamesLeftTop� Width� Height5
ScrollBarsssBothTabOrder  TMemomToAddressesLeft� Top� Width� Height5
ScrollBarsssBothTabOrder  TMemomCcNamesLeftTop� Width� Height5
ScrollBarsssBothTabOrder	  TMemomCcAddressesLeft� Top� Width� Height5
ScrollBarsssBothTabOrder
  TButtonbSaveHeaderLeft�Top$Width=HeightCaptionSaveTabOrderOnClickbSaveHeaderClick   	TTabSheet	TabSheet6CaptionBody
ImageIndex TLabelLabel6LeftTopWidthyHeightAutoSizeCaptionFilesFocusControllbFiles  TLabelLabel5LeftTop� WidthyHeightAutoSizeCaption	Text/html  TLabelLabel9LeftToplWidthyHeightAutoSizeCaption
Text/plain  TMemo	mTextHtmlLeftTop� Width�HeightU
ScrollBarsssBothTabOrder  TListBoxlbFilesLeftTop Width�HeightA
ItemHeightTabOrder   TButtonbSaveLeft�Top WidthKHeightCaptionSaveTabOrderOnClick
bSaveClick  TButtonbAttachLeft�Top4WidthKHeightCaptionAttachTabOrderOnClickbAttachClick  TButtonbHtmlLeft�Top� WidthKHeightCaption
Put in msgTabOrderOnClick
bHtmlClick  TMemo
mTextPlainLeftTop|Width�HeightI
ScrollBarsssBothTabOrder  TButtonbPlainLeft�Top|WidthKHeightCaption
Put in msgTabOrderOnClickbPlainClick  TButtonbRemoveLeft�TopHWidthKHeightCaptionRemoveTabOrderOnClickbRemoveClick  TButtonbPlainRemoveLeft�Top� WidthKHeightCaptionRemoveTabOrderOnClickbPlainRemoveClick  TButtonbHtmlRemoveLeft�Top� WidthKHeightCaptionRemoveTabOrder	OnClickbHtmlRemoveClick     	TTabSheet	TabSheet3CaptionSMTP
ImageIndex TLabelLabel23LeftTopWidthyHeightAutoSizeCaptionHostFocusControl	eSmtpHost  TLabelLabel24Left,TopWidthyHeightAutoSizeCaptionLast response  TEdit	eSmtpHostLeftTop(WidthyHeightTabOrder   TButtonbSmtpConnectLeftTopTWidthQHeightCaptionConnectTabOrderOnClickbSmtpConnectClick  TButtonbSmtpDisconnectLeftTop� WidthQHeightCaption
DisconnectEnabledTabOrderOnClickbSmtpDisconnectClick  TButtonbSendLeftToptWidthQHeightCaptionSendEnabledTabOrderOnClick
bSendClick  TProgressBarProgressBar3LeftTop Width� HeightMin MaxdTabOrder  TListBoxmSmtpResponseLeft,Top(WidthHeight1
ItemHeightTabOrder    TOpenDialog
OpenDialogFilter&All files (*.*)|*.*|Mail (*.eml)|*.emlLeft�  TPOP2000POPPort�	ProxyPort
ProxyUsageProxyString%h% %p%MailMessageMsgTimeOut<
OnProgressPOPProgressLeft  	TSMTP2000SMTPPort	ProxyPort
ProxyUsageProxyString%h% %p%TimeOut<
PacketSize 
OnProgressSMTPProgressLeft0  TSaveDialog
SaveDialogLeft�  TMailMessage2000MsgCharset
iso-8859-1LeftP   