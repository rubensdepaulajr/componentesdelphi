{
----------------------------------------------------------
MAS-CompMaker was used to generate this code
MAS-CompMaker, 2000-2001� Mats Asplund
----------------------------------------------------------

Component Name: TmTagProducer
        Author: Mats Asplund
      Creation: 2001-07-22
       Version: 3.2, 2001-10-18
   Description: A component that generates HTML-Formtags. To be used in a WebModule
                or with WebDispatcher. This component produces tags that
                could easily be added to the Dispatchers ResponseContent.
        Credit:
        E-mail: masprod@telia.com
          Site: http://go.to/masdp
  Legal issues: All rights reserved 2001� by Mats Asplund


         Usage: This software is provided 'as-is', without any express or
                implied warranty.  In no event will the author be held liable
                for any  damages arising from the use of this software.

                Permission is granted to anyone to use this software for any
                purpose, including commercial applications, and to alter it
                and redistribute it freely, subject to the following
                restrictions:

                1. The origin of this software must not be misrepresented,
                   you must not claim that you wrote the original software.
                   If you use this software in a product, an acknowledgment
                   in the product documentation would be appreciated but is
                   not required.

                2. Altered source versions must be plainly marked as such, and
                   must not be misrepresented as being the original software.

                3. This notice may not be removed or altered from any source
                   distribution.

                4. If you decide to use this software in any of your applications.
                   Send me an EMail and tell me about it.


                   KeyMethods:

                   AddToMasterContent

--------------------------------------------------------------------------------
}

unit mtagproducer;

interface

uses
  SysUtils, WinTypes, WinProcs, Messages, Classes, Graphics, Controls,
  Forms, Dialogs, StdCtrls, ExtCtrls, Windows;

type
  TmhtSize = 1..1000;

  TmhtFontSize = -5..5;

  TmhtMethod = (mhtANY, mhtGET, mhtPOST, mhtHEAD, mhtPUT);

  TmhtForm = class(TPersistent)
  private
    fFoAction: TFileName;
    fFoMethod: TmhtMethod;
    fFoContent: string;
    procedure ChangeContent;
    procedure SetFoAction(const Value: TFileName);
    procedure SetFoMethod(const Value: TmhtMethod);
    procedure SetFoContent(const Value: string);
  published
    property Action: TFileName read fFoAction write SetFoAction;
    property Method: TmhtMethod read fFoMethod write SetFoMethod;
    property Content: string read fFoContent write SetFoContent;
  end;

  TmhtFont = class(TPersistent)
  private
    fFColor: string;
    fFSize: TmhtFontSize;
    fFContent: string;
    fFFace: string;
    procedure SetFColor(const Value: string);
    procedure SetFContent(const Value: string);
    procedure SetFFace(const Value: string);
    procedure SetFSize(const Value: TmhtFontSize);
    procedure ChangeContent;
  published
    property Face: string read fFFace write SetFFace;
    property Size: TmhtFontSize read fFSize write SetFSize;
    property Color: string read fFColor write SetFColor;
    property Content: string read fFContent write SetFContent;
  end;

  TmhtPushButton = class(TPersistent)
  private
    fPBValue: string;
    fPBName: string;
    fPBContent: string;
    procedure SetPBContent(const Value: string);
    procedure SetPBName(const Value: string);
    procedure SetPBValue(const Value: string);
    procedure ChangeContent;
  published
    property Name: string read fPBName write SetPBName;
    property Value: string read fPBValue write SetPBValue;
    property Content: string read fPBContent write SetPBContent;
  end;

  TmhtCheckBox = class(TPersistent)
  private
    fCBChecked: Boolean;
    fCBValue: string;
    fCBName: string;
    fCBContent: string;
    procedure ChangeContent;
    procedure SetCBChecked(const Value: Boolean);
    procedure SetCBName(const Value: string);
    procedure SetCBValue(const Value: string);
    procedure SetCBContent(const Value: string);
  published
    property Name: string read fCBName write SetCBName;
    property Value: string read fCBValue write SetCBValue;
    property Checked: Boolean read fCBChecked write SetCBChecked;
    property Content: string read fCBContent write SetCBContent;
  end;

  TmhtFileUpload = class(TPersistent)
  private
    fFUName: string;
    fFUContent: string;
    procedure ChangeContent;
    procedure SetFUName(const Value: string);
    procedure SetFUContent(const Value: string);
  published
    property Name: string read fFUName write SetFUName;
    property Content: string read fFUContent write SetFUContent;
  end;

  TmhtImageButton = class(TPersistent)
  private
    fIBSRC: string;
    fIBName: string;
    fIBContent: string;
    procedure ChangeContent;
    procedure SetIBContent(const Value: string);
    procedure SetIBName(const Value: string);
    procedure SetIBSRC(const Value: string);
  published
    property Name: string read fIBName write SetIBName;
    property SRC: string read fIBSRC write SetIBSRC;
    property Content: string read fIBContent write SetIBContent;
  end;

  TmhtOptionButton = class(TPersistent)
  private
    fOBChecked: Boolean;
    fOBValue: string;
    fOBName: string;
    fOBContent: string;
    procedure ChangeContent;
    procedure SetOBChecked(const Value: Boolean);
    procedure SetOBContent(const Value: string);
    procedure SetOBName(const Value: string);
    procedure SetOBValue(const Value: string);
  published
    property Name: string read fOBName write SetOBName;
    property Value: string read fOBValue write SetOBValue;
    property Checked: Boolean read fOBChecked write SetOBChecked;
    property Content: string read fOBContent write SetOBContent;
  end;

  TmhtResetButton = class(TPersistent)
  private
    fRBValue: string;
    fRBName: string;
    fRBContent: string;
    procedure ChangeContent;
    procedure SetRBContent(const Value: string);
    procedure SetRBName(const Value: string);
    procedure SetRBValue(const Value: string);
  published
    property Name: string read fRBName write SetRBName;
    property Value: string read fRBValue write SetRBValue;
    property Content: string read fRBContent write SetRBContent;
  end;

  TmhtSubmitButton = class(TPersistent)
  private
    fSBValue: string;
    fSBName: string;
    fSBContent: string;
    procedure ChangeContent;
    procedure SetSBContent(const Value: string);
    procedure SetSBName(const Value: string);
    procedure SetSBValue(const Value: string);
  published
    property Name: string read fSBName write SetSBName;
    property Value: string read fSBValue write SetSBValue;
    property Content: string read fSBContent write SetSBContent;
  end;

  TmhtTextBox = class(TPersistent)
  private
    fTBValue: string;
    fTBSize: TmhtSize;
    fTBName: string;
    fTBContent: string;
    procedure ChangeContent;
    procedure SetTBContent(const Value: string);
    procedure SetTBName(const Value: string);
    procedure SetTBSize(const Value: TmhtSize);
    procedure SetTBValue(const Value: string);
  published
    property Name: string read fTBName write SetTBName;
    property Value: string read fTBValue write SetTBValue;
    property Size: TmhtSize read fTBSize write SetTBSize;
    property Content: string read fTBContent write SetTBContent;
  end;

  TmhtTextArea = class(TPersistent)
  private
    fTARows: Integer;
    fTACols: Integer;
    fTAName: string;
    fTAContent: string;
    procedure ChangeContent;
    procedure SetTACols(const Value: Integer);
    procedure SetTAContent(const Value: string);
    procedure SetTAName(const Value: string);
    procedure SetTARows(const Value: Integer);
  published
    property Name: string read fTAName write SetTAName;
    property Cols: Integer read fTACols write SetTACols;
    property Rows: Integer read fTARows write SetTARows;
    property Content: string read fTAContent write SetTAContent;
  end;

  TmhtDropDownList = class(TPersistent)
  private
    fDDLOptionNames: TStrings;
    fDDLName: string;
    fDDLContent: string;
    fDDLOptionsCount: Integer;
    procedure SetDDLOptionNames(const Value: TStrings);
    procedure ChangeContent;
    procedure SetDDLContent(const Value: string);
    procedure SetDDLName(const Value: string);
  published
    property Name: string read fDDLName write SetDDLName;
    property OptionNames: TStrings read fDDLOptionNames write SetDDLOptionNames;
    property Content: string read fDDLContent write SetDDLContent;
  end;

  TmhtListBox = class(TPersistent)
  private
    fLBSize: TmhtSize;
    fLBOptionNames: TStrings;
    fLBName: string;
    fLBContent: string;
    fLBOptionsCount: Integer;
    procedure SetLBOptionNames(const Value: TStrings);
    procedure SetLBSize(const Value: TmhtSize);
    procedure ChangeContent;
    procedure SetLBContent(const Value: string);
    procedure SetLBName(const Value: string);
  published
    property Name: string read fLBName write SetLBName;
    property OptionNames: TStrings read fLBOptionNames write SetLBOptionNames;
    property Size: TmhtSize read fLBSize write SetLBSize;
    property Content: string read fLBContent write SetLBContent;
  end;

  TmhtPassWord = class(TPersistent)
  private
    fPWSize: TmhtSize;
    fPWValue: string;
    fPWName: string;
    fPWContent: string;
    procedure ChangeContent;
    procedure SetPWContent(const Value: string);
    procedure SetPWName(const Value: string);
    procedure SetPWSize(const Value: TmhtSize);
    procedure SetPWValue(const Value: string);
  published
    property Name: string read fPWName write SetPWName;
    property Value: string read fPWValue write SetPWValue;
    property Size: TmhtSize read fPWSize write SetPWSize;
    property Content: string read fPWContent write SetPWContent;
  end;

  TmhtHR = class(TPersistent)
  private
    fHRSize: TmhtSize;
    fHRColor: string;
    fHRWidth: string;
    fHRContent: string;
    procedure ChangeContent;
    procedure SetHRColor(const Value: string);
    procedure SetHRContent(const Value: string);
    procedure SetHRSize(const Value: TmhtSize);
    procedure SetHRWidth(const Value: string);
  published
    property Color: string read fHRColor write SetHRColor;
    property Size: TmhtSize read fHRSize write SetHRSize;
    property Width: string read fHRWidth write SetHRWidth;
    property Content: string read fHRContent write SetHRContent;
  end;

  TmTagProducer = class(TComponent)
  private
    fAbout: string;
    fPushButton: TmhtPushButton;
    fCheckBox: TmhtCheckBox;
    fFileUpload: TmhtFileUpload;
    fImageButton: TmhtImageButton;
    fOptionButton: TmhtOptionButton;
    fResetButton: TmhtResetButton;
    fSubmitButton: TmhtSubmitButton;
    fTextBox: TmhtTextBox;
    fTextArea: TmhtTextArea;
    fDropDownList: TmhtDropDownList;
    fListBox: TmhtListBox;
    FPassWord: TmhtPassWord;
    fHR: TmhtHR;
    fFont: TmhtFont;
    fMasterContent: TStrings;
    fForm: TmhtForm;
    procedure SetAbout(Value: string);
    procedure SetMasterContent(const Value: TStrings);
  { Private declarations }
  protected
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AddToMasterContent(const Value: string);
  { Private declarations }
  published
    property MasterContent: TStrings read fMasterContent write SetMasterContent;
    property Form: TmhtForm read fForm write fForm;
    property Font: TmhtFont read fFont write fFont;
    property PushButton: TmhtPushButton read fPushButton write fPushButton;
    property CheckBox: TmhtCheckBox read fCheckBox write fCheckBox;
    property FileUpload: TmhtFileUpload read fFileUpload write fFileUpload;
    property ImageButton: TmhtImageButton read fImageButton write fImageButton;
    property OptionButton: TmhtOptionButton read fOptionButton write fOptionButton;
    property ResetButton: TmhtResetButton read fResetButton write fResetButton;
    property SubmitButton: TmhtSubmitButton read fSubmitButton write fSubmitButton;
    property TextBox: TmhtTextBox read fTextBox write fTextBox;
    property TextArea: TmhtTextArea read fTextArea write fTextArea;
    property DropDownList: TmhtDropDownList read fDropDownList write fDropDownList;
    property ListBox: TmhtListBox read fListBox write fListBox;
    property Password: TmhtPassWord read FPassWord write fPassWord;
    property HR: TmhtHR read fHR write fHR;
    property About: string read fAbout write SetAbout;
  { Published declarations }
  end;


procedure Register;

implementation

procedure TmTagProducer.AddToMasterContent(const Value: string);
begin
  fMasterContent.Add(Value);
end;

constructor TmTagProducer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  fMasterContent:= TStringList.Create;
  fForm:= TmhtForm.Create;
  fForm.fFoAction:= '/cgi-bin/default.cgi';
  fForm.fFoMethod:= mhtPOST;
  fForm.fFoContent:= '<FORM ACTION="cgi-bin/default.cgi" METHOD="POST">';
  fFont:= TmhtFont.Create;
  fFont.Color:= '#FF0000';
  fFont.Size:= -1;
  fFont.Face:= 'Arial';
  fPushButton:= TmhtPushButton.Create;
  fPushButton.Name:= 'PushButton1';
  fPushButton.Value:= 'Value1';
  fCheckBox:= TmhtCheckBox.Create;
  fCheckBox.Name:= 'CheckBox1';
  fCheckBox.Value:= 'Value1';
  fFileUpload:= TmhtFileUpload.Create;
  fFileUpload.Name:= 'FileUpload1';
  fImageButton:= TmhtImageButton.Create;
  fImageButton.Name:= 'ImageButton1';
  fImageButton.SRC:= 'Image1';
  fOptionButton:= TmhtOptionButton.Create;
  fOptionButton.Name:= 'OptionButton1';
  fOptionButton.Value:= 'Value1';
  fResetButton:= TmhtResetButton.Create;
  fResetButton.Name:= 'ResetButton1';
  fResetButton.Value:= 'Value1';
  fSubmitButton:= TmhtSubmitButton.Create;
  fSubmitButton.Name:= 'SubmitButton1';
  fSubmitButton.Value:= 'Value1';
  fTextBox:= TmhtTextBox.Create;
  fTextBox.Name:= 'TextBox1';
  fTextBox.Value:= 'Value1';
  fTextBox.Size:= 3;
  fTextArea:= TmhtTextArea.Create;
  fTextArea.Name:= 'TextArea1';
  fTextArea.Cols:= 20;
  fTextArea.Rows:= 5;
  fDropDownList:= TmhtDropDownList.Create;
  fDropDownList.Name:= 'DropDownList1';
  fDropDownList.fDDLOptionNames:= TStringList.Create;
//  fDropDownList.fDDLOptionNames.Add('Option1');
  fListBox:= TmhtListBox.Create;
  fListBox.Name:= 'ListBox1';
  fListBox.fLBOptionNames:= TStringList.Create;
//  TmhtOptionList(fListBox.fLBOptionNames).ListBox:=fListBox;
  fListBox.fLBOptionNames.Add('Option1');
  fListBox.Size:= 20;
  fListBox.ChangeContent;
  FPassWord:= TmhtPassWord.Create;
  FPassWord.Name:= 'Password1';
  FPassWord.Size:= 20;
  fHR:= TmhtHR.Create;
  fHR.Color:= '#FF0000';
  fHR.Size:= 3;
  fHR.fHRWidth:= '100%';

  fAbout:= 'Version 3.1, 2001� Mats Asplund, EMail: masprod@telia.com, Site: http://go.to/masdp';
end;

destructor TmTagProducer.Destroy;
begin
  inherited Destroy;
end;

procedure TmTagProducer.SetAbout(Value: string);
begin
  Exit;
end;

procedure Register;
begin
  RegisterComponents('Test', [TmTagProducer]);
end;

{ TmhtListBox }

procedure TmhtListBox.ChangeContent;
var
  OptionStr: string;
  n: integer;
begin
  OptionStr:= '';
  for n:= 0 to fLBOptionsCount - 1 do
    OptionStr:= OptionStr + '<OPTION>' + fLBOptionNames[n] +
      '</OPTION>';
  fLBContent:= '<SELECT NAME="' + fLBName + '" SIZE="' +
    IntToStr(fLBSize) + '">' + OptionStr + '</SELECT>'
end;

procedure TmhtListBox.SetLBContent(const Value: string);
begin
  fLBContent:= Value;
end;

procedure TmhtListBox.SetLBName(const Value: string);
begin
  fLBName:= Value;
  ChangeContent;
end;

procedure TmhtListBox.SetLBOptionNames(const Value: TStrings);
begin
  fLBOptionNames.Assign(Value);
  fLBOptionsCount:= fLBOptionNames.Count;
  ChangeContent;
end;

procedure TmhtListBox.SetLBSize(const Value: TmhtSize);
begin
  if Value < 2 then
    fLBSize:= 2
  else
    fLBSize:= Value;
  ChangeContent;
end;

{ TmhtDropDownList }

procedure TmhtDropDownList.ChangeContent;
var
  OptionStr: string;
  n: Integer;
begin
  OptionStr:= '';
  for n:= 0 to fDDLOptionsCount - 1 do
    OptionStr:= OptionStr + '<OPTION>' + fDDLOptionNames[n] +
      '</OPTION>';
  fDDLContent:= '<SELECT NAME="' + fDDLName + '" SIZE="1">' +
    OptionStr + '</SELECT>';
end;

procedure TmhtDropDownList.SetDDLContent(const Value: string);
begin
  fDDLContent:= Value;
end;

procedure TmhtDropDownList.SetDDLName(const Value: string);
begin
  fDDLName:= Value;
  ChangeContent;
end;

procedure TmhtDropDownList.SetDDLOptionNames(const Value: TStrings);
begin
  fDDLOptionNames.Assign(Value);
  fDDLOptionsCount:= fDDLOptionNames.Count;
  ChangeContent;
end;

{ TmhtPushButton }

procedure TmhtPushButton.ChangeContent;
begin
  fPBContent:= '<INPUT TYPE="BUTTON" NAME="' + fPBName +
    '" VALUE="' + fPBValue + '">';
end;

procedure TmhtPushButton.SetPBContent(const Value: string);
begin
  fPBContent:= Value;
  ChangeContent;
end;

procedure TmhtPushButton.SetPBName(const Value: string);
begin
  fPBName:= Value;
  ChangeContent;
end;

procedure TmhtPushButton.SetPBValue(const Value: string);
begin
  fPBValue:= Value;
  ChangeContent;
end;

{ TmhtCheckBox }

procedure TmhtCheckBox.ChangeContent;
begin
  fCBContent:= '<INPUT TYPE="CHECKBOX" NAME="' + fCBName +
    '" VALUE="' + fCBValue;
  if fCBChecked then
    fCBContent:= fCBContent + '" CHECKED="CHECKED">'
  else
    fCBContent:= fCBContent + '">';
end;

procedure TmhtCheckBox.SetCBChecked(const Value: Boolean);
begin
  fCBChecked:= Value;
  ChangeContent;
end;

procedure TmhtCheckBox.SetCBContent(const Value: string);
begin
  fCBContent:= Value;
end;

procedure TmhtCheckBox.SetCBName(const Value: string);
begin
  fCBName:= Value;
  ChangeContent;
end;

procedure TmhtCheckBox.SetCBValue(const Value: string);
begin
  fCBValue:= Value;
  ChangeContent;
end;

{ TmhtFileUpload }

procedure TmhtFileUpload.ChangeContent;
begin
  fFUContent:= '<INPUT TYPE="FILE" NAME="' + fFUName + '">';
end;

procedure TmhtFileUpload.SetFUContent(const Value: string);
begin
  fFUContent:= Value;
end;

procedure TmhtFileUpload.SetFUName(const Value: string);
begin
  fFUName:= Value;
  ChangeContent;
end;

{ TmhtImageButton }

procedure TmhtImageButton.ChangeContent;
begin
  fIBContent:= '<INPUT TYPE="IMAGE" NAME="' + fIBName + '" SRC="' +
    fIBSRC + '">';
end;

procedure TmhtImageButton.SetIBContent(const Value: string);
begin
  fIBContent:= Value;
end;

procedure TmhtImageButton.SetIBName(const Value: string);
begin
  fIBName:= Value;
  ChangeContent;
end;

procedure TmhtImageButton.SetIBSRC(const Value: string);
begin
  fIBSRC:= Value;
  ChangeContent;
end;

{ TmhtOptionButton }

procedure TmhtOptionButton.ChangeContent;
begin
  fOBContent:= '<INPUT TYPE="RADIO" NAME="' + fOBName +
    '" VALUE="' + fOBValue + '"';
  if fOBChecked then
    fOBContent:= fOBContent + ' CHECKED="CHECKED">'
  else
    fOBContent:= fOBContent + ' CHECKED="UNCHECKED">';
end;

procedure TmhtOptionButton.SetOBChecked(const Value: Boolean);
begin
  fOBChecked:= Value;
  ChangeContent;
end;

procedure TmhtOptionButton.SetOBContent(const Value: string);
begin
  fOBContent:= Value;
end;

procedure TmhtOptionButton.SetOBName(const Value: string);
begin
  fOBName:= Value;
  ChangeContent;
end;

procedure TmhtOptionButton.SetOBValue(const Value: string);
begin
  fOBValue:= Value;
  ChangeContent;
end;

{ TmhtResetButton }

procedure TmhtResetButton.ChangeContent;
begin
  fRBContent:= '<INPUT TYPE="RESET" NAME="' + fRBName +
    '" VALUE="' + fRBValue + '">';
end;

procedure TmhtResetButton.SetRBContent(const Value: string);
begin
  fRBContent:= Value;
end;

procedure TmhtResetButton.SetRBName(const Value: string);
begin
  fRBName:= Value;
  ChangeContent;
end;

procedure TmhtResetButton.SetRBValue(const Value: string);
begin
  fRBValue:= Value;
  ChangeContent;
end;

{ TmhtSubmitButton }

procedure TmhtSubmitButton.ChangeContent;
begin
  fSBContent:= '<INPUT TYPE="SUBMIT" NAME="' + fSBName +
    '" VALUE="' + fSBValue + '">';
end;

procedure TmhtSubmitButton.SetSBContent(const Value: string);
begin
  fSBContent:= Value;
end;

procedure TmhtSubmitButton.SetSBName(const Value: string);
begin
  fSBName:= Value;
  ChangeContent;
end;

procedure TmhtSubmitButton.SetSBValue(const Value: string);
begin
  fSBValue:= Value;
  ChangeContent;
end;

{ TmhtTextBox }

procedure TmhtTextBox.ChangeContent;
begin
  fTBContent:= '<INPUT TYPE="TEXT" NAME="' + fTBName +
    '" VALUE="' + fTBValue + '" SIZE="' + IntToStr(fTBSize) +
    '">';

end;

procedure TmhtTextBox.SetTBContent(const Value: string);
begin
  fTBContent:= Value;
end;

procedure TmhtTextBox.SetTBName(const Value: string);
begin
  fTBName:= Value;
  ChangeContent;
end;

procedure TmhtTextBox.SetTBSize(const Value: TmhtSize);
begin
  fTBSize:= Value;
  ChangeContent;
end;

procedure TmhtTextBox.SetTBValue(const Value: string);
begin
  fTBValue:= Value;
  ChangeContent;
end;

{ TmhtTextArea }

procedure TmhtTextArea.ChangeContent;
begin
  fTAContent:= '<TEXTAREA NAME="' + fTAName +
    '" ROWS="' + IntToStr(fTARows) + '" COLS="' +
    IntToStr(fTACols) + '"></TEXTAREA>';
end;

procedure TmhtTextArea.SetTACols(const Value: Integer);
begin
  fTACols:= Value;
  ChangeContent;
end;

procedure TmhtTextArea.SetTAContent(const Value: string);
begin
  fTAContent:= Value;
end;

procedure TmhtTextArea.SetTAName(const Value: string);
begin
  fTAName:= Value;
  ChangeContent;
end;

procedure TmhtTextArea.SetTARows(const Value: Integer);
begin
  fTARows:= Value;
  ChangeContent;
end;

{ TmhtPassWord }

procedure TmhtPassWord.ChangeContent;
begin
  fPWContent:= '<INPUT TYPE="PASSWORD" NAME="' + fPWName +
    '" VALUE="' + fPWValue + '" SIZE="' + IntToStr(fPWSize) +
    '">';
end;

procedure TmhtPassWord.SetPWContent(const Value: string);
begin
  fPWContent:= Value;
end;

procedure TmhtPassWord.SetPWName(const Value: string);
begin
  fPWName:= Value;
  ChangeContent;
end;

procedure TmhtPassWord.SetPWSize(const Value: TmhtSize);
begin
  fPWSize:= Value;
  ChangeContent;
end;

procedure TmhtPassWord.SetPWValue(const Value: string);
begin
  fPWValue:= Value;
  ChangeContent;
end;

{ TmhtHR }

procedure TmhtHR.ChangeContent;
begin
  fHRContent:= '<HR WIDTH="' + fHRWidth + '" SIZE="' + IntToStr(fHRSize) +
    '" COLOR="' + fHRColor + '">';
end;

procedure TmhtHR.SetHRColor(const Value: string);
begin
  fHRColor:= Value;
  ChangeContent;
end;

procedure TmhtHR.SetHRContent(const Value: string);
begin
  fHRContent:= Value;
end;

procedure TmhtHR.SetHRSize(const Value: TmhtSize);
begin
  fHRSize:= Value;
  ChangeContent;
end;

procedure TmhtHR.SetHRWidth(const Value: string);
begin
  fHRWidth:= Value;
  ChangeContent;
end;

{ TmhtFont }

procedure TmhtFont.ChangeContent;
var
  Sign: string;
begin
  if fFSize > 0 then Sign:= '+' else Sign:= '';
  fFContent:= '<FONT FACE="' + fFFace + '" SIZE="' + Sign + IntToStr(fFSize) +
    '" COLOR="' + fFColor + '">';
end;

procedure TmhtFont.SetFColor(const Value: string);
begin
  fFColor := Value;
  ChangeContent;
end;

procedure TmhtFont.SetFContent(const Value: string);
begin
  fFContent := Value;
end;

procedure TmhtFont.SetFFace(const Value: string);
begin
  fFFace := Value;
  ChangeContent;
end;

procedure TmhtFont.SetFSize(const Value: TmhtFontSize);
begin
  fFSize := Value;
  ChangeContent;
end;

procedure TmTagProducer.SetMasterContent(const Value: TStrings);
begin
  fMasterContent.Assign(Value);
end;

{ TmhtForm }

procedure TmhtForm.ChangeContent;
var Method: string;
begin
  case fFoMethod of
    mhtANY: Method:= 'ANY';
    mhtGET: Method:= 'GET';
    mhtPOST: Method:= 'POST';
    mhtHEAD: Method:= 'HEAD';
    mhtPUT: Method:= 'PUT';
  end;
  fFoContent:= '<FORM ACTION="' + fFoAction + '" METHOD="' + Method + '">';
end;

procedure TmhtForm.SetFoAction(const Value: TFileName);
begin
  fFoAction := Value;
  ChangeContent;
end;

procedure TmhtForm.SetFoContent(const Value: string);
begin
  fFoContent:= Value;
end;

procedure TmhtForm.SetFoMethod(const Value: TmhtMethod);
begin
  fFoMethod := Value;
  ChangeContent;
end;

end.

