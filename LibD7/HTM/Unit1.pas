unit Unit1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, mtagproducer;

type
  TForm1 = class(TForm)
    Button1: TButton;
    mTagProducer1: TmTagProducer;
    Memo1: TMemo;
    Label1: TLabel;
    Button2: TButton;
    SaveDialog1: TSaveDialog;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Memo1Change(Sender: TObject);
  private
    HTML: TStringList;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click(Sender: TObject);
begin
  HTML:= TStringList.Create;
  HTML.Add('<HTML><TITLE></TITLE><HEAD></HEAD><BODY>');
  HTML.Add(mTagProducer1.Font.Content + '<B>TmTagProducer demo<BR><BR>');
  mTagProducer1.Font.Size:= -1;
  HTML.Add(mTagProducer1.Font.Content);
  HTML.Add('PushButton :' + mTagProducer1.PushButton.Content + '<BR>');
  HTML.Add('CheckBox :' + mTagProducer1.CheckBox.Content + '<BR>');
  HTML.Add('FileUpload :' + mTagProducer1.FileUpload.Content + '<BR>');
  HTML.Add('ImageButton :' + mTagProducer1.ImageButton.Content + '<BR>');
  HTML.Add('OptionButton :' + mTagProducer1.OptionButton.Content + '<BR>');
  HTML.Add('ResetButton :' + mTagProducer1.ResetButton.Content + '<BR>');
  HTML.Add('SubmitButton :' + mTagProducer1.SubmitButton.Content + '<BR>');
  HTML.Add('TextBox :' + mTagProducer1.TextBox.Content + '<BR>');
  HTML.Add('TextArea :' + mTagProducer1.TextArea.Content + '<BR>');
  HTML.Add('DropDownList :' + mTagProducer1.DropDownList.Content + '<BR>');
  HTML.Add('ListBox :' + mTagProducer1.ListBox.Content + '<BR>');
  HTML.Add('Password :' + mTagProducer1.Password.Content + '<BR>');
  HTML.Add('HR :' + mTagProducer1.HR.Content + '<BR>');
  HTML.Add('</BODY></HTML>');
  Memo1.Lines.Text:= HTML.Text;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  if SaveDialog1.Execute then
    HTML.SaveToFile(SaveDialog1.FileName);
end;

procedure TForm1.Memo1Change(Sender: TObject);
begin
  Button2.Enabled:= true;
end;

end.
