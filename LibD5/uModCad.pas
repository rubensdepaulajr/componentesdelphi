{*******************************************************************************
* Unit Name : uModCad                                     Mecatron Inform�tica *
* Author    : Rubens Jr                                     Date  : 29.09.2002 *
* Purpose   : Cadastro Modelo                                                  *
*******************************************************************************}
unit uModCad;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Grids, DBGrids, ImgList, ComCtrls, StdCtrls, Buttons, ExtCtrls, DBCtrls,
  ToolWin, Db, ADODb, Menus, TREdit, Spin, Mask, cktUF, Variants;

type
  TString4  = string[4];

  TfModCad = class(TForm)
    dsMod: TDataSource;
    setMod: TADODataSet;
    StatusBar: TStatusBar;
    pnlClient: TPanel;
    CoolBar: TCoolBar;
    ToolBar: TToolBar;
    btnNew: TToolButton;
    btnEdit: TToolButton;
    ToolButton3: TToolButton;
    btnSave: TToolButton;
    ToolButton6: TToolButton;
    btnDel: TToolButton;
    ToolButton8: TToolButton;
    btnHelp: TToolButton;
    ToolButton10: TToolButton;
    btnClose: TToolButton;
    btnPrint: TToolButton;
    ToolButton1: TToolButton;
    btnSearch: TToolButton;
    btnCancel: TToolButton;
    pnlSearch: TPanel;
    Panel1: TPanel;
    btnOK: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnCancelClick(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnDelClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnNewClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure SpeedButton1Click(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure bntCancelClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure btnOKClick(Sender: TObject);
  private
    { Private declarations }
    procedure pShowHint(Sender: TObject);
  protected
    { Protected declarations }
    // Indica que o Form esta no modo de inser��o
    bInsert: Boolean;
    // Recebe o componente que ganhar� foco no momento da Inclus�o/Edi��o
    oFocusControl: TWinControl;

    procedure pLockControls(bBrowse: Boolean); virtual;
    procedure pDecimalKeyPress(Sender: TObject; var Key: Char); 
  public
    { Public declarations }
    procedure pInit_Form;
  end;

var
  fModCad: TfModCad;

implementation

uses uGeral;

{$R *.DFM}

procedure TfModCad.pShowHint(Sender: TObject);
begin
  if Length(Application.Hint) > 0 then
    begin
      StatusBar.SimplePanel := True;
      StatusBar.SimpleText := Application.Hint;
    end
  else
    StatusBar.SimplePanel := False;
  //   StatusBar.Panels[0].Text:= Application.Hint;
end;


procedure TfModCad.pDecimalKeyPress(Sender: TObject; var Key: Char);
begin
  if (Key in ['.',',']) and (Pos(',',TEdit(Sender).Text) = 0) then
    Key := DecimalSeparator
  else
  if (Key in ['.',',','+','-']) or not (Key in ['0'..'9',#8]) then
    Key := #0;
end;

procedure TfModCad.FormCreate(Sender: TObject);
begin
  // Verifica se o usu�rio tem Controle Total no From
  //dmGeral.bControleTotal := dmGeral.fGetAcesso(-1);

  // Determina que a Aplica��o que no momento que for disparado o OnHint da
  // aplica��o o mesmo usar� a fun��o pShowHint e exibir na barra de Status o
  // Hint do componente.
  Application.OnHint := pShowHint;

  if dsMod.Dataset = nil then
    begin
      ShowMessage('Erro no Modelo: Voc� esqueceu de "setar" o DataSet do DataSource');
      PostMessage(Self.Handle, WM_Close, 0, 0);
      Exit;
    end;

  if pnlClient.ControlCount = 0 then
    begin
      ShowMessage('Erro na configura��o do Form: Voc� esqueceu de adicionar os controles de edi��o no Form Tab!');
      PostMessage(Self.Handle, WM_Close, 0, 0);
      Exit;
    end;
  //pLockControls(True);
  btnCancel.Enabled := False;
  btnSearch.Click;
end;

procedure TfModCad.pInit_Form;
begin
  ShowModal;
  Free;
end;

procedure TfModCad.pLockControls(bBrowse: Boolean);
var i: Integer;
begin                                                    
{  for i := 0 to ComponentCount -1 do
    if (Components[i] is TDBEdit) and not(TDBEdit(Components[i]).ReadOnly) then
        TDBEdit(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBMemo) and not(TDBMemo(Components[i]).ReadOnly) then
      TDBMemo(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBLookupComboBox) and not (TDBLookupComboBox(Components[i]).ReadOnly) then
      TDBLookupComboBox(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBComboBox) and not (TDBComboBox(Components[i]).ReadOnly) then
      TDBComboBox(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBRadioGroup) and not (TDBRadioGroup(Components[i]).ReadOnly)then
      TDBRadioGroup(Components[i]).Enabled := not bBrowse
    else
    if (Components[i] is TDBCheckBox) and not (TDBCheckBox(Components[i]).ReadOnly) then
      TDBCheckBox(Components[i]).Enabled := not  bBrowse;
}

  // Mudando a cor dos componentes que tem o preenchimento requerido
  for i := 0 to ComponentCount -1 do
    begin
      if (Components[i] is TDBEdit) and (Components[i].Tag = 1) then
        if bBrowse then
          TDBEdit(Components[i]).Color := clWindow
        else
          TDBEdit(Components[i]).Color := clYellow;
      if (Components[i] is TDBMemo) and (Components[i].Tag = 1) then
        if bBrowse then
          TDBMemo(Components[i]).Color := clWindow
        else
          TDBMemo(Components[i]).Color := clYellow;
      if (Components[i] is TDBLookupComboBox) and (Components[i].Tag = 1) then
        if bBrowse then
          TDBLookupComboBox(Components[i]).Color := clWindow
        else
          TDBLookupComboBox(Components[i]).Color := clYellow;
    end;

  if bBrowse  then
    begin
      pnlSearch.Visible := False;
      btnNew.Enabled    := True;
      btnEdit.Enabled   := not setMod.Eof;
      btnDel.Enabled    := btnEdit.Enabled;
      btnSave.Enabled   := False;
      btnCancel.Enabled := False;
      btnPrint.Enabled  := True;
      btnHelp.Enabled   := True;
    end
  else
    begin
      btnNew.Enabled    := False;
      btnEdit.Enabled   := False;
      btnSearch.Enabled := False;
      btnDel.Enabled    := False;
      btnSave.Enabled   := True;
      btnCancel.Enabled := True;
      btnPrint.Enabled  := False;
      btnHelp.Enabled   := False;
      if Assigned(oFocusControl) and oFocusControl.CanFocus then
        oFocusControl.SetFocus;
    end;
end;

// We want to destroy forms which are closed
procedure TfModCad.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfModCad.btnCancelClick(Sender: TObject);
begin
  pLockControls(True);
  setMod.Cancel;
end;

procedure TfModCad.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var mResult : Integer;
begin
  if (setMod <> nil) and
     (setMod.Modified) then
    begin
      mResult := MessageDlg('Salvar as altera��es', mtConfirmation,
                            [mbYes, mbNo, mbCancel], 0);
      case mResult of
        mrYes    : pLockControls(True);
        mrNo     : pLockControls(True);
        mrCancel : CanClose := False;
      end;
    end;
end;

procedure TfModCad.FormDestroy(Sender: TObject);
begin
  //Esta dando problema aqui no momento de fechar
  setMod.Close;
  // Sen�o liberar, d� um erro de mem�ria quando o Form � fechado
  Application.OnHint := nil;
end;

procedure TfModCad.btnDelClick(Sender: TObject);
begin
  //if not dmGeral.bControleTotal then
  //if not dmGeral.fGetAcesso(1) then
  //  Exit;

  if not dmGeral.fMensagemYesNo('Apagar o registro selecionado?') then
    Abort;
end;

procedure TfModCad.btnEditClick(Sender: TObject);
begin
  bInsert := False;
  //if not dmGeral.bControleTotal then
  //if not dmGeral.fGetAcesso(0) then
  //  Exit;

  pLockControls(False);
end;

procedure TfModCad.btnNewClick(Sender: TObject);
begin
  bInsert := True;
  //if not dmGeral.bControleTotal then
  //if not dmGeral.fGetAcesso(2) then
  //  Exit;
  pLockControls(False);
end;

procedure TfModCad.btnSaveClick(Sender: TObject);

    function fVerificaPreenchimento: Boolean;
    var i: Integer;
    begin
      Result := True;
      for i := 0 to ComponentCount -1 do
        if (Components[i] is TEdit) and (Trim(TEdit(Components[i]).Text) = '')and
           (TEdit(Components[i]).tag = 1) then
          begin
            //showmessage(Components[i].Name);
            Result := False;
            Break;
          end
        else
        if (Components[i] is TMemo) and (Trim(TMemo(Components[i]).Text) = '')and
           (TMemo(Components[i]).tag = 1) then
          begin
            Result := False;
            Break;
          end
        else
        if (Components[i] is TDBLookupComboBox) and (TDBLookupComboBox(Components[i]).KeyValue = Null)and
           (TDBLookupComboBox(Components[i]).tag = 1) then
          begin
            //showmessage(Components[i].Name);
            Result := False;
            Break;
          end
        else
        if (Components[i] is TMaskEdit) and (Trim(TMaskEdit(Components[i]).Text) = '') and
          (TMaskEdit(Components[i]).tag = 1) and (TMaskEdit(Components[i]).EditMask <> '!99/99/9999;1;_') then
          begin
            //showmessage(Components[i].Name);
            Result := False;
            Break;
          end
        else
        if (Components[i] is TMaskEdit) and (TMaskEdit(Components[i]).Text = '  /  /    ') and
          (TMaskEdit(Components[i]).tag = 1) and (TMaskEdit(Components[i]).EditMask = '!99/99/9999;1;_') then
          begin
            //showmessage(Components[i].Name);
            Result := False;
            Break;
          end
        else  
        if (Components[i] is TSpinEdit) and (Trim(TSpinEdit(Components[i]).Text) = '')and
           (TSpinEdit(Components[i]).tag = 1) then
          begin
            //showmessage(Components[i].Name);
            Result := False;
            Break;
          end
        else
        if (Components[i] is TRealEdit) and (TRealEdit(Components[i]).Value = 0)and
           (TRealEdit(Components[i]).tag = 1) then
          begin
            //showmessage(Components[i].Name);
            Result := False;
            Break;
          end
        else
        if (Components[i] is TcktUF) and (Trim(TcktUF(Components[i]).Text) = '')and
           (TcktUF(Components[i]).tag = 1) then
          begin
            //showmessage(Components[i].Name);
            Result := False;
            Break;
          end;
    end;

begin
  if not fVerificaPreenchimento then
    begin
      Application.MessageBox('Os campos em destaque s�o requeridos!','Aten��o!!!', mb_OK + mb_IconExclamation);
      Abort;
    end
  else
    pLockControls(True);
end;

procedure TfModCad.SpeedButton1Click(Sender: TObject);
begin
  Close;
end;

procedure TfModCad.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfModCad.btnHelpClick(Sender: TObject);
begin
  Application.HelpCommand(HELP_CONTENTS, 0);
end;

procedure TfModCad.bntCancelClick(Sender: TObject);
begin
  pLockControls(True);
end;

procedure TfModCad.btnSearchClick(Sender: TObject);
begin
  pnlSearch.Visible := True;
  btnNew.Enabled    := False;
  btnEdit.Enabled   := False;
  btnSave.Enabled   := False;
  btnCancel.Enabled := True;
  btnDel.Enabled    := False;
  btnSearch.Enabled := False;
  btnPrint.Enabled  := False;
  btnHelp.Enabled   := False;
end;

procedure TfModCad.btnOKClick(Sender: TObject);
begin
//  CoolBar.Visible := True;
  pLockControls(True);
end;

end.
