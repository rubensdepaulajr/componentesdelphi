unit uAssocPesquisar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, Mask, ExtCtrls, Grids, DBGrids, Db, ADODB,
  DBCtrls;

type
  TfAssocPesquisar = class(TForm)
    pnlTop: TPanel;
    pnlClient: TPanel;
    pnlToolBar: TPanel;
    pnlSearch: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    edtASPMES: TMaskEdit;
    edtRG: TEdit;
    edtNome: TEdit;
    btnSearch: TBitBtn;
    btnClose: TBitBtn;
    pgc: TPageControl;
    tshTit: TTabSheet;
    tshDep: TTabSheet;
    grd: TDBGrid;
    DBGrid2: TDBGrid;
    setAssoc: TADODataSet;
    setDep: TADODataSet;
    dsAssoc: TDataSource;
    dsDep: TDataSource;
    setAssocASPMES: TStringField;
    setAssocNOME: TStringField;
    setAssocRG: TStringField;
    setDepCODIGO: TStringField;
    setDepNOME: TStringField;
    dbnav: TDBNavigator;
    btnSelect: TBitBtn;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    procedure btnCloseClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure setAssocAfterScroll(DataSet: TDataSet);
    procedure FormDestroy(Sender: TObject);
    procedure pgcChange(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure btnSelectClick(Sender: TObject);
  private
    { Private declarations }
    // Indica se o usu�rio selecionou o Credenciado ou se ele cancelou    
    bOK: Boolean;
  public
    { Public declarations }
    function fInit_Form(var sASPMES, sNomeAss, sCodPac, sPaciente: string): Boolean;
  end;

var
  fAssocPesquisar: TfAssocPesquisar;

implementation

uses udmFaturamento, uGeral;

{$R *.DFM}

function TfAssocPesquisar.fInit_Form(var sASPMES, sNomeAss, sCodPac, sPaciente: string): Boolean;
begin
  pgc.ActivePageIndex := 0;
  bOK := False;
  Self.ShowModal;
  sASPMES := setAssocASPMES.Value;
  sNomeAss := setAssocNOME.Value;
  if pgc.ActivePageIndex = 1 then
    begin
      sCodPac   := setDepCODIGO.Value;
      sPaciente := setDepNOME.Value;
    end
  else
    begin
      sCodPac   := setAssocASPMES.Value;
      sPaciente := setAssocNOME.Value;
    end;
  Result := bOK;
  Self.Free;
end;

procedure TfAssocPesquisar.btnCloseClick(Sender: TObject);
begin
  bOK := False;
  Self.Close;
end;

procedure TfAssocPesquisar.btnSearchClick(Sender: TObject);
var sTmp: string;
begin
  if (Trim(edtASPMES.Text) <> '') then
    begin
      if (Copy(edtASPMES.Text,1,2) = '00') then
        sTmp := 'WHERE aspmes = '+ QuotedStr(edtASPMES.Text)
      else
        sTmp := 'WHERE codigo = '+ QuotedStr(edtASPMES.Text)
    end
  else
  if (pgc.ActivePageIndex = 0) and (Trim(edtRG.Text) <> '') then
    sTmp := 'WHERE rg = '+ edtRG.Text
  else
  if Trim(edtNome.Text) <> '' then
    sTmp := 'WHERE nome LIKE '+ QuotedStr(edtNome.TExt + '%')
  else
    begin
      dmFaturamento.pMensagemOK('Nenhum crit�rio de pesquisa especificado!','!');
      Exit;
    end;

  if (Copy(edtASPMES.Text,1,2) <> '00') and (Trim(edtASPMES.Text) <> '') then
    begin
      sTmp := dmFaturamento.fGetValue('SELECT socio FROM associados WHERE codigo = '+ QuotedStr(sTmp));
      if Trim(sTmp) = '' then
        begin
          dmFaturamento.pMensagemOK('Nenhum dependente encontrado!','!');
          Exit;
        end;
    end;

  with setAssoc do
    begin
      Close;
      CommandText := 'SELECT aspmes, rg, nome FROM associados '+ sTmp;
      Open;
      if Bof and Eof then
        dmFaturamento.pMensagemOK('Nenhum Associado encontrado!','!')
      else
        grd.SetFocus;

      if (Copy(edtASPMES.Text,1,2) <> '00') and (Trim(edtASPMES.Text) <> '') then
        begin
          pgc.ActivePageIndex := 1;
          pgcChange(Sender);
        end;
    end;

end;


procedure TfAssocPesquisar.setAssocAfterScroll(DataSet: TDataSet);
begin
  with setDep do
    begin
      Close;
      CommandText := 'SELECT codigo, nome FROM dependentes WHERE socio = '+ setAssocASPMES.Value;
      Close;
    end;
end;

procedure TfAssocPesquisar.FormDestroy(Sender: TObject);
begin
  setDep.Close;
  setAssoc.Close;
end;

procedure TfAssocPesquisar.pgcChange(Sender: TObject);
begin
    case pgc.ActivePageIndex of
    0 : dbnav.DataSource := dsAssoc;
    1 : dbnav.DataSource := dsDep;
  end;
  with setDep do
    begin
      Close;
      CommandText := 'SELECT codigo, nome FROM dependentes WHERE socio = '+ setAssocASPMES.Text;
      Open;
    end;
end;

procedure TfAssocPesquisar.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
     begin
       key:=#0;
       Perform(WM_NextDlgCtl,0,0);
     end;
end;

procedure TfAssocPesquisar.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_F2 : btnClose.Click;
    VK_F3 : btnSearch.Click;
    VK_F4 : btnSelect.Click;
  end;
end;

procedure TfAssocPesquisar.btnSelectClick(Sender: TObject);
begin
  bOK := True;
  Self.Close;
end;

end.
