unit uProcPesquisar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, Mask, ExtCtrls, Grids, DBGrids, Db, ADODB,
  DBCtrls;

type
  TfProcPesquisar = class(TForm)
    pnlTop: TPanel;
    pnlClient: TPanel;
    pnlToolBar: TPanel;
    pnlSearch: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    edtCod: TMaskEdit;
    edtDesc: TEdit;
    btnSearch: TBitBtn;
    btnClose: TBitBtn;
    pgc: TPageControl;
    tsh: TTabSheet;
    grd: TDBGrid;
    setProc: TADODataSet;
    dsProc: TDataSource;
    dbnav: TDBNavigator;
    btnSelect: TBitBtn;
    rgpTabela: TRadioGroup;
    setProcCODIGO: TStringField;
    setProcDESCRICAO: TStringField;
    setProcTABELA: TStringField;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    procedure btnCloseClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSelectClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    // Indica se o usu�rio selecionou o Credenciado ou se ele cancelou
    bOK: Boolean;
  public
    { Public declarations }
    function fInit_Form(var sCodigo, sNome: string): Boolean;
  end;

var
  fProcPesquisar: TfProcPesquisar;

implementation

uses udmFaturamento;

{$R *.DFM}

function TfProcPesquisar.fInit_Form(var sCodigo, sNome: string): Boolean;
begin
  Self.ShowModal;
  sCodigo := setProcCodigo.Value;
  sNome := setProcDESCRICAO.Value;

  Result := bOK;
  Self.Free;
end;

procedure TfProcPesquisar.btnCloseClick(Sender: TObject);
begin
  bOK := False;
  Self.Close;
end;

procedure TfProcPesquisar.btnSearchClick(Sender: TObject);
var sTmp: string;
begin
  sTmp := '';

  if Trim(edtDesc.Text) <> '' then
    sTmp := 'WHERE descricao LIKE '+ QuotedStr(Trim(edtDesc.Text) +'%')
  else
  if Trim(edtCod.Text) <> '' then
    sTmp := 'WHERE codigo LIKE '+ QuotedStr(Trim(edtCod.Text) +'%')
  else
    begin
      dmFaturamento.pMensagemOK('Nenhum crit�rio de pesquisa especificado!','!');
      Exit;
    end;

  sTmp := sTmp + ' AND tabela = '+ IntToStr(rgpTabela.ItemIndex);

  with setProc do
    begin
      Close;
      CommandText := 'SELECT codigo, descricao, CASE tabela WHEN 0 THEN ''AMB'' WHEN 1 THEN ''CBHPM'' END AS tabela '+
                     'FROM dbo.procedimento '+
                     sTmp +
                     'ORDER BY descricao';
      Open;
      if (Bof and Eof) then
        dmFaturamento.pMensagemOK('Nenhum Procedimento encontrado!','!')
      else
        grd.SetFocus;

    end; // with ...
end;


procedure TfProcPesquisar.FormDestroy(Sender: TObject);
begin
  setProc.Close;
end;

procedure TfProcPesquisar.btnSelectClick(Sender: TObject);
begin
  bOk := True;
  Self.Close;
end;

procedure TfProcPesquisar.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
     begin
       key:=#0;
       Perform(WM_NextDlgCtl,0,0);
     end;
end;

procedure TfProcPesquisar.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_F2 : btnClose.Click;
    VK_F3 : btnSearch.Click;
    VK_F4 : btnSelect.Click;
  end;
end;

end.
