unit uCredPesquisar;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  ComCtrls, StdCtrls, Buttons, Mask, ExtCtrls, Grids, DBGrids, Db, ADODB,
  DBCtrls;

type
  TfCredPesquisar = class(TForm)
    pnlTop: TPanel;
    pnlClient: TPanel;
    pnlToolBar: TPanel;
    pnlSearch: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    edtCodigo: TMaskEdit;
    edtNome: TEdit;
    btnSearch: TBitBtn;
    btnClose: TBitBtn;
    pgc: TPageControl;
    tsh: TTabSheet;
    grd: TDBGrid;
    setCred: TADODataSet;
    dsCred: TDataSource;
    dbnav: TDBNavigator;
    btnSelect: TBitBtn;
    setCredCODIGO: TStringField;
    setCredNOME: TStringField;
    setCredTERMINO: TDateTimeField;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    procedure btnCloseClick(Sender: TObject);
    procedure btnSearchClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure btnSelectClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
  private
    { Private declarations }
    // Indica se o usu�rio selecionou o Credenciado ou se ele cancelou
    bOK: Boolean;
  public
    { Public declarations }
    function fInit_Form(var sCodigo, sNome: string): Boolean;
  end;

var
  fCredPesquisar: TfCredPesquisar;

implementation

uses udmFaturamento;

{$R *.DFM}

function TfCredPesquisar.fInit_Form(var sCodigo, sNome: string): Boolean;
begin
  bOK := False;
  Self.ShowModal;
  sCodigo := setCredCodigo.Value;
  sNome := setCredNOME.Value;

  Result := bOK;
  Self.Free;
end;

procedure TfCredPesquisar.btnCloseClick(Sender: TObject);
begin
  bOK := False;
  Self.Close;
end;

procedure TfCredPesquisar.btnSearchClick(Sender: TObject);
var sTmp: string;
begin
  if (Trim(edtCodigo.Text) <> '') then
    sTmp := 'WHERE codigo = '+ edtCodigo.Text
  else
  if Trim(edtNome.Text) <> '' then
    sTmp := 'WHERE nome LIKE '+ QuotedStr(edtNome.TExt + '%')
  else
    begin
      dmFaturamento.pMensagemOK('Nenhum crit�rio de pesquisa especificado!','!');
      Exit;
    end;

  with setCred do
    begin
      Close;
      CommandText := 'SELECT codigo, nome, termino FROM credenciados '+ sTmp + ' ORDER BY nome ';
      Open;
      if Bof and Eof then
        dmFaturamento.pMensagemOK('Nenhum Credenciado encontrado!','!')
      else
        grd.SetFocus;
      btnSelect.Enabled := not(Bof and Eof);
    end;

end;


procedure TfCredPesquisar.FormDestroy(Sender: TObject);
begin
  setCred.Close;
end;

procedure TfCredPesquisar.btnSelectClick(Sender: TObject);
begin
  bOk := True;
  Self.Close;
end;

procedure TfCredPesquisar.FormKeyPress(Sender: TObject; var Key: Char);
begin
  if key = #13 then
     begin
       key:=#0;
       Perform(WM_NextDlgCtl,0,0);
     end;
end;

procedure TfCredPesquisar.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  case Key of
    VK_F2 : btnClose.Click;
    VK_F3 : btnSearch.Click;
    VK_F4 : btnSelect.Click;
  end;
end;

end.
