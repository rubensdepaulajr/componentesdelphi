// PalmTable - componente para abrir el formato .PDB de Palm y pasar los datos
//             a una base de datos Paradox
//    PDBStruct - estructura de la base de datos .PDB en el formato:
//                fieldName, fieldType, size

unit PalmTable;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  Db, DBTables;

const
     pftString  = 0;
     pftInteger = 1;
     pftFloat   = 2;
     pftBoolean = 3;
     pftDate    = 4;
     pftTime    = 5;

type
//  TPDBPalmVars = (pftString, pftInteger, pftFloat, pftBoolean, pftDate, pftTime);

  TPalmTable = class(TTable)
  private
    { Private declarations }
    F : file;
    FileX : integer;
    fPDBFileName : string;
    fPDBStruct : TStringList;
    fPDBName : string;
    fPDBFlags : integer;
    fPDBVersion : string;
    fPDBCreateDate : TDateTime;
    fPDBModDate : TDateTime;
    fPDBBakDate : TDateTime;
    fPDBModNumber : integer;
    fPDBAppInfo : integer;
    fPDBSortInfo : integer;
    fPDBType : string;
    fPDBCreator : string;
    fPDBUniqueID : integer;
    fPDBNextRecList : integer;
    fPDBRecNum : integer;

    procedure SetPDBFileName(const Value: string);
    procedure SetPDBStruct(const Value : TStringList);
    procedure SetPDBName(const Value : string);
    procedure SetPDBFlags(const Value : integer);
    procedure SetPDBVersion(const value : string);
    procedure SetPDBCreateDate(const value : TDateTime);
    procedure SetPDBModDate(const value : tDateTime);
    procedure SetPDBBakDate(const value : TDateTime);
    procedure SetPDBModNumber (const value : integer);
    procedure SetPDBAppInfo(const value : integer);
    procedure SetPDBSortInfo(const value : integer);
    procedure SetPDBType(const value : string);
    procedure SetPDBCreator(const value : string);
    procedure SetPDBUniqueID(const value : integer);
    procedure SetPDBNextRecList(const value : integer);
    procedure SetPDBRecNum(const value : integer);

    procedure DecodeHeader;
    procedure EncodeHeader;
    procedure ImportaPDB;
    procedure ExportaPDB;
    procedure SpawnStruct;
    procedure GenerateStruct;
    procedure DecodeStruct(s:string; var fname:string; var ftype,fsize:integer);
    function NSBDate(theDate:TDateTime):integer;
    function NSBTime(theTime:TDateTime):integer;
    function CharToWord(c1,c2:char):word;
    function CharToWordStr(c1,c2:char):string;
    function CharToDWord(c1,c2,c3,c4:char): longword;
    function CharToDWordStr(c1,c2,c3,c4:char):string;
    function CharToDate(c1,c2,c3,c4:char):TDateTime;
    function CharToDateStr(c1,c2,c3,c4:char):string;

  protected
    { Protected declarations }

  public
    { Public declarations }

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

  published
    { Published declarations }
    property PDBFileName : string         read fPDBFileName     write SetPDBFileName;
    property PDBStruct: TStringList       read fPDBStruct       write SetPDBStruct;
    property PDBName : string             read fPDBName         write SetPDBName;
    property PDBFlags : integer           read fPDBFlags        write SetPDBFlags;
    property PDBVersion : string          read fPDBVersion      write SetPDBVersion;
    property PDBCreateDate : TDateTime    read fPDBCreateDate   write SetPDBCreateDate;
    property PDBModDate : TDateTime       read fPDBModDate      write SetPDBModDate;
    property PDBBakDate : TDateTime       read fPDBBakDate      write SetPDBBakDate;
    property PDBModNumber : integer       read fPDBModNumber    write SetPDBModNumber;
    property PDBAppInfo : integer         read fPDBAppInfo      write SetPDBAppInfo;
    property PDBSortInfo : integer        read fPDBSortInfo     write SetPDBSortInfo;
    property PDBType : string             read fPDBType         write SetPDBtype;
    property PDBCreator : string          read fPDBCreator      write SetPDBCreator;
    property PDBUniqueID : integer        read fPDBUniqueID     write SetPDBUniqueID;
    property PDBNextRecList : integer     read fPDBNextRecList  write SetPDBNextRecList;
    property PDBRecNum : integer          read fPDBRecNum       write SetPDBRecNum;

    procedure PDB2Table;
    procedure Table2PDB;
  end;

procedure Register;


implementation


procedure Register;
begin
  RegisterComponents('Palm', [TPalmTable]);
end;

{ TPalmTable }

constructor TPalmTable.Create(AOwner: TComponent);
begin
     inherited;
     fPDBStruct := TStringList.Create;
     fPDBCreator := 'Palm';
     fPDBType := 'DATA';
     fPDBName := 'PalmTable';
     fPDBFileName := 'PalmTable.pdb';
     fPDBAppInfo := 0;
     fPDBSortInfo := 0;
     fPDBModNumber := 0;
     fPDBUniqueID := 0;
     fPDBRecNum := 0;
     fPDBNextRecList := 0;
     fPDBFlags := 0;
     fPDBVersion := '';
     fPDBCreateDate := Date();
     fPDBModDate := Date();
     fPDBBakDate := Date();

end;

destructor TPalmTable.Destroy;
begin
     fPDBStruct.Clear;
     fPDBStruct.Free;
     inherited;
end;


procedure TPalmTable.PDB2Table;
var
   seguir : boolean;
begin
     seguir := true;

     {revisa que las propiedades tengan valores}
     if PDBFileName = '' then
        seguir := false;
     if TableName = '' then
        seguir := false;
     seguir := FileExists(PDBFileName);
     if fPDBStruct.Count = 0 then
        seguir := false;

     if seguir then begin
        SpawnStruct;
        {abre el archivo}
        AssignFile(F,fPDBFileName);
        {$I-}
        Reset(F,1);
        {$I+}
        if IOResult = 0 then begin
           DecodeHeader;
           ImportaPDB;
           Active := true;
           CloseFile(F);
        end;
     end;
end;

procedure TPalmTable.Table2PDB;
var
   seguir : boolean;
begin
     seguir := true;

     {revisa que las propiedades tengan valores}
     if PDBFileName = '' then
        PDBFileName := 'PalmTable.PDB';
     if TableName = '' then
        seguir := false;
     seguir := FileExists(TableName);

     if seguir then begin
        GenerateStruct;
        {abre el archivo}
        AssignFile(F,fPDBFileName);
        {$I-}
        Reset(F,1);
        {$I+}
        if IOResult = 0 then begin
           EncodeHeader;
           ExportaPDB;
           Active := true;
           CloseFile(F);
        end;
     end;
end;


procedure TPalmTable.DecodeHeader;
var
    s : string;
    x : integer;
    hBuf : array[1..78] of char;
    c : char;
begin
     {lee el header}
     BlockRead(F,hBuf,78);
     FileX := 78;
     {decodifica el header}
     s := '';
     for x := 1 to 32 do
         s := s + hBuf[x];

     {database header}
     fPDBName        := s;
     fPDBFlags       := CharToWord(hBuf[33],hBuf[34]);
     fPDBVersion     := CharToWordStr(hBuf[35],hBuf[36]);
     fPDBCreateDate  := CharToDate(hBuf[37],hBuf[38],hBuf[39],hBuf[40]);
     fPDBModDate     := CharToDate(hBuf[41],hBuf[42],hBuf[43],hBuf[44]);
     fPDBBakDate     := CharToDate(hBuf[41],hBuf[42],hBuf[43],hBuf[44]);
     fPDBModNumber   := CharToDWord(hBuf[41],hBuf[42],hBuf[43],hBuf[44]);
     fPDBAppInfo     := CharToDWord(hBuf[53],hBuf[54],hBuf[55],hBuf[56]);
     fPDBSortInfo    := CharToDWord(hBuf[57],hBuf[58],hBuf[59],hBuf[60]);
     fPDBType        := hBuf[61]+hBuf[62]+hBuf[63]+hBuf[64];
     fPDBCreator     := hBuf[65]+hBuf[66]+hBuf[67]+hBuf[68];
     fPDBUniqueID    := CharToDWord(hBuf[69],hBuf[70],hBuf[71],hBuf[72]);

     {RecordList structure}
     fPDBNextRecList := CharToDWord(hBuf[73],hBuf[74],hBuf[75],hBuf[76]);
     fPDBRecNum      := CharToWord(hBuf[77],hBuf[78]);

     if fPDBRecNum = 0 then begin
        BlockRead(F,c,1);
        BlockRead(F,c,1);
     end;
end;


procedure TPalmTable.ImportaPDB;
type
  TRecListItem = record
     offset : integer;
     num : integer;
     attrib : integer;
     id : string;
     size : integer;
  end;
var
   RecList : array of TRecListItem;
   rBuf : array[1..8] of char;
   iBuf : array[1..4] of char;
   c : char;
   b : byte;
   x,y,fsize,fieldsize : longint;
   ftype : integer;
   fname,fvalue : string;

   function ExtraeTexto(var xx,tipo:integer) : string;
   var
      s : string;
      b : byte;
   begin
        case tipo of
           pftString : begin
               s := '';
               repeat
                  BlockRead(F,b,1);
                  s := s+chr(b);
                  inc(xx);
                  inc(FileX);
               until b= 0;
               ExtraeTexto := s;
               end;

           pftInteger : begin
               BlockRead(F,iBuf,4);
               inc(xx,4);
               inc(FileX,4);
               ExtraeTexto := CharToDWordStr(iBuf[1],iBuf[2],iBuf[3],iBuf[4]);
               end;
           pftDate : begin
               BlockRead(F,iBuf,4);
               inc(xx,4);
               inc(FileX,4);
               ExtraeTexto := CharToDateStr(iBuf[1],iBuf[2],iBuf[3],iBuf[4]);
               end;
        end;
   end;

begin
     {abre apuntadores a registros}
     SetLength(RecList,fPDBRecNum);
     for x := 0 to fPDBRecNum-1 do begin
         BlockRead(F,rBuf,8);
         RecList[x].Offset := CharToDWord(rBuf[1], rBuf[2], rBuf[3], rBuf[4]);
         RecList[x].Num := x;
         RecList[x].Attrib := ord(rBuf[5]);
         RecList[x].ID := rBuf[6]+rBuf[7]+rBuf[8];
         inc(FileX,8);
     end;

     {calcula tama�o de los registros}
     Last;
     fsize := FileSize(F);
     for x := fPDBRecNum-1 downto 0 do begin
         RecList[x].Size := fsize - RecList[x].Offset;
         fsize :=RecList[x].Offset;
     end;

     (*     { lee 2 bytes no documentados }
     BlockRead(F,b,1);
     BlockRead(F,b,1);
     inc(FileX,2);
     *)

     {avanza al principio de los RECORDS}
     {esto es necesario si existe AppInfo o SortInfo}
     if FileX < RecList[0].Offset then
        repeat
           BlockRead(F,c,1);
           inc(FileX);
        until FileX = RecList[0].Offset;

     { procesa cada registro }
     for x := 0 to fPDBRecNum-1 do begin
         fsize := 0;
         Append;
         for y := 0 to fPDBStruct.Count-1 do begin
             {divide el registro en campos }
             DecodeStruct(fPDBStruct[y],fname,ftype,fieldsize);
             fvalue := ExtraeTexto(fsize,ftype);
             {guarda en la tabla}
             FieldByName(fname).AsString := fvalue;
         end;

         {descarta basura}
         if fsize < RecList[x].Size then
            repeat
                  BlockRead(F,b,1);
                  inc(fsize);
                  inc(FileX);
            until fsize = RecList[x].Size;
     end;
     Post;
end;

procedure TPalmTable.SpawnStruct;
var
   x : integer;
   fname : string;
   ftype,fsize : integer;
begin
     {crea una tabla con la estructura}
     Active := false;
     TableType := ttParadox;
     if TableName = '' then
        TableName := 'PalmTable';
     with FieldDefs do begin
          Clear;
          for x := 0 to fPDBStruct.Count-1 do begin
              with AddFieldDef do begin
                   DecodeStruct(fPDBStruct[x],fname,ftype,fsize);
                   Name := fname;
                   case ftype of
                        pftString : begin
                                    DataType := ftString;
                                    Size := fsize;
                                    end;
                        pftInteger : DataType := ftInteger;
                        pftBoolean : DataType := ftBoolean;
                        pftDate : DataType := ftDate;
                        pftTime : DataType := ftTime;
                        pftFloat : DataType := ftFloat;
                   end;
              end;
          end;
     end;
     CreateTable;
     Active := true;
end;


procedure TPalmTable.DecodeStruct(s:string; var fname:string; var ftype,fsize:integer);
var
   x : integer;
   ss : string;
begin
     x := Pos(',',s);
     fname := Copy(s,1,x-1);
     ss := Copy(s,x+1,length(s)-x);
     x := Pos(',',ss);
     ftype := StrToInt(Copy(ss,1,x-1));
     fsize := StrToInt(Copy(ss,x+1,length(ss)-x));
end;


(********** TALBE 2 PDB ************)

procedure TPalmTable.GenerateStruct;
var
   x : integer;
begin
     x := 1;
end;

procedure TPalmTable.EncodeHeader;
var
    s,Nullstr : string;
    x : integer;
    W : word;
    D : longword;
begin
     {prepara variables}
     FileX := 78;
     NullStr := '';
     for x := 1 to 32 do
         NullStr := NullStr +chr(0);

     {database header}
     s := Copy(fPDBName+NullStr,1,32); BlockWrite(F,s,32);
     W := fPDBFlags; BlockWrite(F,W,2);
     W := Ord(fPDBVersion[1])+Ord(fPDBVersion[2]); Blockwrite(F,W,2);
     D := NSBTime(fPDBCreateDate); BlockWrite(F,D,4);
     D := NSBTime(fPDBModDate); BlockWrite(F,D,4);
     D := NSBTime(fPDBBakDate); Blockwrite(F,D,4);
     D := fPDBModNumber; Blockwrite(F,D,4);
     D := fPDBAppInfo; Blockwrite(F,D,4);
     D := fPDBSortInfo; Blockwrite(F,D,4);
     s := Copy(fPDBType+NullStr,1,4); Blockwrite(F,s,4);
     s := Copy(fPDBCreator+NullStr,1,4); Blockwrite(F,s,4);
     D := fPDBUniqueID; Blockwrite(F,D,4);

     {RecordList structure}
     D := fPDBNextRecList; Blockwrite(F,D,4);
     W := fPDBRecNum; Blockwrite(F,W,2);
end;

procedure TPalmTable.ExportaPDB;
var
   x : integer;
begin
     x := 1;
end;




(******   CONVERSION FUNCTIONS ****************)

function TPalmTable.CharToWord(c1,c2:char):word;
begin
     CharToWord := ord(c1)*256 + ord(c2);
end;

function TPalmTable.CharToWordStr(c1,c2:char):string;
begin
     CharToWordStr := IntToStr(CharToWord(c1,c2));
end;

function TPalmTable.CharToDWord(c1,c2,c3,c4:char): longword;
begin
     CharToDWord := ord(c1)*16777216 + ord(c2)*65536 + ord(c3)*256 + ord(c4);
end;

function TPalmTable.CharToDWordStr(c1,c2,c3,c4:char):string;
begin
     CharToDWordStr := IntToStr(CharToDWord(c1,c2,c3,c4));
end;

function TPalmTable.NSBTime(theTime:TDateTime):integer;
var
  Hour, Min, Sec, MSec: Word;
begin
     DecodeTime(theTime, Hour, Min, Sec, MSec);
     NSBTime := (Hour*10000)+ (Min*100) + Sec;
end;

function TPalmTable.NSBDate(theDate:TDateTime):integer;
var
   y,m,d : word;
begin
     decodedate(theDate,y,m,d);
     NSBDate:= ((y-1900) * 10000) + (m * 100) + d;
end;

function TPalmTable.CharToDate(c1,c2,c3,c4:char):TDateTime;
var
   fecha : double;
   PalmDate : double;
begin
     fecha := CharToDWord(c1,c2,c3,c4); // segundos desde 1/1/1904.
     fecha := fecha / SecsPerDay;
     PalmDate := EncodeDate(1904,1,1) - EncodeDate(1899,12,30);
     fecha := fecha + PalmDate;
     CharToDate := fecha;
end;

function TPalmTable.CharToDateStr(c1,c2,c3,c4:char):string;
begin
     CharToDateStr := DateToStr(CharToDate(c1,c2,c3,c4));
end;



{*********   PROPERTY CONTROL FUNCTIONS ************}

procedure TPalmTable.SetPDBFileName(const Value: string);
begin
     fPDBFileName := Value;
end;

procedure TPalmTable.SetPDBStruct(const Value : TStringList);
begin
     fPDBStruct := Value;
end;

procedure TPalmTable.SetPDBName(const Value : string);
begin
     fPDBName := Value;
end;

procedure TPalmTable.SetPDBFlags(const Value : integer);
begin
     fPDBFlags := Value;
end;

procedure TPalmTable.SetPDBVersion(const value : string);
begin
     fPDBVersion := Value;
end;

procedure TPalmTable.SetPDBCreateDate(const value : TDateTime);
begin
     fPDBCreateDate := Value;
end;

procedure TPalmTable.SetPDBModDate(const value : tDateTime);
begin
     fPDBModDate := Value;
end;

procedure TPalmTable.SetPDBBakDate(const value : TDateTime);
begin
     fPDBBakDate := Value;
end;

procedure TPalmTable.SetPDBModNumber (const value : integer);
begin
     fPDBModNumber := Value;
end;

procedure TPalmTable.SetPDBAppInfo(const value : integer);
begin
     fPDBAppInfo := Value;
end;

procedure TPalmTable.SetPDBSortInfo(const value : integer);
begin
     fPDBSortInfo := Value;
end;

procedure TPalmTable.SetPDBType(const value : string);
begin
     fPDBType := Value;
end;

procedure TPalmTable.SetPDBCreator(const value : string);
begin
     fPDBCreator := Value;
end;

procedure TPalmTable.SetPDBUniqueID(const value : integer);
begin
     fPDBUniqueID := Value;
end;

procedure TPalmTable.SetPDBNextRecList(const value : integer);
begin
     fPDBNextRecList := Value;
end;

procedure TPalmTable.SetPDBRecNum(const value : integer);
begin
     fPDBRecNum := Value;
end;

end.
